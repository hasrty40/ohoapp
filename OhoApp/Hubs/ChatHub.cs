﻿using Microsoft.AspNetCore.SignalR;
using OhoApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OhoApp.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendOrder(OrderDetail order)
        {
            await Clients.All.SendAsync("CurrentOrder", order);
        }
    }
}
