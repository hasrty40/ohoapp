﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Nancy.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OhoApp.Models;
using OhoApp.Services;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static OhoApp.Controllers.DefaultController;
using static OhoApp.Models.OrderDetail;
using StatusCodes = OhoApp.Models.StatusCodes;

namespace OhoApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerData _repository;
        private readonly IConfiguration _configuration;
        public CustomerController(ICustomerData repository, IRestaurantData repositoryRestaurant, IConfiguration configuration)
        {
            this._repository = repository;
            this._configuration = configuration;
        }

        #region Token generator
        private UserToken BuildToken(CustomerInfo data, string guid)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Sid, guid),
                new Claim(ClaimTypes.Email, data.Email ?? " "),
                new Claim(ClaimTypes.MobilePhone, data.PhoneNumber ?? " "),
                new Claim(ClaimTypes.NameIdentifier, data.CUS_ID.ToString()),
                new Claim("mykey", _configuration["JWT:mykeyCustomer"])
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.UtcNow.AddYears(1);

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: null,
                audience: null,
                claims: claims,
                expires: expiration,
                signingCredentials: creds);

            return new UserToken()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expiration
            };
        }

        private UserToken BuildTokenGuest(CustomerGuestInfo data)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, data.Token),
                new Claim(ClaimTypes.NameIdentifier, data.CUS_ID.ToString()),
                new Claim(ClaimTypes.Sid, Guid.NewGuid().ToString("n")),
                new Claim("mykey",  _configuration["JWT:mykeyGuest"])
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.UtcNow.AddYears(1);

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: null,
                audience: null,
                claims: claims,
                expires: expiration,
                signingCredentials: creds);

            return new UserToken()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expiration
            };
        }
        #endregion

        #region Email

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private IRestResponse SendOTPWelcomeMessage(CustomerInfo customer)
        {
            RestClient client = new RestClient();

            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api", _configuration["mailgun:key"]);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", _configuration["mailgun:domain"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", _configuration["mailgun:mailname"]);
            request.AddParameter("to", customer.Email);
            request.AddParameter("template", "welcome-email-customer");

            EmailWelcomeCustomer _email = new EmailWelcomeCustomer();
            _email.button = new EmailWelcomeCustomerBotton();
            _email.from = new EmailFrom();
            _email.footer = new EmailFooter();

            if (customer.AppLanguage == AppLanguageSelect.English)
            {
                request.AddParameter("subject", "Welcome Email Customer");
                _email.topic = "Dear " + customer.Firstname;
                _email.title1 = "Thank you!!";
                _email.title2 = "for confirming your email address. You’re all set and ready to start ordering.";
                _email.title3 = "Click the map below";
                _email.title4 = "to find the restaurants nearest to you!";
                _email.title5 = "We hope you enjoy your meal!";
                _email.from.title = "Best,";
            }
            else
            {
                request.AddParameter("subject", "EatEat - 会員登録が完了しました");
                _email.topic = customer.Firstname;
                _email.title1 = "会員登録が完了いたしました！";
                _email.title2 = "メールアドレスをご確認いただきどうもありがとうございました。Ohoのご利用の準備ができました。";
                _email.title3 = "マップをクリック！";
                _email.title4 = "近くのお店を探そう";
                _email.title5 = "";
                _email.from.title = "今後ともよろしくお願いいたします！";
            }

            _email.from.name = "The Oho team";
            _email.footer.address = "";
            string _template = "{\"topic\": \"" + _email.topic + "\", \"title1\": \"" + _email.title1 + "\", \"title2\": \"" + _email.title2 + "\", " +
            " \"title3\": \"" + _email.title3 + "\", \"title4\": \"" + _email.title4 + "\", \"title5\": \"" + _email.title5 + "\"," +
            " \"button\": {\"title\": \"" + _email.button.title + "\",\"url\": \"" + _email.button.url + "\"}, " +
            " \"from\": {\"title\": \"" + _email.from.title + "\",\"name\": \"" + _email.from.name + "\"}, " +
            " \"footer\": {\"address\": \"" + _email.footer.address + "\"}} ";
            request.AddParameter("h:X-Mailgun-Variables", _template);
            request.Method = Method.POST;
            return client.Execute(request);
        }

        private IRestResponse SendOTPMessageResetPw(string otp, string referKey, CustomerInfo customer)
        {
            RestClient client = new RestClient();
            EmailForgotPassword _email = new EmailForgotPassword();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api", _configuration["mailgun:key"]);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", _configuration["mailgun:domain"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", _configuration["mailgun:mailname"]);

            request.AddParameter("to", customer.Email);
            request.AddParameter("template", "forgot-password");
            _email.button = new EmailForgotPasswordBotton();
            _email.customerInfo = new EmailForgotPasswordCustomerInfo();
            _email.from = new EmailFrom();
            _email.footer = new EmailFooter();

            if (customer.AppLanguage == AppLanguageSelect.English)
            {
                request.AddParameter("subject", "Forgot Password");
                _email.topic = "Dear " + customer.Firstname;
                _email.title1 = "You are receiving this email because you requested a password reset for your Oho account.";
                _email.title2 = "If this is not the case then please disregard this email, otherwise please use this verification code to reset your password.";
                _email.button.otp = otp;
                _email.customerInfo.title = "Your Username is: ";
                _email.customerInfo.name = customer.Email;
                _email.title3 = "Thanks for supporting your local restaurants!";
                _email.from.title = "Best,";
            }
            else
            {
                request.AddParameter("subject", "Oho - パスワードのリセット");
                _email.topic = customer.Firstname;
                _email.title1 = "EatEatアカウントについて、パスワードのリセットのリクエストがありました。下記のリンクをクリックし、操作を続けてください。";
                _email.title2 = "リセットのリクエストをされた覚えがない場合はこのメールを無視してください。";
                _email.button.otp = otp;
                _email.customerInfo.title = "ユーザーネーム: ";
                _email.customerInfo.name = customer.Email;
                _email.title3 = "EatEatをご利用頂きどうもありがとうございます。";
                _email.from.title = "今後ともよろしくお願いいたします！";
            }

            _email.from.name = "The EatEat team";
            _email.footer.address = "";
            string _template = "{\"topic\": \"" + _email.topic + "\", \"title1\": \"" + _email.title1 + "\", \"title2\": \"" + _email.title2 + "\", " +
            " \"button\": {\"otp\": \"" + _email.button.otp + "\"}, " +
            " \"customerInfo\": {\"title\": \"" + _email.customerInfo.title + "\",\"name\": \"" + _email.customerInfo.name + "\"}, " +
            " \"title3\": \"" + _email.title3 + "\", " +
            " \"from\": {\"title\": \"" + _email.from.title + "\",\"name\": \"" + _email.from.name + "\"}, " +
            " \"footer\": {\"address\": \"" + _email.footer.address + "\"}} ";
            request.AddParameter("h:X-Mailgun-Variables", _template);
            request.Method = Method.POST;
            return client.Execute(request);
        }


        #endregion

        #region list of mime types
        private static IDictionary<string, string> _mappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) {
        {".art", "image/x-jg"},
        {".bmp", "image/bmp"},
        {".cmx", "image/x-cmx"},
        {".cod", "image/cis-cod"},
        {".dib", "image/bmp"},
        {".gif", "image/gif"},
        {".ico", "image/x-icon"},
        {".ief", "image/ief"},
        {".jfif", "image/pjpeg"},
        {".jpe", "image/jpeg"},
        {".jpeg", "image/jpeg"},
        {".jpg", "image/jpeg"},
        {".mac", "image/x-macpaint"},
        {".pbm", "image/x-portable-bitmap"},
        {".pct", "image/pict"},
        {".pgm", "image/x-portable-graymap"},
        {".pic", "image/pict"},
        {".pict", "image/pict"},
        {".png", "image/png"},
        {".pnm", "image/x-portable-anymap"},
        {".pnt", "image/x-macpaint"},
        {".pntg", "image/x-macpaint"},
        {".pnz", "image/png"},
        {".ppm", "image/x-portable-pixmap"},
        {".qti", "image/x-quicktime"},
        {".qtif", "image/x-quicktime"},
        {".ras", "image/x-cmu-raster"},
        {".rf", "image/vnd.rn-realflash"},
        {".rgb", "image/x-rgb"},
        {".tif", "image/tiff"},
        {".tiff", "image/tiff"},
        {".wbmp", "image/vnd.wap.wbmp"},
        {".wdp", "image/vnd.ms-photo"},
        {".xbm", "image/x-xbitmap"},
        {".xpm", "image/x-xpixmap"},
        {".xwd", "image/x-xwindowdump"},
        };

        public static string GetMimeType(string extension)
        {
            if (extension == null)
            {
                throw new ArgumentNullException("extension");
            }

            string mime;

            return _mappings.TryGetValue(extension, out mime) ? mime : ".jpeg";
        }
        #endregion




        #region OTP  

        private IRestResponse SendPhoneOTPResetPw(string otp, string referKey, string phone)
        {
            string _text = phone.Substring(0, 1);
            if (_text == "0")
            {
                phone = "66" + phone.Remove(0, 1);
            }
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.movider.co/v1/sms");
            RestRequest request = new RestRequest();
            request.AddParameter("api_key", _configuration["movider:apikey"]);
            request.AddParameter("api_secret", _configuration["movider:apisecret"]);
            request.AddParameter("from", "MVDVERIFY");
            var message = new StringBuilder();
            message.AppendLine("Your EatEat password reset verification code is " + otp);
            message.AppendLine("which expires in 30 minutes. Do not share it with anyone.");
            message.AppendLine(referKey);
            request.AddParameter("text", message.ToString());
            request.Method = Method.POST;
            return client.Execute(request);
        }

        #endregion




        private dynamic APIGoogleDistance(string origin, string destinations)
        {
            var result = 0;
            var key = _configuration["google:key"];
            var apiurl = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + origin + "&destinations=" + destinations + "&language=jp&region=JP&key=" + key;
            try
            {
                var api = new HttpClient();
                var _data = JsonConvert.DeserializeObject<dynamic>(api.GetAsync(apiurl).Result.Content.ReadAsStringAsync().Result);
                if (_data.status == "OK")
                    result = _data.rows[0].elements[0].distance.value;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return result;
        }


        private IRestResponse APIGoogleFindAddress(string lat, string lng)
        {
            var key = _configuration["google:key"];
            var client = new RestClient("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=AIzaSyDEl58gghxIBCixVvW2XRQUs1MfOlwuq6Q");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            return response;
        }

        private IRestResponse GetStatusOrderDelivery(string OrderRef , string OrderNO)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://www.ohothailand.com/OhoDelivery/api/orders/OrderDetails?orderRef=" + OrderRef + "&orderNo=" + OrderNO);
            RestRequest request = new RestRequest();
            request.AddHeader("Content-Type", "application/json");
            request.Method = Method.GET;
            IRestResponse response = client.Execute(request);
            return response;
        }


        private IRestResponse CallDeliveryFee(DeliveryInfo _data)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://ohothailand.com/OhoDelivery/api/orders/CalculateDeliveryRates");
            RestRequest request = new RestRequest();
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(_data);
            request.Method = Method.POST;
            IRestResponse response = client.Execute(request);
            return response;
        }   

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<ResultLoginCustomer>> GetLogin(LoginCustomer data)
        {
            ResultLoginCustomer resData = new ResultLoginCustomer();
            CustomerInfo _data = new CustomerInfo();
            string guid = Guid.NewGuid().ToString("n");
            bool logStatus = false;

            try
            {
                _data = await _repository.GetLoginCustomer(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetLogin");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
                return Unauthorized("username or password incorrect");

            try
            {
                logStatus = await _repository.PostLogCustomerLogin(_data, guid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetLogin");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (logStatus == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = BuildToken(_data, guid);
            }
            return resData;

        }

        [AllowAnonymous]
        [HttpPost("signup")]
        public async Task<ActionResult<Result>> PostRegister(SignUpCustomer data)
        {
            Result resData = new Result();
            int status = 0;
            try
            {
                status = await _repository.PostRegister(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PostRegister");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (status == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Email or Phone number is duplicate";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Password length must have at least 8 character";
                resData.Results = null;
            }
            else
            {
                var logindata = new LoginCustomer();
                logindata.Username = data.Email;
                logindata.Password = data.Password;
                CustomerInfo _data = new CustomerInfo();
                bool logStatus = false;
                string guid = Guid.NewGuid().ToString("n");
                try
                {
                    _data = await _repository.GetLoginCustomer(logindata);
                    logStatus = await _repository.PostLogCustomerLogin(_data, guid);
                    //if (_data.Email != null)
                    //SendOTPWelcomeMessage(_data);
                }
                catch (Exception ex)
                {
                    var lineNoti = new StringBuilder();
                    lineNoti.Append(" Customer PostRegister");
                    lineNoti.AppendLine(" ");
                    lineNoti.AppendLine(ex.Message);
                    SystemNotification.SendLineNotify(lineNoti.ToString());
                    return BadRequest(ex.Message);
                }

                if (logStatus == false)
                {
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                }
                else
                {
                    //await SendNotiWarning(_data.CUS_ID);
                    resData.StatusCode = (int)(StatusCodes.Succuss);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                    resData.Results = BuildToken(_data, guid);
                }
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("gettoken")]
        public async Task<ActionResult<Result>> PostCheckGuild(CustomerGuest data)
        {
            Result resData = new Result();
            CustomerGuestInfo _data;

            try
            {
                _data = await _repository.PostCheckGuild(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PostCheckGuild");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "uid is deny";
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = BuildTokenGuest(_data);
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpGet("otpResetPassword")]
        public async Task<ActionResult<Result>> GetOtpResetPw(string EmailOrPhone)
        {
            Result resData = new Result();
            CustomerInfo _data;
            OtpSMSCustomer reqData = new OtpSMSCustomer();
            Random generator = new Random();
            RandomGenerator generatorString = new RandomGenerator();
            DateTime expDate = DateTime.UtcNow.AddHours(9).AddMinutes(30);
            bool status = false;
            string OTP = generator.Next(0, 999999).ToString("D6");
            string referenceKey = generatorString.RandomString(6);

            try
            {
                _data = await _repository.GetCustomerByPhoneOrEmail(EmailOrPhone);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetOtpResetPw");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "email or phonenumber invalid";
                return resData;
            }

            reqData.CUS_ID = _data.CUS_ID;
            reqData.Reference_Key = referenceKey;
            reqData.OTP = OTP;
            reqData.ExpireDate = expDate;

            try
            {
                status = await _repository.OtpResetPw(reqData);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetOtpResetPw");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                var _customer = await _repository.GetCustomerInfo(_data.CUS_ID);
                if (IsValidEmail(EmailOrPhone) == true)
                {
                    SendOTPMessageResetPw(OTP, referenceKey, _customer);
                }
                else
                {
                    SendPhoneOTPResetPw(OTP, referenceKey, _data.PhoneNumber);
                }
                ResultOtp resOtp = new ResultOtp();
                resOtp.Reference_Key = referenceKey;
                resOtp.ExpireDate = expDate;
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = resOtp;
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("checkOtpResetPw")]
        public async Task<ActionResult<Result>> GetCheckOtpResetPw(CheckOtpResetPw data)
        {
            Result resData = new Result();
            int status = 0;
            try
            {
                status = await _repository.GetCheckOtpResetPw(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetCheckOtpResetPw");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "change password success";

            }
            else if (status == 1)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP expired";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP invalid";
            }
            return resData;
        }

        [HttpGet("appLanguage")]
        public async Task<ActionResult<Result>> GetAppLanguage()
        {
            Result resData = new Result();
            AppLanguage _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetAppLanguage(res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetAppLanguage");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
            }
            resData.Results = _data;
            return resData;
        }

        [HttpPut("appLanguage")]
        public async Task<ActionResult<Result>> PutAppLanguage(AppLanguage data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutAppLanguage(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PutAppLanguage");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }

        [HttpGet("customerNoti")]
        public async Task<ActionResult<Result>> GetCustomerNoti(string Device_ID)
        {
            Result resData = new Result();
            CustomerNoti _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cus_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetCustomerNoti(Device_ID, cus_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetCustomerNoti");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpPut("customerNoti")]
        public async Task<ActionResult<Result>> PutCustomerNoti(CustomerNoti data)
        {
            Result resData = new Result();
            CustomerNoti _data;
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cus_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetCustomerNoti(data.Device_ID, cus_id);
                if (_data == null)
                    status = await _repository.PostCustomerNoti(data, cus_id);
                else
                    status = await _repository.PutCustomerNoti(data, cus_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PutCustomerNoti");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data saved";
            }
            return resData;
        }

        [HttpGet("customerInfo")]
        public async Task<ActionResult<Result>> GetCustomerInfo()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int nameId = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            string keyname = identity.FindFirst("mykey").Value;
            Result resData = new Result();
            CustomerInfo resInfo;

            try
            {
                resInfo = await _repository.GetCustomerInfo(nameId);
                resInfo.ImagePath = _configuration["imagePath:customer"] + resInfo.Image;
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetCustomerInfo");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex);
            }

            if (resInfo == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
            }
            resData.Results = resInfo;
            return resData;
        }

        [HttpPut("customerInfo")]
        public async Task<ActionResult<Result>> PutCustomerInfo(CustomerInfo data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int nameId = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutCustomerInfo(data, nameId);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PutCustomerInfo");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }

        [HttpPut("changePassword")]
        public async Task<ActionResult<Result>> PutCustomerPw(CustomerPassword data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int nameId = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutCustomerPw(data, nameId);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PutCustomerPw");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "password invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "password updated";
            }
            return resData;
        }

        [HttpPost("uploadImage"), DisableRequestSizeLimit]
        public async Task<ActionResult<Result>> PostuploadImage([FromForm] UploadProfileImage data)
        {
            string errMsg = "";
            bool status = false;
            Result resData = new Result();
            ResultUploadImage result = new ResultUploadImage();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                var folderName = "Images/Customer";
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                var cusdata = await _repository.GetCustomerInfo(cusid);
                if (cusdata.Image != "user.jpg")
                {
                    var fullPath = Path.Combine(pathToSave, cusdata.Image);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                }
                var file = data.Image;
                if (file.Length > 0)
                {
                    var contentType = GetMimeType(file.ContentType);
                    var _fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var digit = _fileName.IndexOf(".");
                    string fileName = "CUS-" + cusid + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + contentType;
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    result.FileName = fileName;
                    result.ImagePath = _configuration["imagePath:customer"] + fileName;
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PostuploadImage");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                errMsg = ex.Message;
            }

            try
            {
                status = await _repository.PutCustomerImage(result.FileName, cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PostuploadImage");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (errMsg != "" || status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.Messages = errMsg;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.Messages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = result;
            }

            return resData;
        }


        #region Customer Address

        [HttpGet("customerAddress")]
        public async Task<ActionResult<Result>> GetAddressCustomer()
        {
            Result resData = new Result();
            List<AddressCustomer> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                listResult = await _repository.GetAddressCustomer(cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetAddressCustomer");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex);
            }

            if (listResult.Where(x => x.ID > 0).Count() == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = listResult;
            }
            return resData;
        }

        [HttpGet("customerAddressByID")]
        public async Task<ActionResult<Result>> GetAddressCustomerByID(int ID)
        {
            Result resData = new Result();
            AddressCustomer data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                data = await _repository.GetAddressCustomerByID(cusid,ID);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetAddressCustomer");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex);
            }

            if (data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = data;
            }
            return resData;
        }

        [HttpGet("recentAddress")]
        public async Task<ActionResult<Result>> GetRecentAddress()
        {
            Result resData = new Result();
            List<AddressCustomer> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                listResult = await _repository.GetRecentAddress(cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetAddressCustomer");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex);
            }

            if (listResult.Where(x => x.ID > 0).Count() == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = listResult;
            }
            return resData;
        }

        [HttpGet("recentAddressByID")]
        public async Task<ActionResult<Result>> GetRecentAddressByID(int ID)
        {
            Result resData = new Result();
            AddressCustomer listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                listResult = await _repository.GetRecentAddressByID(ID,cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetAddressCustomer");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex);
            }

            if (listResult == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = listResult;
            }
            return resData;
        }

        [HttpPost("customerAddress")]
        public async Task<ActionResult<Result>> PostCustomerAddress(AddressCustomer data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int nameId = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostCustomerAddress(data, nameId);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PostCustomerAddress");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data saved";
            }
            return resData;
        }

        [HttpPut("customerAddress")]
        public async Task<ActionResult<Result>> PutCustomerAddress(AddressCustomer data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int nameId = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutCustomerAddress(data, nameId);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PutCustomerAddress");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }

        [HttpDelete("customerAddress")]
        public async Task<ActionResult<Result>> DeleteCustomerAddress(int Address_ID)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.DeleteCustomerAddress(Address_ID, cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer DeleteCustomerAddress");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data deleted";
            }
            return resData;
        }


        #endregion

        #region Search Restaurant

        [HttpPut("customerGPSLocation")]
        public async Task<ActionResult<Result>> PutCustomerLocation(CustomerGps data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutCustomerLocation(data,cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PutCustomerLocation");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpGet("cuisine")]
        public async Task<ActionResult<Result>> GetAllCuisine()
        {
            Result resData = new Result();
            List<Cuisine> cuisine;
            var identity = HttpContext.User.Identity as ClaimsIdentity;

            try
            {
                cuisine = await _repository.GetAllCuisine();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            if (cuisine == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = cuisine;
            return resData;
        }

        [HttpGet("allRestaurant")]
        public async Task<ActionResult<Result>> GetAllRestaurant(OrderType OrderType, string Name, string StartTime, string EndTime, string SortBy, int CuisineType)
        {
            Result resData = new Result();
            List<RestaurantData> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            string Day = DateTime.Now.DayOfWeek.ToString();

            try
            {
                listResult = await _repository.GetAllRestaurant(OrderType, Name, StartTime, EndTime, Day, cusid, CuisineType);
                if (SortBy != null && SortBy == "Favorite")
                {
                    listResult = listResult.OrderByDescending(x => x.IsClosed).Where(x => x.IsFavorite == true).ToList();
                }

                else if (SortBy != null && SortBy == "Pricing")
                {
                    listResult = listResult.OrderByDescending(x => x.IsClosed).ThenBy(x => x.Price).ToList();
                }

                else if (SortBy != null && SortBy == "Distance")
                {
                    listResult = listResult.OrderByDescending(x => x.IsClosed).ThenBy(x => x.Distance).ToList();
                }
                else
                {
                    listResult = listResult.OrderByDescending(x => x.IsClosed).ToList();

                }
                foreach (var item in listResult)
                {
                    if (item.Logo != null)
                    {
                        item.LogoPath = _configuration["imagePath:restaurant"] + item.Logo;
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            if (listResult.Count() == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = listResult;
                resData.Messages = "Don't have any restaurant.";
            }
            else if (listResult != null)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = listResult;
                resData.Messages = "Show all restauarant.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is error.";

            }
            return resData;
        }


        [HttpGet("allRestaurant2")]
        public async Task<ActionResult<Result>> GetAllRestaurant2(OrderType OrderType, string Name, string StartTime, string EndTime, string SortBy, int CuisineType)
        {
            Result resData = new Result();
            List<RestaurantData> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            string Day = DateTime.Now.DayOfWeek.ToString();

            try
            {
                listResult = await _repository.GetAllRestaurant2(OrderType, Name, StartTime, EndTime, Day, cusid, CuisineType);
                if (SortBy != null && SortBy == "Favorite")
                {
                    listResult = listResult.OrderByDescending(x => x.Status).Where(x => x.IsFavorite == true).ToList();
                }

                else if (SortBy != null && SortBy == "Pricing")
                {
                    listResult = listResult.OrderByDescending(x => x.Status).ThenBy(x => x.Price).ToList();
                }

                else if (SortBy != null && SortBy == "Distance")
                {
                    listResult = listResult.OrderByDescending(x => x.Status).ThenBy(x => x.Distance).ToList();
                }
                else
                {
                    listResult = listResult.OrderByDescending(x => x.Status).ToList();

                }
                foreach (var item in listResult)
                {
                    if (item.Logo != null)
                    {
                        item.LogoPath = _configuration["imagePath:restaurant"] + item.Logo;
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            if (listResult.Count() == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = listResult;
                resData.Messages = "Don't have any restaurant.";
            }
            else if (listResult != null)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = listResult;
                resData.Messages = "Show all restauarant.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is error.";

            }
            return resData;
        }


        [HttpGet("restaurantByID")]
        public async Task<ActionResult<Result>> GetRestaurant(int RES_ID)
        {
            Result resData = new Result();
            RestaurantData restaurant;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cus_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            string Day = DateTime.Now.DayOfWeek.ToString();


            try
            {
                restaurant = await _repository.GetRestaurantByID(RES_ID, Day, cus_id);
                if (restaurant != null)
                {
                    restaurant.LogoPath = restaurant.Logo != null ? _configuration["imagePath:restaurant"] + restaurant.Logo : null;
                }

            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetRestaurant");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (restaurant == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = restaurant;
            }
            return resData;
        }
        [HttpPut("favoriteRestaurant")]
        public async Task<ActionResult<Result>> PutFavoriteRestaurant(FavoriteRestauarnt data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutFavoriteRestaurant(data, cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer Favorite Restaurant");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is error.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is success.";
            }
            return resData;
        }

        [HttpGet("menuCategoryByResID")]
        public async Task<ActionResult<Result>> GetMenuCategoryByResID(int RES_ID)
        {
            Result resData = new Result();
            List<ViewMenuGroup> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cus_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                listResult = await _repository.GetMenuCategoryByResID(RES_ID);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetMenuCategoryByResID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (listResult.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = listResult;
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = listResult;
            }
            return resData;
        }
        [HttpGet("menuItemByResID")]
        public async Task<ActionResult<Result>> GetMenuItemByResID(int RES_ID)
        {
            Result resData = new Result();
            List<ViewMenuItem> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cus_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                listResult = await _repository.GetMenuItemByResID(RES_ID);
                foreach (var item in listResult)
                {
                    item.ImageMPath = item.ImageM != null ? _configuration["imagePath:restaurantItem"] + item.ImageM : null;
                    if (item.Discount > 0)
                    {
                        item.Total = item.Price - (item.Price * item.Discount / 100);
                    }
                }
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetMenuItemByResID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (listResult.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = listResult;
            }
            return resData;
        }

        [HttpGet("menuItemByID")]
        public async Task<ActionResult<Result>> GetMenuItemByID(int MenuItem_ID)
        {
            Result resData = new Result();
            ViewMenuItem result;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cus_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                result = await _repository.GetMenuItemByID(MenuItem_ID);
                if (result != null)
                {
                    result.ImageMPath = result.ImageM != null ? _configuration["imagePath:restaurantItem"] + result.ImageM : null;
                    if (result.Discount > 0)
                    {
                        result.Total = result.Price - (result.Price * result.Discount / 100);
                    }
                }
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetMenuItemByResID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (result == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = result;
            }
            return resData;
        }


        [HttpPost("order")]
        public async Task<ActionResult<Result>> PostOrder(CreateOrder data)
        {
            Result resData = new Result();
            int orderid = 0;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            ViewCustomerOrderDetail listResult;
            RestaurantData restaurant;
            string Day = DateTime.Now.DayOfWeek.ToString();

            try
            {
                #region Check Status Restaurant Is Sold?
                restaurant = await _repository.GetRestaurantByID(data.RES_ID, Day, cusid);
                if (restaurant.Status == RestaurantStatusActive.SoldOut)
                {
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                    resData.Messages = "restaurant is sold out.";
                    return resData;
                }
                #endregion

                orderid = await _repository.PostOrder(data, cusid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            if (orderid == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {

                listResult = await _repository.GetOrderByID(orderid, cusid);
                if (data.Order_Type == OrderType_Customer.Delivery && data.Address != null)
                {             
                    listResult.OrderID = data.orderID;
                }

                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = listResult;
            }
            return resData;
        }

        [HttpPut("changeStatusOrder")]
        public async Task<ActionResult<Result>> ChangeStatusOrder(ChangeOrderStatus data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.ChangeStatusOrder(data, cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer Cancel Order");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Update status order is error.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Update status order is success.";
            }
            return resData;
        }


        [HttpPut("cancelOrder")]
        public async Task<ActionResult<Result>> PutFavoriteRestaurant(CancelOrder data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutCancelOrder(data.Order_ID, cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer Cancel Order");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Cancel order is error.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Cancel order is success.";
            }
            return resData;
        }


        [HttpGet("orderByID")]
        public async Task<ActionResult<Result>> GetOrderByID(int ORDER_ID)
        {
            Result resData = new Result();
            ViewCustomerOrderDetail _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            string Day = DateTime.Now.DayOfWeek.ToString();


            try
            {
                _data = await _repository.GetOrderByID(ORDER_ID, cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetOrderByID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            try
            {
                if (_data != null)
                {
                    _data.Restaurant = await _repository.GetRestaurantByID(_data.RES_ID, Day, cusid);
                    if (_data.Restaurant != null)
                        _data.Restaurant.LogoPath = _data.Restaurant.Logo != null ? _configuration["imagePath:restaurant"] + _data.Restaurant.Logo : null;
                }

            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetOrderByID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }


            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("orderByOrderNo")]
        public async Task<ActionResult<Result>> GetOrderNo(string Order_NO)
        {
            Result resData = new Result();
            List<ViewCustomerOrderDetail> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            string Day = DateTime.Now.DayOfWeek.ToString();


            try
            {
                _data = await _repository.GetOrderByOrderNO(Order_NO, cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetOrderNo");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            try
            {
                if (_data != null)
                {
                    foreach (var item in _data)
                    {
                        item.Restaurant = await _repository.GetRestaurantByID(item.RES_ID, Day, cusid);
                        if (item.Restaurant != null)
                            item.Restaurant.LogoPath = item.Restaurant.Logo != null ? _configuration["imagePath:restaurant"] + item.Restaurant.Logo : null;
                    }

                }

            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetOrderByID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }


            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = _data;
            }
            return resData;
        }




        [AllowAnonymous]
        [HttpGet("allCoupon")]
        public async Task<ActionResult<Result>> CustomerAllCoupon()
        {
            Result resData = new Result();
            List<CouponDetail> result;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                result = await _repository.GetAllCoupon(cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetMenuItemByResID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (result != null)
            {
                foreach (var item in result)
                {
                    if (DateTime.Now > item.ExpirationDate)
                    {
                        item.IsExp = true;
                    }
                    else
                    {
                        item.IsExp = false;
                    }
                }
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Show all coupon detail.";
                resData.Results = result;
            }
            else if (result.Count() > 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Data is null.";
                resData.Results = result;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is error.";
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("saveCoupon")]
        public async Task<ActionResult<Result>> CustomerSaveCoupon(SaveCoupon data)
        {
            Result resData = new Result();
            int status = 0;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.SaveCoupon(data, cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetMenuItemByResID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == 1)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Add coupon is success.";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "You already have a coupon.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is error.";
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("couponDetail")]
        public async Task<ActionResult<Result>> GetCouponDetail(Coupon data)
        {
            Result resData = new Result();
            CouponDetail result;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                result = await _repository.GetCouponDetail(data, cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetMenuItemByResID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (result != null)
            {
                if (result.IsExp == true)
                {
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                    resData.Messages = "Coupon expired.";
                }
                else if (data.HasCoupon == true)
                {
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                    resData.Messages = "You already have a coupon.";
                }
                else
                {
                    resData.StatusCode = (int)(StatusCodes.Succuss);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                    resData.Messages = "Show coupon detail.";
                    resData.Results = result;
                }
            }

            else if (result == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = result;
                resData.Messages = "Not found.";
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("checkCouponForRestaurant")]
        public async Task<ActionResult<Result>> CheckCouponForRestaurant(CheckCoupon data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.CheckCouponForRestaurant(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetMenuItemByResID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Coupon can use for this restaurant.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Coupon can't use for this restaurant.";
            }
            return resData;
        }





        #endregion



        [HttpPost("deliveryFee")]
        public async Task<ActionResult<Result>> GetDeliveryFee(DeliveryInfo data)
        {
            Result resData = new Result();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);


            data.customerInfo.customerID = cusid.ToString();
            IRestResponse resDeliveryFee = CallDeliveryFee(data);
            var obj = JsonConvert.DeserializeObject<DeliveryDetail>(resDeliveryFee.Content);



            if (obj == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Get data is success.";
                resData.Results = obj;
            }
            return resData;

        }

        [HttpGet("currentOrder")]
        public async Task<ActionResult<Result>> GetCurrentOrder()
        {
            Result resData = new Result();
            List<ViewCustomerOrderDetail> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            string Day = DateTime.Now.DayOfWeek.ToString();

            try
            {
                listResult = await _repository.GetCurrentOrder(cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetCurrentOrder");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (listResult.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                foreach (var item in listResult)
                {
                    item.Restaurant = await _repository.GetRestaurantByID(item.RES_ID, Day, cusid);
                    if (item.Restaurant != null)
                        item.Restaurant.LogoPath = item.Restaurant.Logo != null ? _configuration["imagePath:restaurant"] + item.Restaurant.Logo : null;
                }


                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = listResult;
            }

            return resData;
        }

        [HttpGet("historyOrder")]
        public async Task<ActionResult<Result>> GetHistoryOrder()
        {
            Result resData = new Result();
            List<ViewCustomerOrderDetail> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            string Day = DateTime.Now.DayOfWeek.ToString();


            try
            {
                listResult = await _repository.GetHistoryOrder(cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetCurrentOrder");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (listResult.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {

                foreach (var item in listResult)
                {
                    item.Restaurant = await _repository.GetRestaurantByID(item.RES_ID, Day, cusid);
                    if (item.Restaurant != null)
                        item.Restaurant.LogoPath = item.Restaurant.Logo != null ? _configuration["imagePath:restaurant"] + item.Restaurant.Logo : null;
                }

                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = listResult;
            }

            return resData;
        }



        [HttpGet("reasonReport")]
        public async Task<ActionResult<Result>> GetReasonReport()
        {
            Result resData = new Result();
            List<ReasonReport> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                listResult = await _repository.GetReasonReport();
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetCurrentOrder");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (listResult.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = listResult;
            }

            return resData;
        }

        [HttpPost("reportOrderByID")]
        public async Task<ActionResult<Result>> ReportOrderByID([FromForm] ReportOrder data, [FromForm] IFormFileCollection Image)
        {
            Result resData = new Result();
            int reportID = 0;
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            int no = 0;

            try
            {
                reportID = await _repository.PostReport(data, cusid);

                foreach (var item in Image)
                {
                    no += 1;
                    var folderName = "Images/Customer/Report";
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                    var file = item;
                    if (file.Length > 0)
                    {
                        var contentType = GetMimeType(file.ContentType);
                        string fileName = "Report-" + reportID + "-CUS-" + cusid + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + no + contentType;
                        var fullPath = Path.Combine(pathToSave, fileName);
                        var dbPath = Path.Combine(folderName, fileName);

                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                        status = await _repository.PostImageReport(fileName, reportID, data.Order_ID);

                    }
                    else
                    {
                        return BadRequest();
                    }

                }
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Report is success.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is error";
            }
            return resData;
        }

        [HttpPost("changeStatusDeliveryToComplete")]
        public async Task<ActionResult<Result>> PostOrderRate(ChangeStatusDeliveryToComplete data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.ChangeStatusDeliveryComplete(data, cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PostOrderRate");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is error.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Update status delivery to complete is success.";
            }
            return resData;
        }

        [HttpPost("orderRate")]
        public async Task<ActionResult<Result>> PostOrderRate(OrderRate data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int cusid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostOrderRate(data, cusid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PostOrderRate");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data saved";
            }
            return resData;
        }



        [AllowAnonymous]
        [HttpGet("findAddress")]
        public async Task<ActionResult<Result>> FindAddress(string lat, string lng)
        {
            Result resData = new Result();
            string name = null;
            try
            {
                IRestResponse resGoogleAPI = APIGoogleFindAddress(lat, lng);
                JObject jsonGoogleAPI = JObject.Parse(resGoogleAPI.Content);
                name = (string)jsonGoogleAPI["results"][0]["address_components"][2]["long_name"];

            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer PostOrderRate");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (name != null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Get data is success.";
                resData.Results = name;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Data is error";

            }
            return resData;
        }


        [AllowAnonymous]
        [HttpGet("updateStatusOrderDelivery")]
        public async Task<ActionResult<Result>> UpdateStatusOrderDelivery()
        {
            bool update = false;
            Result resData = new Result();
            List<OrderTypeDeliery> data = new List<OrderTypeDeliery>();
            try
            {
                data = await _repository.GetOrderTypeDelivery();
                foreach (var item in data)
                {
                    int status = 0;
                    IRestResponse resOrderStatus = GetStatusOrderDelivery(item.OrderRef, item.OrderNO);
                    JObject jsonOrderStatus = JObject.Parse(resOrderStatus.Content);
                    var statusName = (string)jsonOrderStatus["status"];

                    if (statusName == "ON_GOING")
                    {
                        status = 1;
                    }
                    else if (statusName == "PICKED_UP")
                    {
                        status = 4;
                    }
                    else if (statusName  == "COMPLETED")
                    {
                        status = 20;
                    }
                    else if (statusName == "CANCELED")
                    {
                        status = 100;
                    }
                    else if (statusName == "EXPIRED")
                    {
                        status = 94;
                    }
                    update = await _repository.UpdateStatusOrderTypeDelivery(item.OrderNO, status);                  
                }
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer UpdateStatusOrderDelivery");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (update == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Update status is success.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Update status is error";

            }



            return resData;
        }


    }
}
