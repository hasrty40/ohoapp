﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OhoApp.DTOs;
using OhoApp.Models;
using OhoApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using StatusCodes = OhoApp.Models.StatusCodes;

namespace OhoApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AnalyticController : ControllerBase
    {

        private readonly IAnalyticData _repository;
        private readonly IConfiguration _configuration;

        public AnalyticController(IAnalyticData repository, IConfiguration configuration)
        {
            this._repository = repository;
            this._configuration = configuration;
        }


        [HttpGet("totalRevenueDaily")]
        public async Task<ActionResult<Result>> GetTotalRevenueDaily()
        {
            Result resData = new Result();
            List<RevenueAndOrder> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetTotalRevenueDaily(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("totalRevenueWeekly")]
        public async Task<ActionResult<Result>> GetTotalRevenueWeekly()
        {
            Result resData = new Result();
            List<RevenueAndOrder> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetTotalRevenueWeekly(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("totalRevenueMonthly")]
        public async Task<ActionResult<Result>> GetTotalRevenueMonthly()
        {
            Result resData = new Result();
            List<RevenueAndOrder> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetTotalRevenueMonthly(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }
        [HttpGet("pickupRevenueDaily")]
        public async Task<ActionResult<Result>> GetPickupRevenueDaily()
        {
            Result resData = new Result();
            List<RevenueAndOrder> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetPickupRevenueDaily(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("pickupRevenueWeekly")]
        public async Task<ActionResult<Result>> GetPickupRevenueWeekly()
        {
            Result resData = new Result();
            List<RevenueAndOrder> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetPickupRevenueWeekly(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("pickupRevenueMonthly")]
        public async Task<ActionResult<Result>> GetPickupRevenueMonthly()
        {
            Result resData = new Result();
            List<RevenueAndOrder> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetPickupRevenueMonthly(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("deliveryRevenueDaily")]
        public async Task<ActionResult<Result>> GetDeliveryRevenueDaily()
        {
            Result resData = new Result();
            List<RevenueAndOrder> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetDeliveryRevenueDaily(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("deliveryRevenueWeekly")]
        public async Task<ActionResult<Result>> GetDeliveryRevenueWeekly()
        {
            Result resData = new Result();
            List<RevenueAndOrder> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetDeliveryRevenueWeekly(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("deliveryRevenueMonthly")]
        public async Task<ActionResult<Result>> GetDeliveryRevenueMonthly()
        {
            Result resData = new Result();
            List<RevenueAndOrder> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetDeliveryRevenueMonthly(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("dineInRevenueDaily")]
        public async Task<ActionResult<Result>> GetDineInRevenueDaily()
        {
            Result resData = new Result();
            List<RevenueAndOrder> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetDineInRevenueDaily(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("dineInRevenueWeekly")]
        public async Task<ActionResult<Result>> GetDineInRevenueWeekly()
        {
            Result resData = new Result();
            List<RevenueAndOrder> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetDineInRevenueWeekly(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("dineInRevenueMonthly")]
        public async Task<ActionResult<Result>> GetDineInRevenueMonthly()
        {
            Result resData = new Result();
            List<RevenueAndOrder> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetDineInRevenueMonthly(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("popularItemDaily")]
        public async Task<ActionResult<Result>> GetPopularItemDaily()
        {
            Result resData = new Result();
            List<PopularItem> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetPopularItemDaily(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("popularItemWeekly")]
        public async Task<ActionResult<Result>> GetPopularItemWeekly()
        {
            Result resData = new Result();
            List<PopularItem> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetPopularItemWeekly(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("popularItemMonthly")]
        public async Task<ActionResult<Result>> GetPopularItemMonthly()
        {
            Result resData = new Result();
            List<PopularItem> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetPopularItemMonthly(resid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (_data.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data is null";
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }
    }
}
