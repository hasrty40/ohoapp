﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using OhoApp.Models;
using OhoApp.Services;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static OhoApp.Controllers.DefaultController;
using StatusCodes = OhoApp.Models.StatusCodes;

namespace OhoApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RestaurantController : ControllerBase
    {
        private readonly IRestaurantData _repository;
        private readonly IConfiguration _configuration;

        public RestaurantController(IRestaurantData repository, IConfiguration configuration)
        {
            this._repository = repository;
            this._configuration = configuration;
        }

        #region Email

        private IRestResponse SendOTPWelcomeMessage(RestaurantInfo restaurant)
        {
            RestClient client = new RestClient();

            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api", _configuration["mailgun:key"]);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", _configuration["mailgun:domain"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", _configuration["mailgun:mailname"]);

            request.AddParameter("to", restaurant.Email);
            request.AddParameter("template", "welcome-email-restaurant");

            EmailWelcomeRestaurant _email = new EmailWelcomeRestaurant();
            _email.from = new EmailFrom();
            _email.footer = new EmailFooter();

            if (restaurant.AppLanguage == AppLanguageSelect.English)
            {
                request.AddParameter("subject", "Oho");
                _email.topic = "Dear " + restaurant.RestaurantNameEN ?? restaurant.RestaurantNameTH;
                _email.title1 = "Thank you!!";
                _email.title2 = "For register restaurant with Oho App, Please waiting for approval from admin.";
                _email.from.title = "Best,";
            }
            else
            {
                request.AddParameter("subject", "Oho");
                _email.topic = restaurant.RestaurantNameTH;
                _email.title1 = "ขอบคุณ！";
                _email.title2 = "สำหรับการสมัครร้านอาหารกับ Oho App, กรุณารอการยืนยันจากแอดมิน";
                _email.from.title = "ขอแสดงความนับถือ！";
            }

            _email.from.name = "The Oho team";
            string _template = "{\"topic\": \"" + _email.topic + "\", \"title1\": \"" + _email.title1 + "\", \"title2\": \"" + _email.title2 + "\", " +
            " \"from\": {\"title\": \"" + _email.from.title + "\",\"name\": \"" + _email.from.name + "\"}}";
            request.AddParameter("h:X-Mailgun-Variables", _template);
            request.Method = Method.POST;
            return client.Execute(request);
        }


        #endregion

        #region BuildToken


        private UserToken BuildToken(LoginRestaurant resInfo, int id, string guid)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, resInfo.EmailOrPhone),
                new Claim(ClaimTypes.NameIdentifier, id.ToString()),
                new Claim(ClaimTypes.Email, resInfo.EmailOrPhone),
                new Claim(ClaimTypes.Sid, guid),
                new Claim("mykey", _configuration["JWT:mykeyRestaurant"])
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.UtcNow.AddYears(1);

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: null,
                audience: null,
                claims: claims,
                expires: expiration,
                signingCredentials: creds);

            return new UserToken()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expiration
            };
        }

        #endregion

        #region OTP

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private IRestResponse SendPhoneOTP(string otp, string referKey, string phone)
        {           
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.movider.co/v1/sms");
            RestRequest request = new RestRequest();
            request.AddParameter("api_key", _configuration["movider:apikey"]);
            request.AddParameter("api_secret", _configuration["movider:apisecret"]);
            request.AddParameter("from", "MVDVERIFY");
            request.AddParameter("to", "66" + phone.Substring(1,9));
            var message = new StringBuilder();
            message.AppendLine("Your Oho verification code is " + otp);
            message.AppendLine("it expires in 30 minutes. Do not share it with anyone.");
            message.AppendLine("REF : " + referKey);
            request.AddParameter("text", message.ToString());
            request.Method = Method.POST;
            return client.Execute(request);
        }

        private IRestResponse SendOTPMessage(string otp, string referKey, string email, RestaurantInfo restaurant)
        {
            RestClient client = new RestClient();
            EmailOTP _email = new EmailOTP();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =  new HttpBasicAuthenticator("api", _configuration["mailgun:key"]);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", _configuration["mailgun:domain"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", _configuration["mailgun:mailname"]);
            request.AddParameter("subject", "Sign UP");
            request.AddParameter("to", email);
            request.AddParameter("template", "email-otp");
            _email.button = new EmailOTPBotton();
            _email.from = new EmailFrom();
            _email.topic = "Dear " + "User Oho";
            _email.title1 = "You are receiving this email because you signup for your Oho account.";
            _email.title2 = "If this is not the case then please disregard this email, otherwise please use this verification code to signup.";
            _email.button.otp = otp;
            _email.title3 = "Thanks for supporting my Oho!";
            _email.from.title = "Best,";
            _email.from.name = "The Oho Team";

            string _template = "{\"topic\": \"" + _email.topic + "\", \"title1\": \"" + _email.title1 + "\", \"title2\": \"" + _email.title2 + "\", " +
           " \"button\": {\"otp\": \"" + _email.button.otp + "\"}, " +
           " \"from\": {\"title\": \"" + _email.from.title + "\",\"name\": \"" + _email.from.name + "\"}}";
            request.AddParameter("h:X-Mailgun-Variables", _template);

            request.Method = Method.POST;
            return client.Execute(request);
        }

        private IRestResponse SendOTPMessageResetPw(string otp, string referKey, RestaurantInfo restaurant)
        {
            RestClient client = new RestClient();
            EmailForgotPassword _email = new EmailForgotPassword();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api", _configuration["mailgun:key"]);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", _configuration["mailgun:domain"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", _configuration["mailgun:mailname"]);

            request.AddParameter("to", restaurant.Email);
            request.AddParameter("template", "forgot-password");

            _email.button = new EmailForgotPasswordBotton();
            _email.customerInfo = new EmailForgotPasswordCustomerInfo();
            _email.from = new EmailFrom();
            _email.footer = new EmailFooter();

            if (restaurant.AppLanguage == AppLanguageSelect.English)
            {
                request.AddParameter("subject", "Forgot Password");
                _email.topic = "Dear " + restaurant.RestaurantNameEN ?? restaurant.RestaurantNameTH;
                _email.title1 = "You are receiving this email because you requested a password reset for your Oho account.";
                _email.title2 = "If this is not the case then please disregard this email, otherwise please use this verification code to reset your password.";
                _email.button.otp = otp;
                _email.customerInfo.title = "Your Username is: ";
                _email.customerInfo.name = restaurant.Email;
                _email.title3 = "Thanks for supporting your local restaurants!";
                _email.from.title = "Best,";
            }
            else
            {
                request.AddParameter("subject", "Oho - パスワードのリセット");
                _email.topic = restaurant.RestaurantNameTH;
                _email.title1 = "Ohoアカウントについて、パスワードのリセットのリクエストがありました。下記のリンクをクリックし、操作を続けてください。";
                _email.title2 = "リセットのリクエストをされた覚えがない場合はこのメールを無視してください。";
                _email.button.otp = otp;
                _email.customerInfo.title = "ユーザーネーム: ";
                _email.customerInfo.name = restaurant.Email;
                _email.title3 = "Ohoをご利用頂きどうもありがとうございます。";
                _email.from.title = "今後ともよろしくお願いいたします！";
            }

            _email.from.name = "The Oho team";
            _email.footer.address = "";
            string _template = "{\"topic\": \"" + _email.topic + "\", \"title1\": \"" + _email.title1 + "\", \"title2\": \"" + _email.title2 + "\", " +
            " \"button\": {\"otp\": \"" + _email.button.otp + "\"}, " +
            " \"customerInfo\": {\"title\": \"" + _email.customerInfo.title + "\",\"name\": \"" + _email.customerInfo.name + "\"}, " +
            " \"title3\": \"" + _email.title3 + "\", " +
            " \"from\": {\"title\": \"" + _email.from.title + "\",\"name\": \"" + _email.from.name + "\"}, " +
            " \"footer\": {\"address\": \"" + _email.footer.address + "\"}} ";
            request.AddParameter("h:X-Mailgun-Variables", _template);
            request.Method = Method.POST;
            return client.Execute(request);
        }

        private IRestResponse SendPhoneOTPResetPw(string otp, string referKey, string phone)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.movider.co/v1/sms");
            RestRequest request = new RestRequest();
            request.AddParameter("api_key", _configuration["movider:apikey"]);
            request.AddParameter("api_secret", _configuration["movider:apisecret"]);
            request.AddParameter("from", "MVDVERIFY");
            request.AddParameter("to", "66" + phone.Substring(1,9));
            var message = new StringBuilder();
            message.AppendLine("Your Oho password reset verification code is " + otp);
            message.AppendLine("which expires in 30 minutes. Do not share it with anyone.");
            message.AppendLine("REF:" + referKey);
            request.AddParameter("text", message.ToString());
            request.Method = Method.POST;
            return client.Execute(request);
        }

        #endregion

        #region list of mime types
        private static IDictionary<string, string> _mappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) {
        {".art", "image/x-jg"},
        {".bmp", "image/bmp"},
        {".cmx", "image/x-cmx"},
        {".cod", "image/cis-cod"},
        {".dib", "image/bmp"},
        {".gif", "image/gif"},
        {".ico", "image/x-icon"},
        {".ief", "image/ief"},
        {".jfif", "image/pjpeg"},
        {".jpe", "image/jpeg"},
        {".jpeg", "image/jpeg"},
        {".jpg", "image/jpeg"},
        {".mac", "image/x-macpaint"},
        {".pbm", "image/x-portable-bitmap"},
        {".pct", "image/pict"},
        {".pgm", "image/x-portable-graymap"},
        {".pic", "image/pict"},
        {".pict", "image/pict"},
        {".png", "image/png"},
        {".pnm", "image/x-portable-anymap"},
        {".pnt", "image/x-macpaint"},
        {".pntg", "image/x-macpaint"},
        {".pnz", "image/png"},
        {".ppm", "image/x-portable-pixmap"},
        {".qti", "image/x-quicktime"},
        {".qtif", "image/x-quicktime"},
        {".ras", "image/x-cmu-raster"},
        {".rf", "image/vnd.rn-realflash"},
        {".rgb", "image/x-rgb"},
        {".tif", "image/tiff"},
        {".tiff", "image/tiff"},
        {".wbmp", "image/vnd.wap.wbmp"},
        {".wdp", "image/vnd.ms-photo"},
        {".xbm", "image/x-xbitmap"},
        {".xpm", "image/x-xpixmap"},
        {".xwd", "image/x-xwindowdump"},
        };

        public static string GetMimeType(string extension)
        {
            if (extension == null)
            {
                throw new ArgumentNullException("extension");
            }

            string mime;

            return _mappings.TryGetValue(extension, out mime) ? mime : ".jpeg";
        }
        #endregion


        private static bool GenerateReport()
        {
            PdfDocument report = new PdfDocument();
            PdfPage objPage = report.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(objPage);
            // Create a font
            XFont font1 = new XFont("Verdana", 15, XFontStyle.Bold);
            XFont font2 = new XFont("Tahoma", 8, XFontStyle.Bold);
            XFont font3 = new XFont("Tahoma", 8, 0);


            // Draw the text
            gfx.DrawString("My Customer", font1, XBrushes.Black,
            new XRect(0, 50, objPage.Width.Point, objPage.Height.Point), XStringFormats.TopCenter);

            // Draw the text
            gfx.DrawString("CustomerID", font2, XBrushes.Black, 65, 80, XStringFormats.TopLeft);
            gfx.DrawString("Name", font2, XBrushes.Black, 140, 80, XStringFormats.TopLeft);
            gfx.DrawString("Email", font2, XBrushes.Black, 215, 80, XStringFormats.TopLeft);
            gfx.DrawString("CountryCode", font2, XBrushes.Black, 365, 80, XStringFormats.TopLeft);
            gfx.DrawString("Budget", font2, XBrushes.Black, 425, 80, XStringFormats.TopLeft);
            gfx.DrawString("Used", font2, XBrushes.Black, 485, 80, XStringFormats.TopLeft);

            // Line
            XPen pen = new XPen(XColor.FromArgb(0, 0, 0));

            gfx.DrawLine(pen, new XPoint(65, 78), new XPoint(520, 78));
            gfx.DrawLine(pen, new XPoint(65, 90), new XPoint(520, 90));

            int intLine = 90;
            int i;

            for (i = 0; i <= 2 - 1; i++)
            {
                gfx.DrawString("Test", font3, XBrushes.Black, 65, intLine, XStringFormats.TopLeft);
                gfx.DrawString("Test", font3, XBrushes.Black, 140, intLine, XStringFormats.TopLeft);
                gfx.DrawString("Test", font3, XBrushes.Black, 215, intLine, XStringFormats.TopLeft);
                gfx.DrawString("Test", font3, XBrushes.Black, 365, intLine, XStringFormats.TopLeft);
                gfx.DrawString("Test", font3, XBrushes.Black, 425, intLine, XStringFormats.TopLeft);
                gfx.DrawString("Test", font3, XBrushes.Black, 485, intLine, XStringFormats.TopLeft);
                intLine = intLine + 10;
            }


            var folderName = Path.Combine("Images", "PDF");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            string itemname = "Test.pdf";
            var fullPath = Path.Combine(pathToSave, itemname);


            // Save the document...

            report.Save(fullPath);

            report.Close();

            return true;



        }






        #region Login 
        [AllowAnonymous]
        [HttpPost("signup")]
        public async Task<ActionResult<ResultLoginRestaurant>> PostRegister(SignUpRestaurant data)
        {
            ResultLoginRestaurant resData = new ResultLoginRestaurant();
            resData.Results = new ResultRestaurant();
            string guid = Guid.NewGuid().ToString("n");
            int status = 0;

            try
            {
                status = await _repository.PostRegister(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" Restaurant PostRegister");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Email or PhoneNumber is duplicate";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Password length must have at least 8 character";
                resData.Results = null;
            }
            else
            {
                var logindata = new LoginRestaurant();
                logindata.EmailOrPhone = data.Email;
                logindata.Password = data.Password;
                LoginStatusRestaurant resStatus = new LoginStatusRestaurant();
                try
                {
                    resStatus = await _repository.GetLoginRestaurant(logindata);
                    var _restaurant = await _repository.GetRestaurantByID(resStatus.RES_ID);
                    await _repository.CreateDeliveryFee(resStatus.RES_ID);
                    //await SendNotiWarning(resStatus.RES_ID);
                    //SendOTPWelcomeMessage(_restaurant);
                }
                catch (Exception ex)
                {
                    var lineNoti = new StringBuilder();
                    lineNoti.Append(" Restaurant PostRegister");
                    lineNoti.AppendLine(" ");
                    lineNoti.AppendLine(ex.Message);
                    SystemNotification.SendLineNotify(lineNoti.ToString());
                    return BadRequest(ex.Message);
                }
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results.RestaurantStatus = (String)EnumString.GetStringValue(RestaurantStatus.WaitInfo);
                resData.Results.Token = BuildToken(logindata, resStatus.RES_ID, guid);
            }
            return resData;
        }


        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<ResultLoginRestaurant>> GetLogin([FromBody] LoginRestaurant data)
        {
            ResultLoginRestaurant resData = new ResultLoginRestaurant();
            LoginStatusRestaurant resStatus = new LoginStatusRestaurant();
            bool logStatus = false;
            string guid = Guid.NewGuid().ToString("n");
            resData.Results = new ResultRestaurant();
            try
            {
                resStatus = await _repository.GetLoginRestaurant(data);
                if (resStatus != null)
                    logStatus = await _repository.PostLogRestaurantLogin(resStatus.RES_ID, guid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.AppendLine(" Restaurant GetLogin");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (resStatus == null || logStatus == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "username or password incorrect";
            }
            else
            {
                if (resStatus.StatusRestaurant == false)
                {
                    resData.Results.RestaurantStatus = (String)EnumString.GetStringValue(RestaurantStatus.WaitInfo);
                }
                else if (resStatus.StatusApproval == false)
                {
                    resData.Results.RestaurantStatus = (String)EnumString.GetStringValue(RestaurantStatus.WaitApproval);
                }
                else if (resStatus.StatusOTP == false)
                {
                    resData.Results.RestaurantStatus = (String)EnumString.GetStringValue(RestaurantStatus.WaitOtp);
                }
                else if (resStatus.StatusService == false)
                {
                    resData.Results.RestaurantStatus = (String)EnumString.GetStringValue(RestaurantStatus.WaitService);
                }
                else
                {
                    resData.Results.RestaurantStatus = (String)EnumString.GetStringValue(RestaurantStatus.Verify);
                }
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results.Token = BuildToken(data, resStatus.RES_ID, guid);
            }
            return resData;

        }

        [HttpPost("changePassword")]
        public async Task<ActionResult<Result>> PostChangePassword(ChangePwRestaurant data)
        {
            Result resData = new Result();
            int status;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostChangePassword(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PostChangePassword");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "password updated";

            }
            else if (status == 1)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "password invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            return resData;
        }


        [HttpGet("restaurantInfo")]
        public async Task<ActionResult<Result>> GetRestaurant()
        {
            Result resData = new Result();
            RestaurantInfo restaurant;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                restaurant = await _repository.GetRestaurantByID(res_id);
                restaurant.LogoPath = restaurant.Logo != null ? _configuration["imagePath:restaurant"] + restaurant.Logo : null;
                restaurant.ContactAdmin = "0812345678";
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetRestaurant");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (restaurant == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
            }
            resData.Results = restaurant;
            return resData;
        }


        [HttpPut("restaurantInfo")]
        public async Task<ActionResult<Result>> PutRestaurantInfo(RestaurantInfo data)
        {
            Result resData = new Result();
            int status = 0;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            data.RES_ID = res_id;
            try
            {
                status = await _repository.PutRestaurantInfo(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutRestaurantInfo");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == 10)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            else if (status == 1)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "email or phone duplicate";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "placeid duplicate";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            return resData;
        }

        [HttpPost("uploadProfileImage"), DisableRequestSizeLimit]
        public async Task<ActionResult<Result>> PostuploadImageAsync([FromForm] UploadProfileImage data)
        {
            string errMsg = "";
            bool status = false;
            Result resData = new Result();
            ResultUploadImage result = new ResultUploadImage();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                var file = data.Image;
                var folderName = Path.Combine("Images", "Restaurant");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                var _data = await _repository.GetRestaurantByID(resid);
                if (_data == null)
                    return BadRequest();
                if (_data.Logo != null)
                {
                    var fullPath = Path.Combine(pathToSave, _data.Logo);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                }
                var contentType = GetMimeType(file.ContentType);
                if (file.Length > 0)
                {
                    string itemname = "RES-" + resid + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + contentType;
                    var fullPath = Path.Combine(pathToSave, itemname);
                    var dbPath = Path.Combine(folderName, itemname);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    result.FileName = itemname;
                    result.ImagePath = _configuration["imagePath:restaurant"] + itemname;
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PostuploadImageAsync");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                errMsg = ex.Message;
            }

            try
            {
                status = await _repository.PutRestaurantProfileImage(result, resid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PostuploadImageAsync");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (errMsg != "" || status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = errMsg;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = result;
            }

            return resData;
        }

        [HttpPost("sendEmail")]
        public async Task<ActionResult<Result>> PostSendEmail()
        {
            Result resData = new Result();
            RestaurantInfo restaurant;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                restaurant = await _repository.GetRestaurantByID(resid);
                SendOTPWelcomeMessage(restaurant);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (restaurant != null)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);

            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            return resData;
        }


        [HttpPost("uploadItemImage"), DisableRequestSizeLimit]
        public async Task<ActionResult<Result>> PostuploadItemImage([FromForm] UploadItemImage data)
        {
            string errMsg = "";
            bool status = false;
            Result resData = new Result();
            ResultUploadItemImage result = new ResultUploadItemImage();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                var file = data.Image;
                var folderName = Path.Combine("Images", "Restaurant", "Items");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                var itemdata = await _repository.GetMenuItemByID(data.MenuItem_ID, resid);
                if (itemdata == null)
                    return BadRequest("Item is null");
                if (itemdata.ImageM != null)
                {
                    var fullPath = Path.Combine(pathToSave, itemdata.ImageM);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                }

                var contentType = GetMimeType(file.ContentType);
                if (file.Length > 0)
                {
                    string itemname = "RES-" + resid + "-" + itemdata.ID + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + contentType;
                    var fullPath = Path.Combine(pathToSave, itemname);
                    var dbPath = Path.Combine(folderName, itemname);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    result.MenuItem_ID = itemdata.ID;
                    result.FileName = itemname;
                    result.ImagePath = _configuration["imagePath:restaurantItem"] + itemname;
                }
                else
                {
                    return BadRequest("Upload failed");
                }
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PostuploadImageAsync");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                errMsg = ex.Message;
            }

            try
            {
                status = await _repository.PutRestaurantItemImage(result);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PostuploadImageAsync");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (errMsg != "" || status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = errMsg;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = result;
            }

            return resData;
        }



        [AllowAnonymous]
        [HttpGet("serviceCharge")]
        public async Task<ActionResult<Result>> GetServiceCharge()
        {
            Result resData = new Result();
            List<ServiceCharge> serviceCharge = new List<ServiceCharge>();
            try
            {
                serviceCharge = await _repository.GetServiceCharge();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            if (serviceCharge == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
            }
            resData.Results = serviceCharge;
            return resData;
        }


        [AllowAnonymous]
        [HttpGet("cuisine")]
        public async Task<ActionResult<Result>> GetCuisine()
        {
            Result resData = new Result();
            List<Cuisine> cuisine = new List<Cuisine>();
            try
            {
                cuisine = await _repository.GetAllCuisine();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            if (cuisine == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
            }
            resData.Results = cuisine;
            return resData;
        }


        [HttpGet("otpSMS")]
        public async Task<ActionResult<Result>> GetOtpSMS()
        {
            Result resData = new Result();
            RestaurantInfo restaurant;
            OtpSMSRestaurant reqData = new OtpSMSRestaurant();
            Random generator = new Random();
            RandomGenerator generatorString = new RandomGenerator();
            DateTime expDate = DateTime.Now.AddMinutes(30);
            bool status = false;
            string OTP = generator.Next(0, 9999).ToString("D4");
            string referenceKey = generatorString.RandomString(6);
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            reqData.RES_ID = res_id;
            reqData.Reference_Key = referenceKey;
            reqData.OTP = OTP;
            reqData.ExpireDate = expDate;

            try
            {
                status = await _repository.GetOtpSMS(reqData);
                restaurant = await _repository.GetRestaurantByID(res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetOtpSMS");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false || restaurant == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                try
                {
                    SendPhoneOTP(OTP, referenceKey, restaurant.PhoneNumber);
                }
                catch (Exception ex)
                {
                    var lineNoti = new StringBuilder();
                    lineNoti.Append(" Restaurant GetOtpSMS");
                    lineNoti.AppendLine(" ");
                    lineNoti.AppendLine(ex.Message);
                    SystemNotification.SendLineNotify(lineNoti.ToString());
                    return BadRequest(ex.Message);
                }
                ResultOtpSMS resOtp = new ResultOtpSMS();
                resOtp.Phone = restaurant.PhoneNumber;
                resOtp.Reference_Key = referenceKey;
                resOtp.ExpireDate = expDate;
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = resOtp;
            }
            return resData;
        }

        [HttpPost("checkOtpSMSRegister")]
        public async Task<ActionResult<Result>> GetCheckOtpSMSRegister(CheckOtp data)
        {
            Result resData = new Result();
            int status;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.GetCheckOtpSMSRegister(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetCheckOtpSMSRegister");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "OTP valid";

            }
            else if (status == 1)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP expired";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP invalid";
            }
            return resData;
        }

        [HttpGet("otpEmail")]
        public async Task<ActionResult<Result>> GetOtpEmail()
        {
            Result resData = new Result();
            RestaurantInfo restaurant;
            OtpEmailRestaurant reqData = new OtpEmailRestaurant();
            Random generator = new Random();
            RandomGenerator generatorString = new RandomGenerator();
            DateTime expDate = DateTime.UtcNow.AddHours(9).AddMinutes(30);
            bool status = false;
            string OTP = generator.Next(0, 999999).ToString("D6");
            string referenceKey = generatorString.RandomString(6);
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            reqData.RES_ID = res_id;
            reqData.Token_ID = referenceKey;
            reqData.OTP = OTP;
            reqData.ExpireDate = expDate;

            try
            {
                status = await _repository.GetOtpEmail(reqData);
                restaurant = await _repository.GetRestaurantByID(res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetOtpEmail");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false || restaurant == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                ResultOtp resOtp = new ResultOtp();
                resOtp.Reference_Key = referenceKey;
                resOtp.ExpireDate = expDate;
                SendOTPMessage(OTP, referenceKey, restaurant.Email, restaurant);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = resOtp;
            }
            return resData;
        }

        [HttpPost("checkOtpEmailRegister")]
        public async Task<ActionResult<Result>> GetCheckOtpEmailRegister(CheckOtp data)
        {
            Result resData = new Result();
            int status;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.GetCheckOtpEmailRegister(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetCheckOtpEmailRegister");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "OTP valid";

            }
            else if (status == 1)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP expired";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP invalid";
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpGet("otpResetPassword")]
        public async Task<ActionResult<Result>> GetOtpResetPw(string EmailOrPhone)
        {
            Result resData = new Result();
            RestaurantInfo restaurant;
            OtpSMSRestaurant reqData = new OtpSMSRestaurant();
            Random generator = new Random();
            RandomGenerator generatorString = new RandomGenerator();
            DateTime expDate = DateTime.UtcNow.AddHours(9).AddMinutes(30);
            int status = 0;
            IRestResponse _status;
            string OTP = generator.Next(0, 999999).ToString("D6");
            string referenceKey = generatorString.RandomString(6);

            try
            {
                restaurant = await _repository.GetRestaurantByPhoneOrEmail(EmailOrPhone);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetOtpResetPw");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (restaurant == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "email or phonenumber invalid";
                return resData;
            }

            reqData.RES_ID = restaurant.RES_ID;
            reqData.Reference_Key = referenceKey;
            reqData.OTP = OTP;
            reqData.ExpireDate = expDate;

            try
            {
                status = await _repository.OtpResetPw(reqData);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetOtpResetPw");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Send OTP not success";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Your account not approve. Please waiting approval from admin. ";
            }
            else
            {
                try
                {
                    if (IsValidEmail(EmailOrPhone) == true)
                    {
                        _status = SendOTPMessageResetPw(OTP, referenceKey, restaurant);

                    }
                    else
                    {
                        _status = SendPhoneOTPResetPw(OTP, referenceKey, restaurant.PhoneNumber);
                    }
                }
                catch (Exception ex)
                {
                    var lineNoti = new StringBuilder();
                    lineNoti.Append(" Restaurant GetOtpResetPw");
                    lineNoti.AppendLine(" ");
                    lineNoti.AppendLine(ex.Message);
                    SystemNotification.SendLineNotify(lineNoti.ToString());
                    return BadRequest(ex.Message);
                }

                ResultOtp resOtp = new ResultOtp();
                resOtp.Reference_Key = referenceKey;
                resOtp.ExpireDate = expDate;
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Send OTP is success.";
                resData.Results = resOtp;
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("checkOtpResetPw")]
        public async Task<ActionResult<Result>> GetCheckOtpResetPw(CheckOtpResetPw data)
        {
            Result resData = new Result();
            int status = 0;
            try
            {
                status = await _repository.GetCheckOtpResetPw(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetCheckOtpResetPw");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "change password success";

            }
            else if (status == 1)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP expired";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP invalid";
            }
            return resData;
        }

        [HttpGet("restaurantStatus")]
        public async Task<ActionResult<Result>> GetRestaurantStatus()
        {
            Result resData = new Result();
            LoginStatusRestaurant restaurant;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                restaurant = await _repository.GetRestaurantStatus(res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetRestaurantStatus");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (restaurant == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                CheckRestaurantStatus status = new CheckRestaurantStatus();

                if (restaurant.StatusRestaurant == false)
                {
                    status.RestaurantStatus = (String)EnumString.GetStringValue(RestaurantStatus.WaitInfo);
                }
                else if (restaurant.StatusApproval == false)
                {
                    status.RestaurantStatus = (String)EnumString.GetStringValue(RestaurantStatus.WaitApproval);
                }
                else if (restaurant.StatusOTP == false)
                {
                    status.RestaurantStatus = (String)EnumString.GetStringValue(RestaurantStatus.WaitOtp);
                }
                else if (restaurant.StatusService == false)
                {
                    status.RestaurantStatus = (String)EnumString.GetStringValue(RestaurantStatus.WaitService);
                }
                else
                {
                    status.RestaurantStatus = (String)EnumString.GetStringValue(RestaurantStatus.Verify);
                }

                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = status;
            }
            return resData;
        }

        [HttpGet("appLanguage")]
        public async Task<ActionResult<Result>> GetAppLanguage()
        {
            Result resData = new Result();
            AppLanguage _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetAppLanguage(res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetAppLanguage");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
            }
            resData.Results = _data;
            return resData;
        }

        [HttpPut("appLanguage")]
        public async Task<ActionResult<Result>> PutAppLanguage(AppLanguage data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutAppLanguage(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutAppLanguage");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }


        #endregion


        #region Opening Hour
        [HttpGet("openingHour")]
        public async Task<ActionResult<Result>> GetOpeningHour()
        {
            Result resData = new Result();
            OpeningHour data = new OpeningHour();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                data = await _repository.GetOpeningHour(res_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (data != null)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = data;
                resData.Messages = "Get data is Success";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is null";
            }

            return resData;
        }

        [HttpPost("openingHour")]
        public async Task<ActionResult<Result>> PostOpeningHour(OpeningHour data)
        {
            Result resData = new Result();
            int status = 0;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostOpeningHour(data, res_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == 1)
            {
                status = await _repository.PostCategory(res_id);

                if (status == 1)
                {
                    resData.StatusCode = (int)(StatusCodes.Succuss);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                    resData.Messages = "Add opening hour is success.";
                }
                else
                {
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                    resData.Messages = "Data is Error.";
                }

            }

            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Update opening hour is success.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is Error.";
            }

            return resData;
        }

        [HttpDelete("openingHour")]
        public async Task<ActionResult<Result>> DeleteOpeningHour()
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.DeleteOpeningHour(res_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Delete opening hours success.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Can't delete this menu group.r";
            }

            return resData;
        }


        #endregion


        #region TEST

        [HttpPut("verifyOTP")]
        public async Task<ActionResult<Result>> PutVerifyOTP(PutStatusOTP data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutStatusOTP(data, res_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Verify OTP success.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is Error";
            }

            return resData;
        }
        [HttpPut("verifyApproval")]
        public async Task<ActionResult<Result>> PutApproval(PutStatusOTP data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutStatusApprove(data, res_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Approval is success.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is Error";
            }

            return resData;
        }

        [HttpPut("resetRestaurantStatus")]
        public async Task<ActionResult<Result>> PutResetRestaurantStatus()
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            RestaurantInfo data = new RestaurantInfo();

            try
            {
                status = await _repository.ResetRestaurantInfo(data, res_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Reset restaurant status is success.";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is Error";
            }

            return resData;
        }


        #endregion


        #region Menu
        [HttpGet("allmenuhours")]
        public async Task<ActionResult<Result>> GetAllMenuHour()
        {
            Result resData = new Result();
            List<ViewMenuHour> menuhours;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                menuhours = await _repository.GetAllMenuHour(res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetAllMenuHour");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (menuhours == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = menuhours;
            }
            return resData;
        }

        [HttpGet("menugroupByID")]
        public async Task<ActionResult<Result>> GetMenuGroupByID(int MenuGroup_ID)
        {
            Result resData = new Result();
            ViewMenuGroup _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetMenuGroupByID(MenuGroup_ID, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetMenuGroupByID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = _data;
            return resData;
        }

        [HttpPost("menugroup")]
        public async Task<ActionResult<Result>> PostMenuGroup(ViewMenuGroup data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostMenuGroup(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PostMenuGroup");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data saved";
            }
            return resData;
        }
        [HttpPut("menugroup")]
        public async Task<ActionResult<Result>> PutMenuGroup(ViewMenuGroup data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutMenuGroup(data);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutMenuGroup");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }

        [HttpDelete("menugroup")]
        public async Task<ActionResult<Result>> DeleteMenuGroup(int MenuGroup_ID)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.DeleteMenuGroup(MenuGroup_ID);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant DeleteMenuGroup");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data deleted";
            }
            return resData;
        }

        [HttpGet("menuItemByMenuGroupID")]
        public async Task<ActionResult<Result>> GetMenuItemByMenuGroupID(int MenuGroup_ID)
        {
            Result resData = new Result();
            List<ViewMenuItem> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                listResult = await _repository.GetMenuItemByMenuGroupID(MenuGroup_ID, res_id);
                foreach (var item in listResult)
                {

                    if (item.MenuGroup_Name == "Oho Surprise Box" || item.MenuGroup_Name == "Oho! Surprise Box")
                    {
                        item.No = 1;
                        item.Section = "surpriseBox";
                        item.ImageMPath = _configuration["imagePath:surprisebox"] + "Oho.png";
                    }
                    else if (item.Status == true)
                    {
                        item.No = 2;
                        item.Section = "saleNow";
                        item.ImageMPath = item.ImageM != null ? _configuration["imagePath:restaurantItem"] + item.ImageM : _configuration["imagePath:restaurantItem"] + "emptyIMG.png";
                    }
                    else if (item.Status == false)
                    {
                        item.No = 3;
                        item.Section = "saleLater";
                        item.ImageMPath = item.ImageM != null ? _configuration["imagePath:restaurantItem"] + item.ImageM : _configuration["imagePath:restaurantItem"] + "emptyIMG.png";
                    }

                    item.Repeat = await _repository.GetRepeatMenuItemByID(item.ID);


                    if (item.Discount > 0)
                    {
                        item.Total = item.Price - (item.Price * item.Discount / 100);
                    }
                }


            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetMenuItemByMenuGroupID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (listResult == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = listResult.OrderBy(x => x.No);
            }
            return resData;
        }

        [HttpGet("menuItemByID")]
        public async Task<ActionResult<Result>> GetMenuItemByID(int MenuItem_ID)
        {
            Result resData = new Result();
            ViewMenuItem _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetMenuItemByID(MenuItem_ID, res_id);


                if (_data != null)
                {
                    if (_data.Discount > 0)
                    {
                        _data.Total = _data.Price - (_data.Price * _data.Discount / 100);
                    }
                    _data.ImageMPath = _data.ImageM != null ? _configuration["imagePath:restaurantItem"] + _data.ImageM : _configuration["imagePath:restaurantItem"] + "emptyIMG.png";
                    _data.Repeat = await _repository.GetRepeatMenuItemByID(_data.ID);
                }


            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetMenuItemByID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("createItemByMenuGroupID")]
        public async Task<ActionResult<Result>> GetCreateMenuItem(int MenuGroup_ID)
        {
            Result resData = new Result();
            ViewMenuItem _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetCreateMenuItem(MenuGroup_ID, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetCreateMenuItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }


        [HttpPut("menuItem")]
        public async Task<ActionResult<Result>> PutMenuItem(EditMenuItem data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutMenuItem(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutMenuItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }

        [HttpDelete("menuItem")]
        public async Task<ActionResult<Result>> DeleteMenuItem(int MenuItem_ID)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.DeleteMenuItem(MenuItem_ID, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant DeleteMenuItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data deleted";
            }
            return resData;
        }

        [HttpPut("putMenuItemRepeat")]
        public async Task<ActionResult<Result>> PutMenuItemRepeat(DataMenuItem data)
        {
            bool status = false;
            Result resData = new Result();
            try
            {
                if (data.Discount == 0 || data.Discount < 25)
                {
                    data.Discount = 25;
                }
                else if (data.Discount > 100)
                {
                    data.Discount = 100;
                }
                status = await _repository.PutRepeatMenuItem(data);

            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            return resData;
        }


        [HttpPut("serviceRepeatQuotaItem")]
        public async Task<ActionResult<Result>> PutRepeatQuotaItem()
        {
            bool status = false;
            Result resData = new Result();
            List<DataRepeat> data = new List<DataRepeat>();
            string day = DateTime.Now.DayOfWeek.ToString();
            try
            {
                data = await _repository.GetDataRepeatMenuItem(day);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (data != null)
            {
                foreach (var item in data)
                {
                    status = await _repository.PutRepeatQuotaItem(item.MenuItem_ID, item.DefaultQuota);
                }
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            return resData;
        }


        [HttpGet("Addon")]
        public async Task<ActionResult<Result>> GetAddOn()
        {
            Result resData = new Result();
            List<ViewAddOnItem> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetAllAddOn(res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetCreateAddOnItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }
        [HttpPost("Addon")]
        public async Task<ActionResult<Result>> PostAddOn(PostAddOnItem data)
        {
            Result resData = new Result();
            ViewAddOnItem _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.PostAddOn(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetCreateAddOnItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpPut("Addon")]
        public async Task<ActionResult<Result>> PutAddOn(PutAddOnItem data)
        {
            Result resData = new Result();
            ViewAddOnItem _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.PutAddOn(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetCreateAddOnItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpDelete("Addon")]
        public async Task<ActionResult<Result>> DeleteAddOn(int AddOn_ID)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.DeleteAddOn(AddOn_ID, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant DeleteAddOnItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data deleted";
            }
            return resData;
        }

        [HttpGet("AddonOption")]
        public async Task<ActionResult<Result>> GetAddOnOption(int AddOnID)
        {
            Result resData = new Result();
            List<ViewAddOnOption> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetAllAddOnOption(AddOnID, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetCreateAddOnItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }
        [HttpPost("AddonOption")]
        public async Task<ActionResult<Result>> PostAddOnOptionItem(ViewAddOnOption data)
        {
            Result resData = new Result();
            ViewAddOnOption _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.PostAddOnOption(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutAddOnOptionItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }


        [HttpPut("AddonOption")]
        public async Task<ActionResult<Result>> PutAddOnOptionItem(ViewAddOnOption data)
        {
            Result resData = new Result();
            ViewAddOnOption _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.PutAddOnOption(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutAddOnOptionItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        [HttpDelete("AddonOption")]
        public async Task<ActionResult<Result>> DeleteAddonOption(int AddOnOption_ID)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.DeleteAddOnOption(AddOnOption_ID, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant DeleteAddonOption");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data deleted";
            }
            return resData;
        }


        [HttpPut("moveMenuItem")]
        public async Task<ActionResult<Result>> PutMoveMenuItem(MoveMenuItem data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutMoveMenuItem(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutMoveMenuItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data saved";
            }
            return resData;
        }

        [HttpPut("duplicateMenuItem")]
        public async Task<ActionResult<Result>> PutDuplicateMenuItem(DuplicateMenuItem data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutDuplicateMenuItem(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutDuplicateMenuItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data saved";
            }
            return resData;
        }

        [HttpPut("duplicateAddOn")]
        public async Task<ActionResult<Result>> PutDuplicateAddOn(DuplicateAddOn data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutDuplicateAddOn(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutDuplicateMenuItem");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data saved";
            }
            return resData;
        }




        #endregion

        #region Order


        [HttpGet("currentOrder")]
        public async Task<ActionResult<Result>> GetCurrentOrder()
        {
            Result resData = new Result();
            List<ViewRestaurantrOrderDetail> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                listResult = await _repository.GetCurrentOrder(res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetCurrentOrder");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message.ToString());
            }

            if (listResult.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = listResult;
            return resData;
        }

        [HttpGet("historyOrder")]
        public async Task<ActionResult<Result>> GetHistoryOrder()
        {
            Result resData = new Result();
            List<ViewRestaurantrOrderDetail> listResult;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                listResult = await _repository.GetHistoryOrder(res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant GetCurrentOrder");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message.ToString());
            }

            if (listResult.Count == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = listResult;
            return resData;
        }


        [HttpGet("orderByID")]
        public async Task<ActionResult<Result>> GetOrderByID(int ORDER_ID)
        {
            Result resData = new Result();
            ViewRestaurantrOrderDetail _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            string Day = DateTime.Now.DayOfWeek.ToString();


            try
            {
                _data = await _repository.GetOrderByID(ORDER_ID, resid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetOrderByID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            try
            {
                if (_data != null)
                {
                    _data.Restaurant = await _repository.GetRestaurantByID(resid);
                    if (_data.Restaurant != null)
                        _data.Restaurant.LogoPath = _data.Restaurant.Logo != null ? _configuration["imagePath:restaurant"] + _data.Restaurant.Logo : null;
                }

            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetOrderByID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }


            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = _data;
            }
            return resData;
        }

        [HttpGet("orderByOrderNo")]
        public async Task<ActionResult<Result>> GetOrderNo(string Order_NO)
        {
            Result resData = new Result();
            List<ViewRestaurantrOrderDetail> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int resid = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            string Day = DateTime.Now.DayOfWeek.ToString();


            try
            {
                _data = await _repository.GetOrderByOrderNO(Order_NO, resid);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetOrderNo");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            try
            {
                if (_data != null)
                {
                    foreach (var item in _data)
                    {
                        item.Restaurant = await _repository.GetRestaurantByID(item.RES_ID);
                        if (item.Restaurant != null)
                            item.Restaurant.LogoPath = item.Restaurant.Logo != null ? _configuration["imagePath:restaurant"] + item.Restaurant.Logo : null;
                    }

                }

            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Customer GetOrderByID");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }


            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = _data;
            }
            return resData;
        }



        [HttpPut("acceptOrder")]
        public async Task<ActionResult<Result>> PutAcceptOrder(AcceptOrder data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            ViewRestaurantrOrderDetail _data;
            try
            {
                _data = await _repository.GetOrderByID(data.Order_ID, res_id);
                if (_data == null)
                {
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                    resData.Messages = "data invalid";
                }
                else if (_data.Status != OrderStatus.New)
                {
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                    resData.Messages = "order accept failed";
                }
                else
                {
                    status = await _repository.PutAcceptOrder(data, res_id);
                }
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutAcceptOrder");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                //await SendNotiOrderCustomer(data.Order_ID, res_id);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
            }
            resData.Results = _data;
            return resData;
        }

        [HttpPut("cookingDone")]
        public async Task<ActionResult<Result>> PutCookingDone(CookingOrder data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutCookingDone(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutCookingDone");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }

        [HttpPut("takeoutComplete")]
        public async Task<ActionResult<Result>> PutTakeOutComplete(PickUpCompleteOrder data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutTakeOutComplete(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutTakeOutComplete");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                //await SendNotiOrderCustomer(data.Order_ID, res_id);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }

        [HttpPut("cancelOrder")]
        public async Task<ActionResult<Result>> PutCancelOrder(CancelOrder data)
        {
            Result resData = new Result();
            ViewRestaurantrOrderDetail _dataOrder;
            RestaurantInfo _dataRestaurant;
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _dataOrder = await _repository.GetOrderByID(data.Order_ID, res_id);
                _dataRestaurant = await _repository.GetRestaurantByID(_dataOrder.RES_ID);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutCancelOrder");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            try
            {
                status = await _repository.PutCancelOrder(data, res_id);
            }
            catch (Exception ex)
            {
                var lineNoti = new StringBuilder();
                lineNoti.Append(" Restaurant PutCancelOrder");
                lineNoti.AppendLine(" ");
                lineNoti.AppendLine(ex.Message);
                SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "data updated";
            }
            return resData;
        }

        [HttpGet("listRejectOrder")]
        public async Task<ActionResult<Result>> GetListRejectOrder()
        {
            Result resData = new Result();
            List<ListReject> data = new List<ListReject>();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                data = await  _repository.GetListReject();
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (data != null || data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Get list reject is success.";
                resData.Results = data;

            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is error";

            }
            return resData;
        }

        [HttpPut("rejectOrder")]
        public async Task<ActionResult<Result>> PutRejectOrder(RejectOrder data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int res_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.RejectOrder(data,res_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Get list reject is success.";
                resData.Results = data;

            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is error";

            }
            return resData;
        }


        [AllowAnonymous]

        [HttpPost("createReportTest")]
        public async Task<ActionResult<Result>> CreateReportTest()
        {
            Result resData = new Result();
            bool status = false;
            try
            {
                status = GenerateReport();
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Create report is success.";

            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Create report is error.";

            }
            return resData;
        }
        #endregion






    }
}
