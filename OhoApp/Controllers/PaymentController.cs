﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OhoApp.Hubs;
using OhoApp.Models;
using OhoApp.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using static OhoApp.Controllers.DefaultController;
using StatusCodes = OhoApp.Models.StatusCodes;

namespace OhoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {

        private readonly IPaymentData _repository;
        private readonly IConfiguration _configuration;
        private IHubContext<ChatHub> _hub;

        public PaymentController(IPaymentData repository, IRestaurantData repositoryRestaurant, IConfiguration configuration, IHubContext<ChatHub> hub)
        {
            this._repository = repository;
            this._configuration = configuration;
            this._hub = hub;

        }

        public static string GetRandomAlphanumericStringNormal(int length)
        {
            const string alphanumericCharacters =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "abcdefghijklmnopqrstuvwxyz" +
                "0123456789";
            return GetRandomString(length, alphanumericCharacters);
        }

        public static string GetRandomAlphanumericString(int length)
        {
            const string alphanumericCharacters =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "0123456789";
            return GetRandomString(length, alphanumericCharacters);
        }

        public static string GetRandomString(int length, IEnumerable<char> characterSet)
        {
            if (length < 0)
                throw new ArgumentException("length must not be negative", "length");
            if (length > int.MaxValue / 8) // 250 million chars ought to be enough for anybody
                throw new ArgumentException("length is too big", "length");
            if (characterSet == null)
                throw new ArgumentNullException("characterSet");
            var characterArray = characterSet.Distinct().ToArray();
            if (characterArray.Length == 0)
                throw new ArgumentException("characterSet must not be empty", "characterSet");

            var bytes = new byte[length * 8];
            var result = new char[length];
            using (var cryptoProvider = new RNGCryptoServiceProvider())
            {
                cryptoProvider.GetBytes(bytes);
            }
            for (int i = 0; i < length; i++)
            {
                ulong value = BitConverter.ToUInt64(bytes, i * 8);
                result[i] = characterArray[value % (uint)characterArray.Length];
            }
            return new string(result);
        }


        private IRestResponse GenerateToken(TokenSCB DataToken )
        {
            RestClient client = new RestClient();

            client.BaseUrl = new Uri("https://api-sandbox.partners.scb/partners/sandbox/v1/oauth/token");
            RestRequest request = new RestRequest();
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("resourceOwnerId", _configuration["SCB:key"]);
            request.AddHeader("requestUId", "{{$guid}}");
            request.AddHeader("accept-language", "EN");
            request.AddJsonBody(DataToken);
            request.Method = Method.POST;
            IRestResponse response = client.Execute(request);
            return response;
        }

        private IRestResponse GenerateQRCode30(QRCode30 data,string token)
        {
            RestClient client = new RestClient();



            client.BaseUrl = new Uri("https://api-sandbox.partners.scb/partners/sandbox/v1/payment/qrcode/create");
            RestRequest request = new RestRequest();
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + token);
            request.AddHeader("resourceOwnerId", _configuration["SCB:key"]);
            request.AddHeader("requestUId", "{{$guid}}");
            request.AddHeader("accept-language", "EN");
            request.AddJsonBody(data);
            request.Method = Method.POST;
            IRestResponse response = client.Execute(request);
            return response;
        }

        private IRestResponse PlaceOrder(PlaceOrder _data)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://ohothailand.com/OhoDelivery/api/orders/PlaceOrders");
            RestRequest request = new RestRequest();
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(_data);
            request.Method = Method.POST;
            IRestResponse response = client.Execute(request);
            return response;
        }


        //API Payment






        [HttpPost("generateQRCode")]
        public async Task<ActionResult<Result>> generateQRCode(DataQRCode30 data)
        {
            Result resData = new Result();
            bool status = false;
            var key = _configuration["SCB:key"];
            var secretKey = _configuration["SCB:secretKey"];
            TokenSCB DataToken = new TokenSCB();
            QRCode30 _data = new QRCode30();
            string ref1 = GetRandomAlphanumericString(10);
            string ref2 = GetRandomAlphanumericString(10);
            string qrID = GetRandomAlphanumericStringNormal(10);
            try
            {
                DataToken.applicationKey = key;
                DataToken.applicationSecret = secretKey;
                IRestResponse resTokenSCB = GenerateToken(DataToken);
                if (resTokenSCB.StatusCode == HttpStatusCode.OK)
                {
                    var jsonTokenSCB = JsonConvert.DeserializeObject<ResultToken>(resTokenSCB.Content);
                    var token = jsonTokenSCB.data.accessToken;

                    // Generate QRCode30

                    _data.qrType = "PP";
                    _data.ppType = "BILLERID";
                    _data.ppId = "795610447133205";
                    _data.amount = data.Amount + ".00";
                    _data.ref1 = ref1;
                    _data.ref2 = ref2;
                    _data.ref3 = "AUZ";

                    IRestResponse resQRCode30 = GenerateQRCode30(_data,token);
                    var jsonQRCode30 = JsonConvert.DeserializeObject<ResultGenerateQR30>(resQRCode30.Content);
                    jsonQRCode30.data.qrID = qrID;
                    resData.Results = jsonQRCode30;

                    status = await _repository.PutDataPaymentOrder(ref1, ref2, _data.ref3, qrID, data.ID);

                    if (status == false)
                    {
                        resData.StatusCode = (int)(StatusCodes.Error);
                        resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                        resData.Messages = "Generate QRCode 30 is error.";
                    }
                    else
                    {
                        resData.StatusCode = (int)(StatusCodes.Succuss);
                        resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                        resData.Messages = "Generate QRCode 30 is success.";
                    }


                }

            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            return resData;
        }



        [HttpPost("paymentConfirm")]
        public async Task<ActionResult<ResultPaymentConfirm>> PaymentConfirm(DataConfirm data)
        {
            ResultPaymentConfirm resData = new ResultPaymentConfirm();

            var qrID = "";
            try
            {           

                qrID = await _repository.ChangeStatusPaymentOrder(data);
                await _hub.Clients.All.SendAsync(qrID, "Paid");
                var Order_Type = await _repository.CheckOrderType(qrID);
                if (Order_Type == true)
                {
                    var placeOrder = await _repository.GetPlaceOrder(qrID);
                    IRestResponse resPlaceOrder = PlaceOrder(placeOrder);
                    if (resPlaceOrder.StatusCode == HttpStatusCode.OK)
                    {
                        var lineNoti = new StringBuilder();
                        lineNoti.Append(" Customer PaymentConfirm");
                        lineNoti.AppendLine("Paid is success.");
                        SystemNotification.SendLineNotify(lineNoti.ToString());
                    }
                    else
                    {
                        var lineNoti = new StringBuilder();
                        lineNoti.Append(" Customer PaymentConfirm");
                        lineNoti.AppendLine("Paid is error.");
                        SystemNotification.SendLineNotify(lineNoti.ToString());
                    }
                }
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (data != null)
            {
                resData.resCode = "00";
                resData.ResDesc = "success";
                resData.transactionId = data.transactionId;
            }
            else
            {
                resData.resCode = "404";
                resData.ResDesc = "Bad Request.";
            }
            return resData;
        }



    }
}
