﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace OhoApp.Models
{

    public class EnumValue : System.Attribute
    {
        private string _value;
        public EnumValue(string value)
        {
            _value = value;
        }
        public string Value
        {
            get { return _value; }
        }
    }
    public enum OrderPayment
    {
        [Description("Not Add Credit")]
        NoCredit = 0,
        [Description("QRCode")]
        QRCode = 1,
        [Description("Credit Card")]
        CreditCard = 2
    }

    public enum PaymentStatus
    {
        [Description("Unpaid")]
        Unpaid = 0,
        [Description("Paid")]
        Paid = 1,
    }


    public enum CustomerOrderStatus
    {
        [Description("In Progess")]
        InProgess = 1,
        [Description("COMPLETED")]
        Complete = 20,
        [Description("REPORT")]
        Report = 96,
        [Description("REFUND")]
        Refund = 97,
        [Description("CANCELLED")]
        CustomerCancel = 98,
        [Description("CANCELLED")]
        RestaurantCancel = 99,
        [Description("CANCELLED")]
        RiderCancel = 100
    }

    public enum OrderStatus
    {
        [Description("NEW")]
        New = 1,
        [Description("COOKING")]
        Cooking = 2,
        [Description("READY")]
        Ready = 3,
        [Description("ENROUTE")]
        Enroute = 4,
        [Description("COMPLETED")]
        Complete = 20,
        [Description("EXPIRE")]
        Expire = 94,
        [Description("REPORT")]
        Report = 95,
        [Description("REJECT")]
        Reject = 96,
        [Description("REFUND")]
        Refund = 97,
        [Description("CANCELLED")]
        CustomerCancel = 98,
        [Description("CANCELLED")]
        RestaurantCancel = 99,
        [Description("CANCELLED")]
        RiderCancel = 100
    }
    public static class EnumString
    {
        public static string GetStringValue(Enum value)
        {
            string output = null;
            Type type = value.GetType();
            FieldInfo fi = type.GetField(value.ToString());
            EnumValue[] attrs = fi.GetCustomAttributes(typeof(EnumValue), false) as EnumValue[];
            if (attrs.Length > 0)
            {
                output = attrs[0].Value;
            }
            return output;
        }
    }

    public static class EnumExtension
    {
        public static string ToDescription(this System.Enum value)
        {
            try
            {
                var attributes = (DescriptionAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return attributes.Length > 0 ? attributes[0].Description : value.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
    }

    public class Location
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public enum AppLanguageSelect
    {
        [Description("English")]
        English = 1,
        [Description("Thai")]
        Thai = 2
    }


    public class AppLanguage
    {
        public AppLanguageSelect Language { get; set; }
        public string Language_Text { get { return Language.ToDescription(); } }

    }

    public enum TaxType
    {
        [Description("Inclusive")]
        Inclusive = 1,
        [Description("Exclusive")]
        Exclusive = 2,
    }

    public enum OrderType
    {
        [Description("PickUp/DineIn")]
        PickUp_DineIn = 1,
        [Description("Delivery")]
        Delivery = 2,
    }

    public enum OrderType_Customer
    {
        [Description("PickUp")]
        PickUp = 1,
        [Description("DineIn")]
        DineIn = 2,
        [Description("Delivery")]
        Delivery = 3,
    }


    public enum RestaurantStatusActive
    {
        [Description("Sold-Out")]
        SoldOut = 0,
        [Description("Open")]
        Open = 1
    }



}
