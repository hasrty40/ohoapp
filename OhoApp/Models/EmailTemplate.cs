﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OhoApp.Models
{
    public class EmailOTP
    {
        public string topic { get; set; }
        public string title1 { get; set; }
        public string title2 { get; set; }
        public EmailOTPBotton button { get; set; }
        public string title3 { get; set; }
        public EmailFrom from { get; set; }
        public EmailFooter footer { get; set; }
    }

    public class EmailOTPBotton
    {
        public string otp { get; set; }
    }
    public class EmailFrom
    {
        public string title { get; set; }
        public string name { get; set; }
    }

    public class EmailFooter
    {
        public string address { get; set; }
        public string webUrl { get; set; }
        public string facebookUrl { get; set; }
        public string instagramUrl { get; set; }
    }
    public class EmailForgotPassword
    {
        public string topic { get; set; }
        public string title1 { get; set; }
        public string title2 { get; set; }
        public EmailForgotPasswordBotton button { get; set; }
        public EmailForgotPasswordCustomerInfo customerInfo { get; set; }
        public string title3 { get; set; }
        public EmailFrom from { get; set; }
        public EmailFooter footer { get; set; }
    }

    public class EmailForgotPasswordBotton
    {
        public string otp { get; set; }
    }

    public class EmailForgotPasswordCustomerInfo
    {
        public string title { get; set; }
        public string name { get; set; }
    }

    public class EmailWelcomeRestaurant
    {
        public string topic { get; set; }
        public string title1 { get; set; }
        public string title2 { get; set; }
        public string title3 { get; set; }
        public string title4 { get; set; }
        public EmailFrom from { get; set; }
        public EmailFooter footer { get; set; }
    }


    #region Customer 

    public class EmailWelcomeCustomer
    {
        public string topic { get; set; }
        public string title1 { get; set; }
        public string title2 { get; set; }
        public string title3 { get; set; }
        public string title4 { get; set; }
        public EmailWelcomeCustomerBotton button { get; set; }
        public string title5 { get; set; }
        public EmailFrom from { get; set; }
        public EmailFooter footer { get; set; }
    }

    public class EmailWelcomeCustomerBotton
    {
        public string url { get; set; }
        public string title { get; set; }
    }


    #endregion

}
