﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OhoApp.Models
{
    public class CreateOrder
    {
        public int orderID { get; set; }
        public int logID { get; set; }
        public string distance { get; set; }
        public OrderType_Customer Order_Type { get; set; }
        public int RES_ID { get; set; }
        public CustomerDetail Customer { get; set; }
        public CustomerAddress Address { get; set; }
        public int Coupon_ID { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int DeliveryFee { get; set; }

        public List<CreateOrderItem> Items { get; set; }
    }

    public class CustomerDetail
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class CustomerAddress
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string AddressDetail { get; set; }
        public string Comment { get; set; }


    }

    public class CreateOrderItem
    {
        public int MenuItem_ID { get; set; }
        public int Amount { get; set; }
        public string Comment { get; set; }
        public List<CreateOrderItemAddOn> AddOns { get; set; }

    }

    public class CreateOrderItemAddOn
    {
        public int AddOnOption_ID { get; set; }
    }


    public class OrderDetail
    {
        public int ID { get; set; }
        public string Order_No { get; set; }
        public string OrderRef { get; set; }
        public int RES_ID { get; set; }
        public RestaurantData Restaurant { get; set; }
        public string RestaurantLocation { get; set; }
        public int CUS_ID { get; set; }
        public string Name { get; set; }
        public string CustomerLocation { get; set; }
        public decimal Distance { get; set; }
        public double DistanceKilometers { get { return (double)Distance / 1000; } }
        public OrderType_Customer Order_Type { get; set; }
        public string Order_TypeText
        {
            get
            {
                return Order_Type.ToDescription();
            }
        }
        public int CookingTime { get; set; }
        public int DriverID { get; set; }
        public int ServiceCharge { get; set; }
        public int SmallOrderFee { get; set; }
        public int DeliveryFee { get; set; }
        public int Discount { get; set; }
        public int SubTotal { get; set; }
        public int Total { get; set; }
        public CustomerOrderStatus Status { get; set; }
        public string StatusText
        {
            get
            {
                return Status.ToDescription();
            }
        }
        public OrderPayment PaymentMethod { get; set; }
        public string PaymentMethodText
        {
            get
            {
                return PaymentMethod.ToDescription();
            }
        }
        public bool PaymentStatus { get; set; }
        public OrderAddress AddressOrder { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<ViewOrderItem> Items { get; set; }
    }



    public class OrderAddress
    {
        public int ID { get; set; }
        public string CustomerName { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string Address_Detail { get; set; }
        public string Comment { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class OrderRate
    {
        public int Order_ID { get; set; }
        public int FoodRate { get; set; }
        public string FoodComment { get; set; }
        public int DriverRate { get; set; }
        public string DriverComment { get; set; }
    }

    public class ViewCustomerOrderDetail
    {
        public int ID { get; set; }
        public int OrderID { get; set; }
        public string OrderRef { get; set; }
        public string Order_No { get; set; }
        public int RES_ID { get; set; }
        public RestaurantData Restaurant { get; set; }
        public string RestaurantLocation { get; set; }
        public int CUS_ID { get; set; }
        public string CustomerLocation { get; set; }
        public int DriverID { get; set; }
        public decimal Distance { get; set; }
        public double DistanceKilometers { get { return (double)Distance / 1000; } }
        public OrderType_Customer Order_Type { get; set; }
        public string Order_TypeText
        {
            get
            {
                return Order_Type.ToDescription();
            }
        }
        public int CookingTime { get; set; }
        public int ServiceCharge { get; set; }
        public int SmallOrderFee { get; set; }
        public int DeliveryFee { get; set; }
        public int Discount { get; set; }
        public int SubTotal { get; set; }
        public int Total { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public DateTime? Accept_Time { get; set; }
        public DateTime? Cooked_Time { get; set; }
        public DateTime? Delivery_Start { get; set; }
        public DateTime? Delivery_End { get; set; }
        public DateTime? Complete_Time { get; set; }
        public OrderAddress AddressOrder { get; set; }
        public CustomerOrderStatus Status { get; set; }
        public string StatusText
        {
            get
            {
                return Status.ToDescription();
            }
        }
        public OrderPayment PaymentMethod { get; set; }
        public string PaymentMethodText
        {
            get
            {
                return PaymentMethod.ToDescription();
            }
        }
        public PaymentStatus PaymentStatus { get; set; }

        public string PaymentStatusText
        {
            get
            {
                return PaymentStatus.ToDescription();
            }
        }
        public string Payment_ID { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public OrderAddress OrderAddress { get; set; }
        public List<ViewOrderItem> Items { get; set; }
    }

    public class ViewOrderItem
    {
        public int ID { get; set; }
        public int MenuItem_ID { get; set; }
        public string Name_EN { get; set; }
        public string Name_TH { get; set; }
        public string Desc_EN { get; set; }
        public string Desc_TH { get; set; }
        public int Price { get; set; }
        public int Amount { get; set; }
        public string Comment { get; set; }
        public int MenuGroup_ID { get; set; }
        public List<ViewAddOnOrderOption> Options { get; set; }
    }

    public class ViewAddOnOrderOption
    {
        public int ID { get; set; }
        public int AddOn_ID { get; set; }
        public int AddOnOption_ID { get; set; }
        public int OrderItem_ID { get; set; }
        public string Name_EN { get; set; }
        public string Name_TH { get; set; }
        public int Price { get; set; }
    }

    public class OrderLocation
    {
        public int CUS_ID { get; internal set; }
        public string RestaurantLocation { get; set; }
        public int RES_ID { get; internal set; }
        public string CustomerLocation { get; set; }
    }



    public class ViewRestaurantrOrderDetail
    {
        public int ID { get; set; }
        public int OrderID { get; set; }
        public string OrderRef { get; set; }
        public string Order_No { get; set; }
        public int RES_ID { get; set; }
        public RestaurantInfo Restaurant { get; set; }
        public string RestaurantLocation { get; set; }
        public int CUS_ID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerLocation { get; set; }
        public decimal Distance { get; set; }
        public double DistanceKilometers { get { return (double)Distance / 1000; } }
        public OrderType_Customer Order_Type { get; set; }
        public string Order_TypeText
        {
            get
            {
                return Order_Type.ToDescription();
            }
        }
        public int CookingTime { get; set; }
        public int ServiceCharge { get; set; }
        public int SmallOrderFee { get; set; }
        public int DeliveryFee { get; set; }
        public int Discount { get; set; }
        public int SubTotal { get; set; }
        public int Total { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public DateTime? Accept_Time { get; set; }
        public DateTime? Cooked_Time { get; set; }
        public DateTime? Delivery_Start { get; set; }
        public DateTime? Delivery_End { get; set; }
        public DateTime? Complete_Time { get; set; }
        public DriverInfo DriverInfo { get; set; }
        public OrderStatus Status { get; set; }
        public string StatusText
        {
            get
            {
                return Status.ToDescription();
            }
        }
        public OrderPayment PaymentMethod { get; set; }
        public string PaymentMethodText
        {
            get
            {
                return PaymentMethod.ToDescription();
            }
        }
        public PaymentStatus PaymentStatus { get; set; }

        public string PaymentStatusText
        {
            get
            {
                return PaymentStatus.ToDescription();
            }
        }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public OrderAddress OrderAddress { get; set; }
        public OrderRate OrderRate { get; set; }
        public List<ViewOrderItem> Items { get; set; }

    }

    public class CheckOrderTypeAndOrderID
    {
        public int ID { get; set; }
        public int Order_Type { get; set; }
        public int RES_ID { get; set; }
        public int CUS_ID { get; set; }

    }



    public class ChangeStatusDeliveryToComplete
    {
        public int ID { get; set; }
        public string Order_NO { get; set; }
    }

    public class ChangeOrderStatus
    {
        public int Order_ID { get; set; }
        public int Status { get; set; }
    }


    public class DriverInfo
    {
        public int DriverID { get; set; }
        public string DriverName { get; set; }
        public string DriverPhone { get; set; }
        public string PlateNumber { get; set; }
        public string Photo { get; internal set; }       
    }

    public class AcceptOrder
    {
        public int Order_ID { get; set; }
    }

    public class CookingOrder
    {
        public int Order_ID { get; set; }
    }

    public class PickUpCompleteOrder
    {
        public int Order_ID { get; set; }
    }



    public class CancelOrder
    {
        public int Order_ID { get; set; }
    }


    public class RejectOrder
    {
        public int Order_ID { get; set; }
        public int Reason_ID { get; set; }
        //public string Other_Reason { get; set; }
    }

    public class OrderTypeDeliery
    {
        public string OrderRef { get; set; }
        public string OrderNO { get; set; }
    }


}

