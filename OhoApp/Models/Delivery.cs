﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OhoApp.Models
{
    public class DeliveryInfo
    {
        public string serviceType { get; set; }
        public string distance { get; set; }
        public Restaurant restaurantInfo { get; set; }
        public Customer customerInfo { get; set; }
    }

    public class Restaurant
    {
        public string restaurantID { get; set; }
    }


    public class Customer
    {
        public string customerID { get; internal set; }
        public string customerName { get; set; }
        public string customerMobilePhone { get; set; }
        public string customerAddress { get; set; }
        public CustomerLocation location { get; set; }
    }

    public class CustomerLocation
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class Position
    {
        public string Location { get; set; }
    }

    public class DeliveryDetail
    {
        public int orderID { get; set; }
        public int logID { get; set; }
        public string totalFee { get; set; }
        public string totalFeeCurrency { get; set; }

    }


    public class PlaceOrder
    {
        public int orderID { get; set; }
        public int logID { get; set; }
        public string orderNo { get; set; }
        public string serviceType { get; set; }
        public string distance { get; set; }
        public Restaurant restaurantInfo { get; set; }
        public Customer customerInfo { get; set; }
    }

    public class OrderPlaceDetail
    {
        public string orderRef { get; set; }
        public string orderNo { get; set; }
        public string status { get; set; }
    }

    public class DataOrder
    {
        public int ID { get; set; }
        public int RES_ID { get; set; }
        public int CUS_ID { get; set; }
    }


}
