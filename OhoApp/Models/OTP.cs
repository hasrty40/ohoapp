﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhoApp.Models
{
    public class RandomGenerator
    {
        private readonly Random _random = new Random();

        public string RandomString(int size, bool lowerCase = false)
        {
            var builder = new StringBuilder(size);

            char offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26; // A...Z or a..z: length = 26  

            for (var i = 0; i < size; i++)
            {
                var @char = (char)_random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }

    }

    public class OtpSMSRestaurant
    {
        public string Reference_Key { get; set; }
        public string OTP { get; set; }
        public int RES_ID { get; set; }
        public int Type { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpireDate { get; set; }
    }

    public class ResultOtpSMS
    {
        public string Reference_Key { get; set; }
        public DateTime ExpireDate { get; set; }
        public string Phone { get; set; }
    }

    public class CheckOtp
    {
        public string OTP { get; set; }
        public string Reference_Key { get; set; }
    }

    public class OtpEmailRestaurant
    {
        public string Token_ID { get; set; }
        public string OTP { get; set; }
        public int RES_ID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpireDate { get; set; }
    }

    public class ResultOtp
    {
        public string Reference_Key { get; set; }
        public DateTime ExpireDate { get; set; }
    }

    public class CheckOtpResetPw
    {
        public string EmailOrPhone { get; set; }
        public string NewPassword { get; set; }
        public string Reference_Key { get; set; }
        public string OTP { get; set; }
    }

    #region Customer

    public class OtpSMSCustomer
    {
        public string Reference_Key { get; set; }
        public string OTP { get; set; }
        public int CUS_ID { get; set; }
        public int Type { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpireDate { get; set; }
    }


    #endregion

}
