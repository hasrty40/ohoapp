﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OhoApp.Models
{
    public class SignUpCustomer
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }

    public class CustomerGuest
    {
        public string Guid { get; set; }
    }

    public class LoginCustomer
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class CustomerGuestInfo
    {
        public int CUS_ID { get; set; }
        public string Token { get; set; }
        public int Status { get; set; }
        public bool IsEnable { get; set; }
        public bool IsGuest { get { return Token != null; } }
    }
    public class CustomerInfo
    {
        public int CUS_ID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string Location { get;  set; }
        public string LocationAddress { get; internal set; }
        public string LocationName { get; internal set; }
        public string LocationDetail { get; internal set; }
        public string LocationNote { get; internal set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string Image { get; set; }
        public string ImagePath { get; set; }

        public string StripeID { get; set; }
        public bool IsGuest { get { return Token != null; } }
        public AppLanguageSelect AppLanguage { get; set; }
    }
    public class CustomerPassword
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }



    public class CustomerNoti
    {
        public string Device_ID { get; set; }
        public bool IsEnable { get; set; }
    }

    public class AddressCustomer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string Address_Detail { get; set; }
        public string Comment { get; set; }
        public bool DefaultSelection { get; set; }
    }

    public class CustomerGps
    {
        public string LocationName { get; set; }
        public string LocationAddress { get; set; }
        public string LocationDetail { get; set; }
        public string LocationNote { get; set; }
        public string GPS { get; set; }
    }

    public class RestaurantData
    {
        public int RES_ID { get; set; }
        public string RestaurantNameEN { get; set; }
        public string RestaurantNameTH { get; set; }
        public string Logo { get; set; }
        public string LogoPath { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string OfficialPhoneNumber { get; set; }
        public string PhoneNumber { get; set; }
        public int ServiceCharge { get; set; }
        public decimal? Price { get; set; }
        public int Discount { get; set; }
        public decimal? Total { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public decimal? Rate { get; set; }
        public int? RateCount { get; set; }
        public int? PriceRate { get; set; }
        public decimal? DeliveryDistance { get; set; }
        public decimal? Distance { get; set; }
        public double? DistanceKilometers { get { return Distance != null ? Math.Round((double)Distance / 1000 , 2) : 0;  } }
        public int? Cuisine_ID { get; set; }
        public Cuisine Cuisine { get; set; }
        public string CuisineOtherName { get; set; }
        public RestaurantService RestaurantService { get; set; }
        public RestaurantStatusActive Status { get; set; }
        public bool HasMenu { get; internal set; }
        public bool IsFavorite { get; internal set; }
        public int IsEveryDay { get; internal set; }

        public bool IsClosed { get; set; }
        public string StatusText
        {
            get
            {
                return Status.ToDescription();
            }
        }

        public bool HaveRestaurant { get; internal set; }
    }
    
    public class PriceRestaurant
    {
        public int? Price { get; set; }
        public int Discount { get; set; }
        public int? Total { get; set; }
    }

    public class OpeningHourRestaurant
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }

    public class FavoriteRestauarnt
    {
        public int RES_ID { get; set; }
    }

    public class ReasonReport
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class ReportOrder
    {
        public int Order_ID { get; set; }
        public int Reason_ID { get; set; }
        public string Description { get; set; }
    }


}
