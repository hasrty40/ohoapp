﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OhoApp.Models
{
    public class ViewMenuHour
    {
        public int ID { get; set; }
        public string Name_EN { get; set; }
        public string Name_TH { get; set; }
        public int Sequence { get; set; }
        public TaxType TaxType { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<ViewMenuGroup> MenuGroups { get; set; }
    }
    public class ViewMenuGroup
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }
        public int MenuHour_ID { get; set; }
        public bool IsAll { get; set; }
    }



    public class ViewMenuItem
    {
        public int ID { get; set; }
        public string Section { get; internal set; }
        public int No { get; internal set; }
        public int? MenuGroup_ID { get;  set; }
        public string MenuGroup_Name { get; internal set; }
        public string Name_EN { get; set; }
        public string Name_TH { get; set; }
        public string Desc_EN { get; set; }
        public string Desc_TH { get; set; }
        public int? Price { get; set; }
        public int Quota { get; internal set; }
        public int? Discount { get; internal set; }
        public int? Total { get; set; }
        public string ImageM { get; internal set; }
        public string ImageMPath { get; internal set; }
        public int? CookingTime { get; set; }
        public bool? Status { get; set; }
        public DayOfRepeat Repeat { get; set; }
        public List<ViewAddOnItem> AddOns { get; set; }
    }

    public class ViewAddOnItem
    {
        public int ID { get; set; }
        public string Name_EN { get; set; }
        public string Name_TH { get; set; }
        public bool? Require { get; set; }
        public int? OptionCount { get; set; }
        public bool? Status { get; set; }
        public bool? IsEnable { get; set; }
        public List<ViewAddOnOption> Options { get; set; }
    }

    public class ViewAddOnOption
    {
        public int ID { get;  set; }
        public int? AddOn_ID { get; set; }
        public string Name_EN { get; set; }
        public string Name_TH { get; set; }
        public int? Price { get; set; }
        public bool? Status { get; set; }
    }


    public class PostAddOnItem
    {
        public int ID { get; set; }
        public string Name_EN { get; set; }
        public string Name_TH { get; set; }
        public bool? Require { get; set; }
        public int? OptionCount { get; set; }
        public bool? Status { get; set; }
        public bool? IsEnable { get; set; }
        public List<PostAddOnOption> Options { get; set; }
    }

    public class PostAddOnOption
    {
        public string Name_EN { get; set; }
        public string Name_TH { get; set; }
        public int? Price { get; set; }
    }


    public class PutAddOnItem
    {
        public int ID { get; set; }
        public string Name_EN { get; set; }
        public string Name_TH { get; set; }
        public bool? Require { get; set; }
        public int? OptionCount { get; set; }
        public bool? Status { get; set; }
        public bool? IsEnable { get; set; }
    }

    public class DataMenuItem
    {
        public int MenuItem_ID { get; set; }
        public int Quota { get; set; }
        public int Discount { get; set; }
        public DayOfRepeat DayRepeat { get; set; }

    }

    public class DayOfRepeat
    {
        public bool IsEveryDay { get; set; }
        public bool IsSun { get; set; }
        public bool IsMon { get; set; }
        public bool IsTue { get; set; }
        public bool IsWed { get; set; }
        public bool IsThu { get; set; }
        public bool IsFri { get; set; }
        public bool IsSat { get; set; }
    }
    public class DataRepeat
    {
        public int MenuItem_ID { get; set; }
        public int DefaultQuota { get; set; }
    }

    public class EditMenuItem
    {
        public int ID { get; set; }
        public string Section { get; internal set; }
        public int? MenuGroup_ID { get; set; }
        public string MenuGroup_Name { get; internal set; }
        public string Name_EN { get; set; }
        public string Name_TH { get; set; }
        public string Desc_EN { get; set; }
        public string Desc_TH { get; set; }
        public int? Price { get; set; }
        public int? CookingTime { get; set; }
        public bool? Status { get; set; }
        public List<EditAddOn> AddOns { get; set; }
    }


    public class EditAddOn
    {
        public int AddOnID { get; set; }
        public int IsEnable { get; internal set; }
    }

    public class UploadItemImage
    {
        public int MenuItem_ID { get; set; }
        public IFormFile Image { get; set; }
    }

    public class MoveMenuItem
    {
        public int MenuItem_ID { get; set; }
        public int MenuGroup_ID { get; set; }
    }

    public class DuplicateMenuItem
    {
        public int MenuItem_ID { get; set; }
        public int MenuGroup_ID { get; set; }
    }

    public class DuplicateAddOn
    {
        public int AddOn_ID { get; set; }
    }

    public class AddOnOptionID
    {
        public int ID { get; set; }
    }

    public class menuItemCancel
    {
        public int MenuItem_ID { get; set; }
        public int Amount { get; set; }
    }


}
