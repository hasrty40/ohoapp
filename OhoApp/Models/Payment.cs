﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OhoApp.Models
{
    public class TokenSCB
    {
        public string applicationKey { get; set; }
        public string applicationSecret { get; set; }
    }

    public class ResultToken
    {
        public Status status { get; set; }
        public Token data { get; set; }
    }
    public class Status
    {
        public string code { get; set; }
        public string description { get; set; }
    }

    public class Token
    {
        public string accessToken { get; set; }
    }

    public class QRCode30
    {
        public string qrType { get; set; }
        public string ppType { get; set; }
        public string ppId { get; set; }
        public string amount { get; set; }
        public string ref1 { get; set; }
        public string ref2 { get; set; }
        public string ref3 { get; set; }
    }

    public class DataQRCode30
    {
        public int ID { get; set; }
        public string Amount { get; set; }
    }

    public class ResultQRCode30
    {
        public string qrID { get; set; }
        public string qrRawData { get; set; }
        public string qrImage { get; set; }
    }

    public class ResultGenerateQR30
    {
        public Status status { get; set; }
        public ResultQRCode30 data { get; set; }
    }

    public class DataConfirm
    {
        public string payeeProxyId { get; set; }
        public string payeeProxyType { get; set; }
        public string payeeAccountNumber { get; set; }
        public string payerAccountNumber { get; set; }
        public string payerAccountName { get; set; }
        public string payerName { get; set; }
        public string sendingBankCode { get; set; }
        public string receivingBankCode { get; set; }
        public string amount { get; set; }
        public string transactionId { get; set; }
        public DateTime transactionDateandTime { get; set; }
        public string billPaymentRef1 { get; set; }
        public string billPaymentRef2 { get; set; }
        public string billPaymentRef3 { get; set; }
        public string currencyCode { get; set; }
        public string channelCode { get; set; }
        public string transactionType { get; set; }
    }

    public class ResultPaymentConfirm
    {
        public string resCode { get; set; }
        public string ResDesc { get; set; }
        public string transactionId { get; set; }
    }


}
