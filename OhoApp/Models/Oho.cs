﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OhoApp.Models
{
    public class OhoInfo
    {
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string TaxID { get; set; }
        public string Logo { get; set; }
        public string LogoPath { get; set; }
        public string WebURL { get; set; }
        public string CustomerWebURL { get; set; }
        public string CustomerSupportPhone { get; set; }
        public string RestaurantSupportPhone { get; set; }
        public string AgentSupportPhone { get; set; }
        public string FaqCustomerEN { get; set; }
        public string FaqCustomerJP { get; set; }
        public string FaqRestaurantEN { get; set; }
        public string FaqRestaurantJP { get; set; }
        public string FaqAgentEN { get; set; }
        public string FaqAgentJP { get; set; }
        public string TermCustomerEN { get; set; }
        public string TermCustomerJP { get; set; }
        public string TermRestaurantEN { get; set; }
        public string TermRestaurantJP { get; set; }
        public string TermAgentEN { get; set; }
        public string TermAgentJP { get; set; }
        public string HelpRestaurantEN { get; set; }
        public string HelpRestaurantJP { get; set; }
        public string HelpCustomerEN { get; set; }
        public string HelpCustomerJP { get; set; }
        public string HelpAgentEN { get; set; }
        public string HelpAgentJP { get; set; }
    }
}
