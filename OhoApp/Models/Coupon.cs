﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OhoApp.Models
{

    public class Coupon
    {
        public string Code { get; set; }
        public bool HasCoupon { get; internal set; }
    }

    public class CheckCoupon
    {
        public int Coupon_ID { get; set; }
        public int RES_ID { get; set; }
    }

    public class SaveCoupon
    {
        public int Coupon_ID { get; set; }
    }


    public class CouponDetail
    {
        public int ID { get;  set; }
        public string Code { get; set; }
        public int? RES_ID { get;  set; }
        public int Min { get; set; }
        public int Discount { get;  set; }
        public DateTime ExpirationDate { get;  set; }
        public bool IsPercent { get; set; }
        public bool IsExp { get; set; }
    }


}
