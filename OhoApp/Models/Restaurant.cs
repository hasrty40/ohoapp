﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OhoApp.Models
{
    public class SignUpRestaurant
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }



    public class LoginRestaurant
    {
        public string EmailOrPhone { get; set; }
        public string Password { get; set; }
    }

    public class LoginStatusRestaurant
    {
        public int RES_ID { get; set; }
        public bool StatusRestaurant { get; set; }
        public bool StatusApproval { get; set; }
        public bool StatusService { get; set; }
        public bool StatusOTP { get; set; }
        public bool AlternativeVerify { get; set; }
    }
    public class ChangePwRestaurant
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class RestaurantInfo
    {
        public int RES_ID { get; internal set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Logo { get; internal set; }
        public string LogoPath { get; internal set; }
        public string RestaurantNameEN { get; set; }
        public string RestaurantNameTH { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public bool? ConfirmAddress { get; set; }
        public string OfficialPhoneNumber { get; set; }
        public string PreferredPhoneNumber { get; set; }
        public string PreferredEmail { get; set; }
        public int? ServiceCharge { get; set; }
        public decimal? Rate { get; set; }
        public int? PriceRate { get; set; }
        public int? Cuisine_ID { get; set; }
        public Cuisine cuisine { get; set; }
        public string CuisineOtherName { get; set; }
        public int? Status { get; set; }
        public string PlaceId { get; set; }
        public string ContactAdmin { get; internal set; }
        public AppLanguageSelect AppLanguage { get; set; }
    }

    public class Cuisine
    {
        public int ID { get; set; }
        public string NameEN { get; set; }
        public string NameTH { get; set; }
        public bool IsEnable { get; set; }
    }
    public class ServiceCharge
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsEnable { get; set; }
    }

    public class CheckRestaurantStatus
    {
        public string RestaurantStatus { get; set; }
    }

    public class OpeningHour
    {
        public int RES_ID { get; internal set; }
        public Everyday Everyday { get; set; }
        public Monday Mon { get; set; }
        public Tuesday Tue { get; set; }
        public Wednesday Wed { get; set; }
        public Thursday Thu { get; set; }
        public Friday Fri { get; set; }
        public Saturday Sat { get; set; }
        public Sunday Sun { get; set; }
    }


    public class Monday
    {
        public bool IsOpen { get; set; }
        public bool IsPickUp { get; set; }
        public bool IsDineIn { get; set; }
        public bool IsDelivery { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }

    public class Everyday
    {
        public bool IsOpen { get; set; }
        public bool IsPickUp { get; set; }
        public bool IsDineIn { get; set; }
        public bool IsDelivery { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
    public class Tuesday
    {
        public bool IsOpen { get; set; }
        public bool IsPickUp { get; set; }
        public bool IsDineIn { get; set; }
        public bool IsDelivery { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }        
    }
    public class Wednesday
    {
        public bool IsOpen { get; set; }
        public bool IsPickUp { get; set; }
        public bool IsDineIn { get; set; }
        public bool IsDelivery { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
    public class Thursday
    {
        public bool IsOpen { get; set; }
        public bool IsPickUp { get; set; }
        public bool IsDineIn { get; set; }
        public bool IsDelivery { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }

    }
    public class Friday
    {
        public bool IsOpen { get; set; }
        public bool IsPickUp { get; set; }
        public bool IsDineIn { get; set; }
        public bool IsDelivery { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }

    }

    public class Saturday
    {
        public bool IsOpen { get; set; }
        public bool IsPickUp { get; set; }
        public bool IsDineIn { get; set; }
        public bool IsDelivery { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }

    }
    public class Sunday
    {
        public bool IsOpen { get; set; }
        public bool IsPickUp { get; set; }
        public bool IsDineIn { get; set; }
        public bool IsDelivery { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }


    public class PutStatusOTP
    {
        public bool IsVerify { get; set; }
    }

    public class DataOpeningHour
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string StartTime2 { get; set; }
        public string EndTime2 { get; set; }
        public string StartTime3 { get; set; }
        public string EndTime3 { get; set; }
        public string StartTime4 { get; set; }
        public string EndTime4 { get; set; }
        public string StartTime5 { get; set; }
        public string EndTime5 { get; set; }
        public string StartTime6 { get; set; }
        public string EndTime6 { get; set; }
        public string StartTime7 { get; set; }
        public string EndTime7 { get; set; }
        public string StartTime8 { get; set; }
        public string EndTime8 { get; set; }
        public bool IsPickUp { get; set; }
        public bool IsDineIn { get; set; }
        public bool IsDelivery { get; set; }
        public bool IsPickUp2 { get; set; }
        public bool IsDineIn2 { get; set; }
        public bool IsDelivery2 { get; set; }
        public bool IsPickUp3 { get; set; }
        public bool IsDineIn3 { get; set; }
        public bool IsDelivery3 { get; set; }
        public bool IsPickUp4 { get; set; }
        public bool IsDineIn4 { get; set; }
        public bool IsDelivery4 { get; set; }
        public bool IsPickUp5 { get; set; }
        public bool IsDineIn5 { get; set; }
        public bool IsDelivery5 { get; set; }
        public bool IsPickUp6 { get; set; }
        public bool IsDineIn6 { get; set; }
        public bool IsDelivery6 { get; set; }
        public bool IsPickUp7 { get; set; }
        public bool IsDineIn7 { get; set; }
        public bool IsDelivery7 { get; set; }
        public bool IsPickUp8 { get; set; }
        public bool IsDineIn8 { get; set; }
        public bool IsDelivery8 { get; set; }
    }


    public class ListReject
    {
        public int ID { get; set; }
        public string Description { get; set; }
    }

}
