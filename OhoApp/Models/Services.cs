﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OhoApp.Models
{
    public class RestaurantService
    {
        public bool IsPickup { get; set; }
        public bool IsDineIn { get; set; }
        public bool IsDelivery { get; set; }
    }
}
