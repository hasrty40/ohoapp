﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OhoApp.Models
{
    public class Result
    {
        public int StatusCode { get; set; }
        public string StatusMessages { get; set; }
        public string Messages { get; set; }
        public object Results { get; set; }
    }


    public class ResultLoginRestaurant
    {
        public int StatusCode { get; set; }
        public string StatusMessages { get; set; }
        public string Messages { get; set; }
        public ResultRestaurant Results { get; set; }
    }
    public class ResultRestaurant
    {
        public string RestaurantStatus { get; set; }
        public object Token { get; set; }
    }

    public class UploadProfileImage
    {
        public IFormFile Image { get; set; }
    }

    public class ResultUploadImage
    {
        public string FileName { get; set; }
        public string ImagePath { get; set; }
    }
    public class ResultUploadItemImage
    {
        public int MenuItem_ID { get; set; }
        public string FileName { get; set; }
        public string ImagePath { get; set; }
    }

    public enum StatusCodes
    {
        [EnumValue("Connection Error.")]
        Connection = 99,
        [EnumValue("Success.")]
        Succuss = 1,
        [EnumValue("Error.")]
        Error = 0
    }

    public enum RestaurantStatus
    {
        [EnumValue("waitInformation")]
        WaitInfo = 0,
        [EnumValue("waitApproval")]
        WaitApproval = 1,
        [EnumValue("waitOtp")]
        WaitOtp = 2,
        [EnumValue("verify")]
        Verify = 3,
        [EnumValue("waitService")]
        WaitService = 4
    }

    #region Customer

    public class ResultLoginCustomer
    {
        public int StatusCode { get; set; }
        public string StatusMessages { get; set; }
        public string Messages { get; set; }
        public object Results { get; set; }
    }


    #endregion

}
