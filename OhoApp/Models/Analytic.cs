﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace OhoApp.DTOs
{
    public class RevenueAndOrder
    {
        public string Date { get; set; }
        public string Orders { get; set; }
        public int Price { get; set; }
    }

    public class PopularItem
    {
        public string Name_EN { get; set; }
        public string Name_JP { get; set; }
        public int Items { get; set; }
    }

    public class TotalTax
    {
        public string Date { get; set; }
        public int Tax { get; set; }
    }
}
