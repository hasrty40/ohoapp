﻿using Dapper;
using Microsoft.Extensions.Configuration;
using OhoApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhoApp.Services
{

    public interface IPaymentData
    {
        Task<bool> PutDataPaymentOrder(string ref1, string ref2, string ref3, string qrID, int ID);
        Task<string> ChangeStatusPaymentOrder(DataConfirm data);
        Task<bool> CheckOrderType(string qrID);
        Task<PlaceOrder> GetPlaceOrder(string qrID);
    }


    public class PaymentRepository : IPaymentData
    {
        private readonly IConfiguration configuration;

        public PaymentRepository(IConfiguration config)
        {
            this.configuration = config;
        }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(ConfigurationExtensions.GetConnectionString(this.configuration, "DefaultConnection"));
            }
        }
        public async Task<bool> PutDataPaymentOrder(string ref1,string ref2,string ref3,string qrID,int ID)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    var query = new StringBuilder();
                    query.AppendLine("UPDATE [dbo].[Order] SET qrID = @qrID,ref1 = @ref1,ref2 = @ref2,ref3 = @ref3 WHERE ID = @ID");
                    var status = await dbConnection.ExecuteAsync(query.ToString(), 
                        new 
                        {
                            ID = ID,
                            qrID = qrID,
                            ref1 = ref1,
                            ref2 = ref2,
                            ref3 = ref3
                        });



                    if (status == 1)
                    {
                        var data = await dbConnection.QueryFirstOrDefaultAsync<CheckOrderTypeAndOrderID>("SELECT Order_Type,ID,RES_ID,CUS_ID FROM [dbo].[Order] WHERE ref1 = @ref1 AND ref2 = @ref2 AND ref3 = @ref3", new
                        {
                            ref1 = ref1,
                            ref2 = ref2,
                            ref3 = ref3
                        });
                        if (data.Order_Type == 1)
                        {
                            query = new StringBuilder();
                            query.AppendLine("UPDATE [Order] SET  RestaurantStatus = @STATUS,Accept_Time = GETDATE(), Cooked_Time = GETDATE()  WHERE ID = @ORDER_ID AND RES_ID = @RES_ID");
                            status = await dbConnection.ExecuteAsync(query.ToString(), new { STATUS = 2, ORDER_ID = data.ID, RES_ID = data.RES_ID });
                        }
                        else if (data.Order_Type == 3)
                        {
                            query = new StringBuilder();
                            query.AppendLine("UPDATE [Order] SET  RestaurantStatus = @STATUS,Accept_Time = GETDATE(), Cooked_Time = GETDATE()  WHERE ID = @ORDER_ID AND RES_ID = @RES_ID");
                            status = await dbConnection.ExecuteAsync(query.ToString(), new { STATUS = 1, ORDER_ID = data.ID, RES_ID = data.RES_ID });
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        public async Task<string> ChangeStatusPaymentOrder(DataConfirm data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    var query = new StringBuilder();
                    query.AppendLine("UPDATE [dbo].[Order] SET PaymentStatus = 1,PaymentMethod = 1, sendingBankCode = @sendingBankCode ,transactionId = @transactionId WHERE ref1 = @ref1 AND ref2 = @ref2 AND ref3 = @ref3");
                    var updateStatusPayment = await dbConnection.ExecuteAsync(query.ToString(), 
                        new
                        {
                            ref1 = data.billPaymentRef1,
                            ref2 = data.billPaymentRef2,
                            ref3 = data.billPaymentRef3,
                            sendingBankCode = data.sendingBankCode,
                            transactionId = data.transactionId
                        });



                    if (updateStatusPayment == 1)
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT qrID FROM [dbo].[Order] WHERE ref1 = @ref1 AND ref2 = @ref2 AND ref3 = @ref3");
                        var qrID = await dbConnection.QueryFirstOrDefaultAsync<string>(query.ToString(), 
                        new
                        {
                            ref1 = data.billPaymentRef1,
                            ref2 = data.billPaymentRef2,
                            ref3 = data.billPaymentRef3
                        });
                        return qrID;
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> CheckOrderType(string qrID)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    //Add Coupon
                    var query = new StringBuilder();
                    query.AppendLine("SELECT Order_Type FROM [dbo].[Order] WHERE qrID = @qrID");
                    var Order_Type = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { qrID = qrID });



                    if (Order_Type == 3)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        public async Task<PlaceOrder> GetPlaceOrder(string qrID)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT OrderID as orderID,LogID as logID,Order_NO as orderNO,Distance FROM [dbo].[Order] WHERE qrID = @qrID");
                    var data = await dbConnection.QueryFirstOrDefaultAsync<PlaceOrder>(query.ToString(), new
                    {
                        qrID = qrID
                    });

                    if (data != null)
                    {
                        var _data = await dbConnection.QueryFirstOrDefaultAsync<DataOrder>("SELECT ID,CUS_ID,RES_ID FROM [dbo].[Order] WHERE qrID = @qrID", new { qrID = qrID });

                        data.restaurantInfo = new Restaurant();
                        data.customerInfo = new Customer();
                        data.restaurantInfo.restaurantID = _data.RES_ID.ToString();




                        query = new StringBuilder();
                        query.AppendLine("SELECT C.CUS_ID as 'customerID',C.Firstname + '  ' + C.Lastname as 'customerName',A.PhoneNumber as 'customerMobilePhone',A.Address as 'customerAddress' FROM [dbo].[Customer] as C INNER JOIN [dbo].[Order] as O ON O.CUS_ID = C.CUS_ID");
                        query.AppendLine("INNER JOIN [dbo].[OrderAddress] as A ON A.Order_ID = O.ID");
                        query.AppendLine("WHERE O.ID = @ID");
                        data.customerInfo = await dbConnection.QueryFirstOrDefaultAsync<Customer>(query.ToString(), new { ID = _data.ID });

                        var location = await dbConnection.QueryFirstOrDefaultAsync<Position>("SELECT Location FROM [dbo].[OrderAddress] WHERE Order_ID = @ID",new { ID = _data.ID });
                        string[] _cusLocation = location.Location.Split(',');
                        data.customerInfo.location = new CustomerLocation();
                        data.customerInfo.location.lat = _cusLocation[0];
                        data.customerInfo.location.lng = _cusLocation[1];
                    }
                    data.serviceType = "MOTORCYCLE";
                    return data;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

    }
}
