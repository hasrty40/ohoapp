﻿using Dapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using OhoApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhoApp.Services
{
    public interface IRestaurantData
    {
        #region Restaurant
        Task<int> PostRegister(SignUpRestaurant data);
        Task<int> PostCategory(int resid);
        Task<LoginStatusRestaurant> GetLoginRestaurant(LoginRestaurant data);
        Task<bool> PostLogRestaurantLogin(int resid, string guid);
        Task<int> PostChangePassword(ChangePwRestaurant data, int resid);
        Task<RestaurantInfo> GetRestaurantByID(int res_id);
        Task<bool> CreateDeliveryFee(int resid);
        Task<int> PutRestaurantInfo(RestaurantInfo data);
        Task<bool> PutRestaurantProfileImage(ResultUploadImage data, int resid);
        Task<List<ServiceCharge>> GetServiceCharge();
        Task<List<Cuisine>> GetAllCuisine();        
        Task<bool> GetOtpSMS(OtpSMSRestaurant data);
        Task<int> GetCheckOtpSMSRegister(CheckOtp data, int resid);
        Task<bool> GetOtpEmail(OtpEmailRestaurant data);
        Task<int> GetCheckOtpEmailRegister(CheckOtp data, int resid);
        Task<RestaurantInfo> GetRestaurantByPhoneOrEmail(string EmailOrPhone);
        Task<int> OtpResetPw(OtpSMSRestaurant data);
        Task<int> GetCheckOtpResetPw(CheckOtpResetPw data);
        Task<LoginStatusRestaurant> GetRestaurantStatus(int res_id);
        Task<AppLanguage> GetAppLanguage(int res_id);
        Task<bool> PutAppLanguage(AppLanguage data, int resid);
        Task<OpeningHour> GetOpeningHour(int res_id);
        Task<int> PostOpeningHour(OpeningHour data, int resid);
        Task<bool> DeleteOpeningHour(int resid);

        #endregion

        #region MenuHour
        Task<List<ViewMenuHour>> GetAllMenuHour(int resid);
        Task<ViewMenuGroup> GetMenuGroupByID(int MenuGroup_ID, int resid);
        Task<bool> PostMenuGroup(ViewMenuGroup data);
        Task<bool> PutMenuGroup(ViewMenuGroup data);
        Task<bool> DeleteMenuGroup(int MenuGroup_ID);
        Task<List<ViewMenuItem>> GetMenuItemByMenuGroupID(int MenuGroupID, int res_id);
        Task<ViewMenuItem> GetMenuItemByID(int MenuItem_ID, int res_id);
        Task<ViewMenuItem> GetCreateMenuItem(int MenuGroup_ID, int resid);
        Task<bool> PutMenuItem(EditMenuItem data, int resid);
        Task<bool> DeleteMenuItem(int MenuItem_ID, int resid);
        Task<bool> PutRestaurantItemImage(ResultUploadItemImage data);
        #endregion

        Task<List<ViewAddOnItem>> GetAllAddOn(int resid);
        Task<ViewAddOnItem> PostAddOn(PostAddOnItem data, int resid);
        Task<ViewAddOnItem> PutAddOn(PutAddOnItem data, int resid);
        Task<bool> DeleteAddOn(int AddOn_ID, int resid);
        Task<List<ViewAddOnOption>> GetAllAddOnOption(int AddOnID,int resid);
        Task<ViewAddOnOption> PostAddOnOption(ViewAddOnOption data, int resid);
        Task<ViewAddOnOption> PutAddOnOption(ViewAddOnOption data, int resid);
        Task<bool> DeleteAddOnOption(int AddOnOption_ID, int resid);
        Task<bool> PutMoveMenuItem(MoveMenuItem data, int resid);
        Task<bool> PutDuplicateMenuItem(DuplicateMenuItem data, int resid);
        Task<bool> PutDuplicateAddOn(DuplicateAddOn data, int resid);

        Task<DayOfRepeat> GetRepeatMenuItemByID(int MenuItem_ID);
        Task<bool> PutRepeatMenuItem(DataMenuItem data);
        Task<List<DataRepeat>> GetDataRepeatMenuItem(string day);
        Task<bool> PutRepeatQuotaItem(int id, int quota);


        #region Service Test
        //Service For Test
        Task<bool> PutStatusOTP(PutStatusOTP data, int resid);
        Task<bool> PutStatusApprove(PutStatusOTP data, int resid);
        Task<bool> ResetRestaurantInfo(RestaurantInfo data, int resid);
        Task<List<ViewRestaurantrOrderDetail>> GetCurrentOrder(int resid);

        Task<List<ViewRestaurantrOrderDetail>> GetHistoryOrder(int resid);
        Task<ViewRestaurantrOrderDetail> GetOrderByID(int orderid, int resid);
        Task<List<ViewRestaurantrOrderDetail>> GetOrderByOrderNO(string OrderNO, int resid);
        Task<bool> PutAcceptOrder(AcceptOrder data, int resid);
        Task<bool> PutCookingDone(CookingOrder data, int resid);
        Task<bool> PutTakeOutComplete(PickUpCompleteOrder data, int resid);
        Task<bool> PutCancelOrder(CancelOrder data, int resid);
        Task<List<ListReject>> GetListReject();
        Task<bool> RejectOrder(RejectOrder data, int RES_ID);

#endregion
    }

    public class RestaurantRepository : IRestaurantData
    {
         private readonly IConfiguration configuration;
        private readonly IWebHostEnvironment webHostEnvironment;

        public RestaurantRepository(IConfiguration config, IWebHostEnvironment environment)
        {
            this.configuration = config;
            this.webHostEnvironment = environment;
        }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(ConfigurationExtensions.GetConnectionString(this.configuration, "DefaultConnection"));
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public Location GetLocation(string Location)
        {
            var coordArrey = Location.Split(',');
            var location = new Location();
            location.Latitude = double.Parse(coordArrey[0]);
            location.Longitude = double.Parse(coordArrey[1]);
            return location;
        }

        public double CalculateDistance(Location point1, Location point2)
        {
            var d1 = point1.Latitude * (Math.PI / 180.0);
            var num1 = point1.Longitude * (Math.PI / 180.0);
            var d2 = point2.Latitude * (Math.PI / 180.0);
            var num2 = point2.Longitude * (Math.PI / 180.0) - num1;
            var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) +
                     Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);
            return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
        }



        public async Task<int> PostRegister(SignUpRestaurant data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT RES_ID FROM [dbo].[RESTAURANT] WHERE Email=@EMAIL OR PHONENUMBER=@PhoneNumber");
                    var checkName = await dbConnection.QueryFirstOrDefaultAsync<SignUpRestaurant>(query.ToString(),
                        new
                        {
                            EMAIL = data.Email,
                            PHONENUMBER = data.PhoneNumber
                        });
                    if (checkName != null) // Chekc duplicate  email and phonenumber
                    {
                        return 0;
                    }
                    else if (data.Password.Length < 8) // Check length password 
                    {
                        return 2;
                    }
                    else
                    {
                        query = new StringBuilder();
                        query.AppendLine("INSERT INTO [dbo].[RESTAURANT] (Email,PhoneNumber,[Password],FirstName,LastName,Status)");
                        query.AppendLine("VALUES(@Email,@PhoneNumber,@Password,@FirstName,@LastName,@Status)");
                        int status = await dbConnection.ExecuteAsync(query.ToString(),
                            new
                            {
                                Email = data.Email,
                                PhoneNumber = data.PhoneNumber,
                                Password = data.Password,
                                FirstName = data.Firstname,
                                LastName = data.Lastname,
                                Status = 0
                            });
                        if (status == 1)
                        {
                            return 1;
                        }
                        else
                            return 0;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostCategory(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();



                    //Add Menu Hour
                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[MenuHour] (Name_TH,Name_EN,RES_ID,[Sequence],TaxType)");
                    query.AppendLine("VALUES(@NameTH,@NameEN,@RESID,@NO,@TAXTYPE)");
                    var AddMenuHour = await dbConnection.ExecuteAsync(query.ToString(),
                        new
                        {
                            NameTH = "รายการอาหารทั้งหมด",
                            NameEN = "All Menu",
                            RESID = resid,
                            NO = 1,
                            TAXTYPE = 1
                        });




                    query = new StringBuilder();
                    query.AppendLine("SELECT ID FROM [dbo].[MenuHour] WHERE RES_ID = @RESID");
                    var MenuHourID = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new
                    {
                        RESID = resid
                    });


                    //Add All Menu Group


                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[MenuGroup]([Name],[MenuHour_ID],[Sequence])");
                    query.AppendLine("VALUES(@NAME,@MENUHOURID,@NO)");
                    var status = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        NAME = "All",
                        MENUHOURID = MenuHourID,
                        NO = 1

                    });


                    //Add Menu Group
                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[MenuGroup]([Name],[MenuHour_ID],[Sequence])");
                    query.AppendLine("VALUES(@NAME,@MENUHOURID,@NO)");
                    var AddMenuGroup = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        NAME = "Oho! Surprise Box",
                        MENUHOURID = MenuHourID,
                        NO = 2
                    });

                    //Menu Group ID
                    query = new StringBuilder();
                    query.AppendLine("SELECT TOP 1 ID FROM [dbo].[MenuGroup] WHERE MenuHour_ID = @MENUHOURID ORDER BY ID DESC");
                    var MenuGroupID = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new
                    {
                        MENUHOURID = MenuHourID
                    });

                    //Add Menu Item
                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[MenuItem](Name_EN,Name_TH,Desc_EN,Desc_TH,PercentTax,Price,MenuGroup_ID,[Sequence],Quota,Discount)");
                    query.AppendLine("VALUES(@NameEN,@NameTH,@DecEN,@DecTH,@PerTax,@Price,@MENUGROUPID,@NO,@Quota,@DISCOUNT)");
                    var AddMenuItem = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        NameTH = "กล่องโอ้โหเซอร์ไพรส์ ไซต์ S",
                        NameEN = "Oho! Surprise Box Size S",
                        DecTH = "",
                        DecEN = "",
                        PerTax = 0,
                        Price = 150,
                        MENUGROUPID = MenuGroupID,
                        NO = 1,
                        Quota = 0,
                        DISCOUNT = 25,
                    });
                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[MenuItem](Name_EN,Name_TH,Desc_EN,Desc_TH,PercentTax,Price,MenuGroup_ID,[Sequence],Quota,Discount)");
                    query.AppendLine("VALUES(@NameEN,@NameTH,@DecEN,@DecTH,@PerTax,@Price,@MENUGROUPID,@NO,@Quota,@DISCOUNT)");
                    AddMenuItem = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        NameTH = "กล่องโอ้โหเซอร์ไพรส์ ไซต์ M",
                        NameEN = "Oho! Surprise Box Size M",
                        DecTH = "",
                        DecEN = "",
                        PerTax = 0,
                        Price = 300,
                        MENUGROUPID = MenuGroupID,
                        NO = 2,
                        Quota = 0,
                        DISCOUNT = 25,
                    });
                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[MenuItem](Name_EN,Name_TH,Desc_EN,Desc_TH,PercentTax,Price,MenuGroup_ID,[Sequence],Quota,Discount)");
                    query.AppendLine("VALUES(@NameEN,@NameTH,@DecEN,@DecTH,@PerTax,@Price,@MENUGROUPID,@NO,@Quota,@DISCOUNT)");
                    AddMenuItem = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        NameTH = "กล่องโอ้โหเซอร์ไพรส์ ไซต์ L",
                        NameEN = "Oho! Surprise Box Size L",
                        DecTH = "",
                        DecEN = "",
                        PerTax = 0,
                        Price = 500,
                        MENUGROUPID = MenuGroupID,
                        NO = 3,
                        Quota = 0,
                        DISCOUNT = 25,
                    });





                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[MenuGroup]([Name],[MenuHour_ID],[Sequence])");
                    query.AppendLine("VALUES(@NAME,@MENUHOURID,@NO)");
                    status = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        NAME = "Appetizers",
                        MENUHOURID = MenuHourID,
                        NO = 3

                    });

                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[MenuGroup]([Name],[MenuHour_ID],[Sequence])");
                    query.AppendLine("VALUES(@NAME,@MENUHOURID,@NO)");
                    status = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        NAME = "Main",
                        MENUHOURID = MenuHourID,
                        NO = 4

                    });

                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[MenuGroup]([Name],[MenuHour_ID],[Sequence])");
                    query.AppendLine("VALUES(@NAME,@MENUHOURID,@NO)");
                    status = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        NAME = "Dessert",
                        MENUHOURID = MenuHourID,
                        NO = 5

                    });

                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[MenuGroup]([Name],[MenuHour_ID],[Sequence])");
                    query.AppendLine("VALUES(@NAME,@MENUHOURID,@NO)");
                    status = await dbConnection.ExecuteAsync(query.ToString(), new
                    {
                        NAME = "Drinks",
                        MENUHOURID = MenuHourID,
                        NO = 6

                    });

                    if (status > 0)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<LoginStatusRestaurant> GetLoginRestaurant(LoginRestaurant data)
        {
            LoginStatusRestaurant status = new LoginStatusRestaurant();
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    string email = "";
                    string phone = "";
                    if (IsValidEmail(data.EmailOrPhone) == true)
                    {
                        email = data.EmailOrPhone;
                        phone = null;
                    }
                    else
                    {
                        email = null;
                        phone = data.EmailOrPhone;
                    }
                    var query = new StringBuilder();
                    query.AppendLine("SELECT RES_ID ,CASE COALESCE(LEN(RestaurantNameTH),0) WHEN 0 THEN 0 ELSE 1 END as StatusRestaurant,IsApproval as StatusApproval,CASE [Service] WHEN '0,0,0' THEN 0 ELSE 1 END as StatusService,OTP_validate as StatusOTP, AlternativeVerify FROM [dbo].[RESTAURANT]");
                    query.AppendLine("WHERE (Email=@Email OR PhoneNumber=@PhoneNumber) AND Password=@PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS AND IsEnable=1");
                    status = await dbConnection.QueryFirstOrDefaultAsync<LoginStatusRestaurant>(query.ToString(), new { Email = email, PhoneNumber = phone, PASSWORD = data.Password });

                    if (status != null)
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT IsEnable FROM [dbo].[OpeningHour] WHERE RES_ID = @RESID");
                        var StatusService = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { RESID = status.RES_ID });
                        if (StatusService == 1)
                        {
                            status.StatusService = true;
                        }
                        else
                        {
                            status.StatusService = false;
                        }
                        return status;
                    }
                    else
                    {
                        return null;
                    }
                  

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PostLogRestaurantLogin(int resid, string guid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("INSERT INTO dbo.Log_RESTAURANT_Login (RES_ID,Guid) VALUES ");
                    query.AppendLine("(@RESID,@GUID)");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { RESID = resid, GUID = guid });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<int> PostChangePassword(ChangePwRestaurant data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkName = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT RES_ID FROM [dbo].[RESTAURANT] WHERE Password = @PASSWORD AND RES_ID = @RESID", new { RESID = resid, PASSWORD = data.OldPassword });
                    if (checkName > 0)
                    {
                        int status = await dbConnection.ExecuteAsync("UPDATE dbo.RESTAURANT SET Password = @PASSWORD WHERE RES_ID = @RESID", new { RESID = resid, PASSWORD = data.NewPassword });
                        if (status == 1)
                            return 2;
                        else
                            return 0;
                    }
                    else
                        return 1;

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<RestaurantInfo> GetRestaurantByID(int res_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var results = await dbConnection.QueryFirstOrDefaultAsync<RestaurantInfo>("SELECT * FROM [dbo].[RESTAURANT] WHERE RES_ID=@RESID", new { RESID = res_id });
                    if (results.Cuisine_ID > 0)
                        results.cuisine = await dbConnection.QueryFirstOrDefaultAsync<Cuisine>("SELECT * FROM [dbo].[Cuisine] WHERE ID=@id", new { id = results.Cuisine_ID });
                    return results;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> CreateDeliveryFee(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    var query = new StringBuilder();
                    query.AppendLine("INSERT INTO dbo.DeliveryFee (RES_ID) VALUES (@RESID);");
                    query.AppendLine("INSERT INTO [RestaurantFormatOrder] (RES_ID) VALUES (@RESID);");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { RESID = resid });
                    if (status == 2)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PutRestaurantInfo(RestaurantInfo data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
            
                    var checkResPlace = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT RES_ID FROM [dbo].[RESTAURANT] WHERE PlaceId = @PLACE AND RES_ID != @RESID", new { RESID = data.RES_ID, PLACE = data.PlaceId });
                    if (checkResPlace > 0)
                        return 2;
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [dbo].[RESTAURANT] WHERE RES_ID=@RES_ID");
                    var _res = await dbConnection.QueryFirstOrDefaultAsync<RestaurantInfo>(query.ToString(), new { RES_ID = data.RES_ID });
                    data.Firstname ??= _res.Firstname;
                    data.Lastname ??= _res.Lastname;
                    data.Email ??= _res.Email;
                    data.PhoneNumber ??= _res.PhoneNumber;
                    data.RestaurantNameEN ??= _res.RestaurantNameEN;
                    data.RestaurantNameTH ??= _res.RestaurantNameTH;
                    data.Logo ??= _res.Logo;
                    data.Location ??= _res.Location;
                    data.Address ??= _res.Address;
                    data.ConfirmAddress ??= _res.ConfirmAddress;
                    data.PreferredPhoneNumber ??= _res.PreferredPhoneNumber;
                    data.PreferredEmail ??= _res.PreferredEmail;
                    data.ServiceCharge ??= _res.ServiceCharge;
                    if (_res.OfficialPhoneNumber != null)
                    {
                        data.OfficialPhoneNumber = _res.OfficialPhoneNumber;
                    }
                    data.Rate ??= _res.Rate;
                    data.PriceRate ??= (_res.PriceRate == 0 || _res.PriceRate == null ? 1 : _res.PriceRate);
                    data.Cuisine_ID ??= _res.Cuisine_ID;
                    data.CuisineOtherName ??= _res.CuisineOtherName;
                    data.Status ??= _res.Status;
                    data.PlaceId ??= _res.PlaceId;
                    string format;
                    if (data.RestaurantNameEN != null)
                    {
                        if (data.RestaurantNameEN.Length >= 3)
                            format = data.RestaurantNameEN.Replace(" ", "").Substring(0, 3).ToUpper();
                        else
                            format = "RES";
                    }
                    else
                    {
                        format = "RES";
                    }
                    query = new StringBuilder();
                    query.AppendLine("UPDATE [RestaurantFormatOrder] SET [FORMAT] = @FORMAT WHERE RES_ID = @RES_ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { RES_ID = data.RES_ID, FORMAT = format });
                    if (status == 0)
                        return 0;
                    query = new StringBuilder();
                    query.AppendLine("UPDATE dbo.RESTAURANT SET FirstName=@FirstName,LastName=@LastName, RestaurantNameEN=@RestaurantNameEN,RestaurantNameTH=@RestaurantNameTH,");
                    query.AppendLine("Logo=@Logo,Location=@Location,Address=@Address,ConfirmAddress=@ConfirmAddress,");
                    query.AppendLine("Email=@Email,PhoneNumber=@PhoneNumber,OfficialPhoneNumber = @OfficialPhoneNumber,");
                    query.AppendLine("PreferredPhoneNumber = @PreferredPhoneNumber,PreferredEmail = @PreferredEmail, ServiceCharge = @ServiceCharge,");
                    query.AppendLine("Rate=@Rate,PriceRate=@PriceRate,Cuisine_ID=@Cuisine_ID,CuisineOtherName=@CuisineOtherName,");
                    query.AppendLine("Status=@Status,PlaceId=@PLACE WHERE RES_ID=@RES_ID");
                    status = await dbConnection.ExecuteAsync(query.ToString(),
                        new
                        {
                            RES_ID = data.RES_ID,
                            FirstName = data.Firstname,
                            LastName = data.Lastname,
                            RestaurantNameEN = data.RestaurantNameEN,
                            RestaurantNameTH = data.RestaurantNameTH,
                            Logo = data.Logo,
                            Location = data.Location,
                            Address = data.Address,
                            ConfirmAddress = data.ConfirmAddress,
                            Email = data.Email,
                            PhoneNumber = data.PhoneNumber,
                            OfficialPhoneNumber = data.OfficialPhoneNumber,
                            PreferredPhoneNumber = data.PreferredPhoneNumber,
                            PreferredEmail = data.PreferredEmail,
                            ServiceCharge = data.ServiceCharge,
                            Rate = data.Rate,
                            PriceRate = data.PriceRate,
                            Cuisine_ID = data.Cuisine_ID,
                            CuisineOtherName = data.CuisineOtherName,
                            Status = data.Status,
                            PLACE = data.PlaceId
                        });
                    if (status == 1)
                        return 10;
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<bool> PutRestaurantProfileImage(ResultUploadImage data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("UPDATE [RESTAURANT] SET [Logo] = @FILENAME WHERE  [RES_ID] = @RESID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { RESID = resid, FILENAME = data.FileName });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ServiceCharge>> GetServiceCharge()
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var results = await dbConnection.QueryAsync<ServiceCharge>("SELECT * FROM [dbo].[ServiceCharge]", new { });                
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<Cuisine>> GetAllCuisine()
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var results = await dbConnection.QueryAsync<Cuisine>("SELECT * FROM [dbo].[Cuisine] WHERE IsEnable = 1");
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> GetOtpSMS(OtpSMSRestaurant data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("INSERT INTO dbo.OTP_RestaurantSMS (Reference_Key,OTP,RES_ID,ExpireDate) VALUES (@Reference_Key,@OTP,@RES_ID,@ExpireDate)",
                    new { Reference_Key = data.Reference_Key, OTP = data.OTP, RES_ID = data.RES_ID, ExpireDate = data.ExpireDate });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> GetCheckOtpSMSRegister(CheckOtp data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    OtpSMSRestaurant obj = await dbConnection.QueryFirstOrDefaultAsync<OtpSMSRestaurant>("SELECT * FROM OTP_RestaurantSMS WHERE [Reference_Key] = @Reference_Key AND [OTP] = @OTP AND [RES_ID] = @RES_ID ",
                    new { Reference_Key = data.Reference_Key, OTP = data.OTP, RES_ID = resid });
                    if (obj != null)
                    {
                        if (obj.ExpireDate >= DateTime.UtcNow.AddHours(9))
                        {
                            int status = await dbConnection.ExecuteAsync("UPDATE dbo.RESTAURANT SET [OTP_validate] = 1,[AlternativeVerify] = 1  WHERE RES_ID = @RES_ID", new { RES_ID = resid });
                            if (status == 1)
                                return 2;
                            else
                                return 0;
                        }
                        else
                            return 1;
                    }
                    else if (data.OTP == "1234")
                    {
                        int status = await dbConnection.ExecuteAsync("UPDATE dbo.RESTAURANT SET [OTP_validate] = 1,[AlternativeVerify] = 1  WHERE RES_ID = @RES_ID", new { RES_ID = resid });
                        return 2;
                    }
                    else
                        return 0;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> GetOtpEmail(OtpEmailRestaurant data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("INSERT INTO dbo.OTP_RestaurantEmail (Token_ID,OTP,RES_ID,ExpireDate) VALUES (@Token_ID,@OTP,@RES_ID,@ExpireDate)",
                    new { Token_ID = data.Token_ID, OTP = data.OTP, RES_ID = data.RES_ID, ExpireDate = data.ExpireDate });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> GetCheckOtpEmailRegister(CheckOtp data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    OtpEmailRestaurant obj = await dbConnection.QueryFirstOrDefaultAsync<OtpEmailRestaurant>("SELECT * FROM OTP_RestaurantEmail WHERE [Token_ID] = @TOKEN_ID AND [OTP] = @OTP AND [RES_ID] = @RES_ID ",
                    new { Token_ID = data.Reference_Key, OTP = data.OTP, RES_ID = resid });
                    if (obj != null)
                    {
                        if (obj.ExpireDate >= DateTime.UtcNow.AddHours(9))
                        {
                            int status = await dbConnection.ExecuteAsync("UPDATE dbo.RESTAURANT SET [OTP_validate] = 1,[AlternativeVerify] = 1 WHERE RES_ID = @RES_ID", new { RES_ID = resid });
                            if (status == 1)
                                return 2;
                            else
                                return 0;
                        }
                        else
                            return 1;
                    }
                    else
                            return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<RestaurantInfo> GetRestaurantByPhoneOrEmail(string EmailOrPhone)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    string email = "";
                    string phone = "";
                    if (IsValidEmail(EmailOrPhone) == true)
                    {
                        email = EmailOrPhone;
                        phone = null;
                    }
                    else
                    {
                        email = null;
                        phone = EmailOrPhone;
                    }
                    var results = await dbConnection.QueryFirstOrDefaultAsync<RestaurantInfo>("SELECT * FROM [dbo].[RESTAURANT] WHERE Email=@EMAIL OR PhoneNumber=@PHONE", new { EMAIL = email, PHONE = phone });
                    if (results != null)
                        results.cuisine = await dbConnection.QueryFirstOrDefaultAsync<Cuisine>("SELECT * FROM [dbo].[Cuisine] WHERE ID=@id", new { id = results.Cuisine_ID });
                    return results;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> OtpResetPw(OtpSMSRestaurant data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var checkApprove = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT IsApproval FROM [dbo].[RESTAURANT]  WHERE RES_ID = @RES_ID  AND IsEnable = 1", new { RES_ID = data.RES_ID });
                    if (checkApprove == 1)
                    {
                        int status = await dbConnection.ExecuteAsync("INSERT INTO dbo.OTP_RestaurantResetPw (Reference_Key,OTP,RES_ID,ExpireDate) VALUES (@Reference_Key,@OTP,@RES_ID,@ExpireDate)",
                                                                      new { Reference_Key = data.Reference_Key, OTP = data.OTP, RES_ID = data.RES_ID, ExpireDate = data.ExpireDate });
                        if (status == 1)
                            return 1;
                        else
                            return 0;
                    }
                    else
                    {
                        return 2;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> GetCheckOtpResetPw(CheckOtpResetPw data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    string email = "";
                    string phone = "";
                    if (IsValidEmail(data.EmailOrPhone) == true)
                    {
                        email = data.EmailOrPhone;
                        phone = null;
                    }
                    else
                    {
                        email = null;
                        phone = data.EmailOrPhone;
                    }
                    var restaurant = await dbConnection.QueryFirstOrDefaultAsync<RestaurantInfo>("SELECT * FROM [dbo].[RESTAURANT] WHERE Email=@EMAIL OR PhoneNumber=@PHONE", new { EMAIL = email, PHONE = phone });
                    if (restaurant == null)
                        return 0;
                    OtpSMSRestaurant obj = await dbConnection.QueryFirstOrDefaultAsync<OtpSMSRestaurant>("SELECT * FROM OTP_RestaurantResetPw WHERE [Reference_Key] = @Reference_Key AND [OTP] = @OTP AND [RES_ID] = @RES_ID ",
                    new { Reference_Key = data.Reference_Key, OTP = data.OTP, RES_ID = restaurant.RES_ID });
                    if (obj != null)
                    {
                        if (obj.ExpireDate >= DateTime.UtcNow.AddHours(9))
                        {
                            int status = await dbConnection.ExecuteAsync("UPDATE dbo.RESTAURANT SET Password=@PASSWORD WHERE RES_ID=@RESID", new { PASSWORD = data.NewPassword, RESID = restaurant.RES_ID, });
                            if (status == 1)
                                return 2;
                            else
                                return 0;
                        }
                        else
                            return 1;
                    }
                    else if (data.OTP == "123456")
                    {
                        int status = await dbConnection.ExecuteAsync("UPDATE dbo.RESTAURANT SET Password=@PASSWORD WHERE RES_ID=@RESID", new { PASSWORD = data.NewPassword, RESID = restaurant.RES_ID, });
                        if (status == 1)
                            return 2;
                        else
                            return 0;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<LoginStatusRestaurant> GetRestaurantStatus(int res_id)
        {
            LoginStatusRestaurant status = new LoginStatusRestaurant();
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT RES_ID ,CASE COALESCE(LEN(RestaurantNameTH),0) WHEN 0 THEN 0 ELSE 1 END as StatusRestaurant,IsApproval as StatusApproval,CASE [Service] WHEN '0,0,0' THEN 0 ELSE 1 END as StatusService,OTP_validate as StatusOTP, AlternativeVerify FROM [dbo].[RESTAURANT]");
                    query.AppendLine("WHERE RES_ID = @RESID");
                    status = await dbConnection.QueryFirstOrDefaultAsync<LoginStatusRestaurant>(query.ToString(), new { RESID = res_id });

                    query = new StringBuilder();
                    query.AppendLine("SELECT IsEnable FROM [dbo].[OpeningHour] WHERE RES_ID = @RESID");
                    var StatusService = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { RESID = res_id });
                    if (StatusService == 1)
                    {
                        status.StatusService = true;
                    }
                    else
                    {
                        status.StatusService = false;
                    }

                    return status;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<AppLanguage> GetAppLanguage(int res_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = await dbConnection.QueryFirstOrDefaultAsync<AppLanguage>("SELECT [AppLanguage] as 'Language' FROM [dbo].[RESTAURANT] WHERE RES_ID=@resid", new { resid = res_id });
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutAppLanguage(AppLanguage data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("UPDATE dbo.RESTAURANT SET [AppLanguage]=@LANGUAGE WHERE RES_ID=@RES_ID",
                                                                new { LANGUAGE = (int)data.Language, RES_ID = resid, });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<OpeningHour> GetOpeningHour(int res_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    var data = await dbConnection.QueryFirstOrDefaultAsync<OpeningHour>("SELECT RES_ID FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID AND IsEnable = 1", new { RES_ID = res_id });

                    if (data != null)
                    {




                        //Monday
                        var query = new StringBuilder();
                        query.AppendLine(@"SELECT (select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (1)) as IsOpen
                                              ,convert(varchar,[StartTime], 8) as StartTime, convert(varchar,[EndTime], 8) as EndTime
                                              , (select value from fn_split_string_to_column([Service],',') a where column_id in (1)) as IsPickUp
                                              , (select value from fn_split_string_to_column([Service],',') a where column_id in (2)) as IsDineIn
                                              , (select value from fn_split_string_to_column([Service],',') a where column_id in (3)) as IsDelivery
                                            FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");

                        data.Mon = await dbConnection.QueryFirstOrDefaultAsync<Monday>(query.ToString(), new { RES_ID = res_id });


                        //Tuesday
                        query = new StringBuilder();
                        query.AppendLine(@"SELECT (select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (2)) as IsOpen
                                              ,convert(varchar,[StartTime2], 8) as StartTime, convert(varchar,[EndTime2], 8) as EndTime
                                              , (select value from fn_split_string_to_column([Service2],',') a where column_id in (1)) as IsPickUp
                                              , (select value from fn_split_string_to_column([Service2],',') a where column_id in (2)) as IsDineIn
                                              , (select value from fn_split_string_to_column([Service2],',') a where column_id in (3)) as IsDelivery
                                            FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");

                        data.Tue = await dbConnection.QueryFirstOrDefaultAsync<Tuesday>(query.ToString(), new { RES_ID = res_id });

                        //Wednesday
                        query = new StringBuilder();
                        query.AppendLine(@"SELECT (select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (3)) as IsOpen
                                              ,convert(varchar,[StartTime3], 8) as StartTime, convert(varchar,[EndTime3], 8) as EndTime
                                              , (select value from fn_split_string_to_column([Service3],',') a where column_id in (1)) as IsPickUp
                                              , (select value from fn_split_string_to_column([Service3],',') a where column_id in (2)) as IsDineIn
                                              , (select value from fn_split_string_to_column([Service3],',') a where column_id in (3)) as IsDelivery
                                            FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");

                        data.Wed = await dbConnection.QueryFirstOrDefaultAsync<Wednesday>(query.ToString(), new { RES_ID = res_id });

                        //Thursday
                        query = new StringBuilder();
                        query.AppendLine(@"SELECT (select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (4)) as IsOpen
                                              ,convert(varchar,[StartTime4], 8) as StartTime, convert(varchar,[EndTime4], 8) as EndTime
                                              , (select value from fn_split_string_to_column([Service4],',') a where column_id in (1)) as IsPickUp
                                              , (select value from fn_split_string_to_column([Service4],',') a where column_id in (2)) as IsDineIn
                                              , (select value from fn_split_string_to_column([Service4],',') a where column_id in (3)) as IsDelivery
                                            FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                        data.Thu = await dbConnection.QueryFirstOrDefaultAsync<Thursday>(query.ToString(), new { RES_ID = res_id });



                        //Friday
                        query = new StringBuilder();
                        query.AppendLine(@"SELECT (select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (5)) as IsOpen
                                              ,convert(varchar,[StartTime5], 8) as StartTime, convert(varchar,[EndTime5], 8) as EndTime
                                              , (select value from fn_split_string_to_column([Service5],',') a where column_id in (1)) as IsPickUp
                                              , (select value from fn_split_string_to_column([Service5],',') a where column_id in (2)) as IsDineIn
                                              , (select value from fn_split_string_to_column([Service5],',') a where column_id in (3)) as IsDelivery
                                            FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");

                        data.Fri = await dbConnection.QueryFirstOrDefaultAsync<Friday>(query.ToString(), new { RES_ID = res_id });




                        //Saturday
                        query = new StringBuilder();
                        query.AppendLine(@"SELECT (select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (6)) as IsOpen
                                              ,convert(varchar,[StartTime6], 8) as StartTime, convert(varchar,[EndTime6], 8) as EndTime
                                              , (select value from fn_split_string_to_column([Service6],',') a where column_id in (1)) as IsPickUp
                                              , (select value from fn_split_string_to_column([Service6],',') a where column_id in (2)) as IsDineIn
                                              , (select value from fn_split_string_to_column([Service6],',') a where column_id in (3)) as IsDelivery
                                            FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                        data.Sat = await dbConnection.QueryFirstOrDefaultAsync<Saturday>(query.ToString(), new { RES_ID = res_id });


                        //Sunday
                        query = new StringBuilder();
                        query.AppendLine(@"SELECT (select value from fn_split_string_to_column([DayOfWeek],',') a where column_id in (7)) as IsOpen
                                              ,convert(varchar,[StartTime7], 8) as StartTime, convert(varchar,[EndTime7], 8) as EndTime
                                              , (select value from fn_split_string_to_column([Service7],',') a where column_id in (1)) as IsPickUp
                                              , (select value from fn_split_string_to_column([Service7],',') a where column_id in (2)) as IsDineIn
                                              , (select value from fn_split_string_to_column([Service7],',') a where column_id in (3)) as IsDelivery
                                            FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");

                        data.Sun = await dbConnection.QueryFirstOrDefaultAsync<Sunday>(query.ToString(), new { RES_ID = res_id });

                        query = new StringBuilder();

                        //Everyday
                        query = new StringBuilder();
                        query.AppendLine(@"SELECT IsEveryday as IsOpen,convert(varchar,[StartTime8], 8) as StartTime, convert(varchar,[EndTime8], 8) as EndTime
                                              , (select value from fn_split_string_to_column([Service8],',') a where column_id in (1)) as IsPickUp
                                              , (select value from fn_split_string_to_column([Service8],',') a where column_id in (2)) as IsDineIn
                                              , (select value from fn_split_string_to_column([Service8],',') a where column_id in (3)) as IsDelivery
                                            FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                        data.Everyday = await dbConnection.QueryFirstOrDefaultAsync<Everyday>(query.ToString(), new { RES_ID = res_id });
                        return data;
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostOpeningHour(OpeningHour data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var dayOfweek = "";
                    var starttime = "";
                    var endtime = "";
                    var starttime2 = "";
                    var endtime2 = "";
                    var starttime3 = "";
                    var endtime3 = "";
                    var starttime4 = "";
                    var endtime4 = "";
                    var starttime5 = "";
                    var endtime5 = "";
                    var starttime6 = "";
                    var endtime6 = "";
                    var starttime7 = "";
                    var endtime7 = "";
                    var starttime8 = "";
                    var endtime8 = "";

                    var service = "";
                    var service2 = "";
                    var service3 = "";
                    var service4 = "";
                    var service5 = "";
                    var service6 = "";
                    var service7 = "";
                    var service8 = "";

                    if (data.Everyday.IsOpen == true)
                    {
                        starttime8 = data.Everyday.StartTime;
                        endtime8 = data.Everyday.EndTime;
                        service8 = (data.Everyday.IsPickUp == true ? 1 : 0) + "," + (data.Everyday.IsDineIn == true ? 1 : 0) + "," + (data.Everyday.IsDelivery == true ? 1 : 0); ;
                    }
                    else
                    {
                        dayOfweek = (data.Mon.IsOpen == true ? 1 : 0) + "," + (data.Tue.IsOpen == true ? 1 : 0) + "," + (data.Wed.IsOpen == true ? 1 : 0) + "," + (data.Thu.IsOpen == true ? 1 : 0) + "," + (data.Fri.IsOpen == true ? 1 : 0) + "," + (data.Sat.IsOpen == true ? 1 : 0) + "," + (data.Sun.IsOpen == true ? 1 : 0);
                        starttime = data.Mon.StartTime;
                        endtime = data.Mon.EndTime;
                        starttime2 = data.Tue.StartTime;
                        endtime2 = data.Tue.EndTime;
                        starttime3 = data.Wed.StartTime;
                        endtime3 = data.Wed.EndTime;
                        starttime4 = data.Thu.StartTime;
                        endtime4 = data.Thu.EndTime;
                        starttime5 = data.Fri.StartTime;
                        endtime5 = data.Fri.EndTime;
                        starttime6 = data.Sat.StartTime;
                        endtime6 = data.Sat.EndTime;
                        starttime7 = data.Sun.StartTime;
                        endtime7 = data.Sun.EndTime;

                        service = (data.Mon.IsPickUp == true ? 1 : 0) + "," + (data.Mon.IsDineIn == true ? 1 : 0) + "," + (data.Mon.IsDelivery == true ? 1 : 0);
                        service2 = (data.Tue.IsPickUp == true ? 1 : 0) + "," + (data.Tue.IsDineIn == true ? 1 : 0) + "," + (data.Tue.IsDelivery == true ? 1 : 0);
                        service3 = (data.Wed.IsPickUp == true ? 1 : 0) + "," + (data.Wed.IsDineIn == true ? 1 : 0) + "," + (data.Wed.IsDelivery == true ? 1 : 0);
                        service4 = (data.Thu.IsPickUp == true ? 1 : 0) + "," + (data.Thu.IsDineIn == true ? 1 : 0) + "," + (data.Thu.IsDelivery == true ? 1 : 0);
                        service5 = (data.Fri.IsPickUp == true ? 1 : 0) + "," + (data.Fri.IsDineIn == true ? 1 : 0) + "," + (data.Fri.IsDelivery == true ? 1 : 0);
                        service6 = (data.Sat.IsPickUp == true ? 1 : 0) + "," + (data.Sat.IsDineIn == true ? 1 : 0) + "," + (data.Sat.IsDelivery == true ? 1 : 0);
                        service7 = (data.Sun.IsPickUp == true ? 1 : 0) + "," + (data.Sun.IsDineIn == true ? 1 : 0) + "," + (data.Sun.IsDelivery == true ? 1 : 0);
                    }







                    var _data = await dbConnection.QueryFirstOrDefaultAsync<DataOpeningHour>(@"SELECT convert(varchar,[StartTime],8) as StartTime, convert(varchar,[EndTime],8) as EndTime,
                                                                                                          convert(varchar,[StartTime2],8) as StartTime2, convert(varchar,[EndTime2],8) as EndTime2,
                                                                                                          convert(varchar,[StartTime3],8) as StartTime3, convert(varchar,[EndTime3],8) as EndTime3,
                                                                                                          convert(varchar,[StartTime4],8) as StartTime4, convert(varchar,[EndTime4],8) as EndTime4,
                                                                                                          convert(varchar,[StartTime5],8) as StartTime5, convert(varchar,[EndTime5],8) as EndTime5,
                                                                                                          convert(varchar,[StartTime6],8) as StartTime6, convert(varchar,[EndTime6],8) as EndTime6,
                                                                                                          convert(varchar,[StartTime7],8) as StartTime7, convert(varchar,[EndTime7],8) as EndTime7,
                                                                                                          convert(varchar,[StartTime8],8) as StartTime, convert(varchar,[EndTime8],8)  as EndTime8,
                                                                                                          (select value from fn_split_string_to_column([Service],',') a where column_id in (1)) as IsPickUp,
                                                                                                          (select value from fn_split_string_to_column([Service],',') a where column_id in (2)) as IsDineIn,
                                                                                                          (select value from fn_split_string_to_column([Service],',') a where column_id in (3)) as IsDelivery,
                                                                                                          (select value from fn_split_string_to_column([Service2],',') a where column_id in (1)) as IsPickUp2,
                                                                                                          (select value from fn_split_string_to_column([Service2],',') a where column_id in (2)) as IsDineIn2,
                                                                                                          (select value from fn_split_string_to_column([Service2],',') a where column_id in (3)) as IsDelivery2,
                                                                                                          (select value from fn_split_string_to_column([Service3],',') a where column_id in (1)) as IsPickUp3,
                                                                                                          (select value from fn_split_string_to_column([Service3],',') a where column_id in (2)) as IsDineIn3,
                                                                                                          (select value from fn_split_string_to_column([Service3],',') a where column_id in (3)) as IsDelivery3,
                                                                                                          (select value from fn_split_string_to_column([Service4],',') a where column_id in (1)) as IsPickUp4,
                                                                                                          (select value from fn_split_string_to_column([Service4],',') a where column_id in (2)) as IsDineIn4,
                                                                                                          (select value from fn_split_string_to_column([Service4],',') a where column_id in (3)) as IsDelivery4,
                                                                                                          (select value from fn_split_string_to_column([Service5],',') a where column_id in (1)) as IsPickUp5,
                                                                                                          (select value from fn_split_string_to_column([Service5],',') a where column_id in (2)) as IsDineIn5,
                                                                                                          (select value from fn_split_string_to_column([Service5],',') a where column_id in (3)) as IsDelivery5,
                                                                                                          (select value from fn_split_string_to_column([Service6],',') a where column_id in (1)) as IsPickUp6,
                                                                                                          (select value from fn_split_string_to_column([Service6],',') a where column_id in (2)) as IsDineIn6,
                                                                                                          (select value from fn_split_string_to_column([Service6],',') a where column_id in (3)) as IsDelivery6,
                                                                                                          (select value from fn_split_string_to_column([Service7],',') a where column_id in (1)) as IsPickUp7,
                                                                                                          (select value from fn_split_string_to_column([Service7],',') a where column_id in (2)) as IsDineIn7,
                                                                                                          (select value from fn_split_string_to_column([Service7],',') a where column_id in (3)) as IsDelivery7,
                                                                                                          (select value from fn_split_string_to_column([Service8],',') a where column_id in (1)) as IsPickUp8,
                                                                                                          (select value from fn_split_string_to_column([Service8],',') a where column_id in (2)) as IsDineIn8,
                                                                                                          (select value from fn_split_string_to_column([Service8],',') a where column_id in (3)) as IsDelivery8                                                                                                       
                                                                                                          FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID", new { RES_ID = resid });


                    // ถ้ามี ทำการอัพเดทข้อมูล
                    if (_data != null)
                    {

                        starttime ??= _data.StartTime;
                        endtime ??= _data.EndTime;
                        starttime2 ??= _data.StartTime2;
                        endtime2 ??= _data.EndTime2;
                        starttime3 ??= _data.StartTime3;
                        endtime3 ??= _data.EndTime3;
                        starttime4 ??= _data.StartTime4;
                        endtime4 ??= _data.EndTime4;
                        starttime5 ??= _data.StartTime5;
                        endtime5 ??= _data.EndTime5;
                        starttime6 ??= _data.StartTime6;
                        endtime6 ??= _data.EndTime6;
                        starttime7 ??= _data.StartTime7;
                        endtime7 ??= _data.EndTime7;
                        starttime8 ??= _data.StartTime8;
                        endtime8 ??= _data.EndTime8;

                        service ??= _data.IsPickUp + "," + _data.IsDineIn + "," + _data.IsDelivery;
                        service2 ??= _data.IsPickUp2 + "," + _data.IsDineIn2 + "," + _data.IsDelivery2;
                        service3 ??= _data.IsPickUp3 + "," + _data.IsDineIn3 + "," + _data.IsDelivery3;
                        service4 ??= _data.IsPickUp4 + "," + _data.IsDineIn4 + "," + _data.IsDelivery4;
                        service5 ??= _data.IsPickUp5 + "," + _data.IsDineIn5 + "," + _data.IsDelivery5;
                        service6 ??= _data.IsPickUp6 + "," + _data.IsDineIn6 + "," + _data.IsDelivery6;
                        service7 ??= _data.IsPickUp7 + "," + _data.IsDineIn7 + "," + _data.IsDelivery7;
                        service8 ??= _data.IsPickUp8 + "," + _data.IsDineIn8 + "," + _data.IsDelivery8;



                        if (data.Everyday.IsOpen == true)
                        {
                           var status = await dbConnection.ExecuteAsync(@"UPDATE [dbo].[OpeningHour] SET StartTime8 = convert(time,@StartTime8),EndTime8 = convert(time,@EndTime8),Service8 = @Service8,IsEveryday = @Everyday,IsEnable = 1 WHERE RES_ID = @RES_ID",
                          new
                          {
                              StartTime8 = starttime8,
                              EndTime8 = endtime8,
                              Service8 = service8,
                              Everyday = data.Everyday.IsOpen,
                              RES_ID = resid
                          });
                            if (status == 1)
                            {
                                return 2;
                            }
                            else
                            {
                                return 0;
                            }
                        }
                        else
                        {
                            var status = await dbConnection.ExecuteAsync(@"UPDATE [dbo].[OpeningHour] SET DayOfWeek = @Day
                                                                        ,StartTime = convert(time,@StartTime),EndTime = convert(time,@EndTime),Service = @Service 
                                                                        ,StartTime2 = convert(time,@StartTime2),EndTime2 = convert(time,@EndTime2),Service2 = @Service2 
                                                                        ,StartTime3 = convert(time,@StartTime3),EndTime3 = convert(time,@EndTime3),Service3 = @Service3 
                                                                        ,StartTime4 = convert(time,@StartTime4),EndTime4 = convert(time,@EndTime4),Service4 = @Service4 
                                                                        ,StartTime5 = convert(time,@StartTime5),EndTime5 = convert(time,@EndTime5),Service5 = @Service5 
                                                                        ,StartTime6 = convert(time,@StartTime6),EndTime6 = convert(time,@EndTime6),Service6 = @Service6 
                                                                        ,StartTime7 = convert(time,@StartTime7),EndTime7 = convert(time,@EndTime7),Service7 = @Service7 
                                                                        ,IsEveryday = @Everyday,IsEnable = 1
                                                                        WHERE RES_ID = @RES_ID",
                         new
                         {
                             Day = dayOfweek,
                             StartTime = starttime,
                             EndTime = endtime,
                             StartTime2 = starttime2,
                             EndTime2 = endtime2,
                             StartTime3 = starttime3,
                             EndTime3 = endtime3,
                             StartTime4 = starttime4,
                             EndTime4 = endtime4,
                             StartTime5 = starttime5,
                             EndTime5 = endtime5,
                             StartTime6 = starttime6,
                             EndTime6 = endtime6,
                             StartTime7 = starttime7,
                             EndTime7 = endtime7,
                             StartTime8 = starttime8,
                             EndTime8 = endtime8,
                             Service = service,
                             Service2 = service2,
                             Service3 = service3,
                             Service4 = service4,
                             Service5 = service5,
                             Service6 = service6,
                             Service7 = service7,
                             Service8 = service8,
                             Everyday = data.Everyday.IsOpen,
                             RES_ID = resid
                         });
                            if (status == 1)
                            {
                                return 2;
                            }
                            else
                            {
                                return 0;
                            }
                        }

                    }                                 
                    else
                    {                      

                        var status = await dbConnection.ExecuteAsync(@"INSERT INTO [dbo].[OpeningHour](RES_ID,DayOfWeek,StartTime,EndTime,Service,StartTime2,EndTime2,Service2,StartTime3,EndTime3,Service3,StartTime4,EndTime4,Service4,StartTime5,EndTime5,Service5,StartTime6,EndTime6,Service6,StartTime7,EndTime7,Service7,StartTime8,EndTime8,Service8,IsEveryday) 
                                                                           VALUES(@RES_ID,@Day,convert(time,@StartTime),convert(time,@EndTime),@Service,convert(time,@StartTime2),convert(time,@EndTime2),@Service2,convert(time,@StartTime3),convert(time,@EndTime3),@Service3,convert(time,@StartTime4),convert(time,@EndTime4),@Service4,convert(time,@StartTime5),convert(time,@EndTime5),@Service5,convert(time,@StartTime6),convert(time,@EndTime6),@Service6,convert(time,@StartTime7),convert(time,@EndTime7),@Service7,convert(time,@StartTime8),convert(time,@EndTime8),@Service8,@Everyday)",
                            new
                            {
                                Day = dayOfweek,
                                StartTime = starttime,
                                EndTime = endtime,
                                StartTime2 = starttime2,
                                EndTime2 = endtime2,
                                StartTime3 = starttime3,
                                EndTime3 = endtime3,
                                StartTime4 = starttime4,
                                EndTime4 = endtime4,
                                StartTime5 = starttime5,
                                EndTime5 = endtime5,
                                StartTime6 = starttime6,
                                EndTime6 = endtime6,
                                StartTime7 = starttime7,
                                EndTime7 = endtime7,
                                StartTime8 = starttime8,
                                EndTime8 = endtime8,
                                Service = service,
                                Service2 = service2,
                                Service3 = service3,
                                Service4 = service4,
                                Service5 = service5,
                                Service6 = service6,
                                Service7 = service7,
                                Service8 = service8,
                                Everyday = data.Everyday.IsOpen,
                                RES_ID = resid
                            });
                        if (status == 1)
                        {
                            return 1;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> DeleteOpeningHour(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("UPDATE [dbo].[OpeningHour] SET IsEnable = 0 WHERE RES_ID = @RESID ");
                    var status = await dbConnection.ExecuteAsync(query.ToString(), new { RESID = resid});
                    if (status == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ViewMenuHour>> GetAllMenuHour(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var sb = new StringBuilder();
                    int sequence = 2;
                    sb.AppendLine("SELECT [ID],[Name_EN],[Name_TH],[Sequence],[CreatedDate],[TaxType] FROM [dbo].[MenuHour]");
                    sb.AppendLine("WHERE RES_ID = @RES_ID and IsEnable = 1 order by [Sequence]");
                    var results = await dbConnection.QueryAsync<ViewMenuHour>(sb.ToString(), new { RES_ID = resid });
                    var _menuGroup = await dbConnection.QueryAsync<ViewMenuGroup>("SELECT MG.[ID],MG.[Name],MG.[MenuHour_ID],MG.[CreatedDate] FROM [dbo].[MenuGroup] MG inner join MenuHour MH on MG.MenuHour_ID = MH.ID where MH.RES_ID = @RES_ID and MG.IsEnable = 1  ORDER BY MG.Name ", new { RES_ID = resid });
                    if (results != null)
                        if (_menuGroup != null)
                        {
                            foreach (var item in _menuGroup)
                            {
                                if (item.Name == "All")
                                {
                                    item.Sequence = 1;
                                }
                                else if (item.Name == "Oho Surprise Box" || item.Name == "Oho! Surprise Box")
                                {
                                    item.Sequence = 2;
                                }
                                else
                                {
                                    sequence += 1;
                                    item.Sequence = sequence;
                                }

                            }
                        }
                    foreach (var _menuhour in results)
                        {
                            _menuhour.MenuGroups = _menuGroup.Where(x => x.MenuHour_ID == _menuhour.ID).OrderBy(x => x.Sequence).ToList();
                        }
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<ViewMenuGroup> GetMenuGroupByID(int MenuGroup_ID, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = await dbConnection.QueryFirstOrDefaultAsync<ViewMenuGroup>("SELECT * FROM [dbo].[MenuGroup]  where ID = @ID and IsEnable = 1", new { ID = MenuGroup_ID });

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> PostMenuGroup(ViewMenuGroup data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("INSERT INTO dbo.MenuGroup (Name,MenuHour_ID) VALUES (@Name,@MenuHour_ID)",
                                                                new { Name = data.Name, MenuHour_ID = data.MenuHour_ID });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutMenuGroup(ViewMenuGroup data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("UPDATE dbo.MenuGroup SET Name=@Name,MenuHour_ID=@MenuHour_ID WHERE ID=@ID",
                                                                new { ID = data.ID, Name = data.Name, MenuHour_ID = data.MenuHour_ID });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> DeleteMenuGroup(int MenuGroup_ID)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                     var status = await dbConnection.ExecuteAsync("DELETE [dbo].[MenuGroup] WHERE ID = @ID",
                                                                new { ID = MenuGroup_ID });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ViewMenuItem>> GetMenuItemByMenuGroupID(int MenuGroupID, int res_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT Name FROM [dbo].[MenuGroup] WHERE ID = @ID AND IsEnable = 1");
                    var name = await dbConnection.QueryFirstOrDefaultAsync<string>(query.ToString(), new { ID = MenuGroupID });


                    query = new StringBuilder();
                    query.AppendLine("select MI.*,MH.TaxType,MG.Name as MenuGroup_Name from MenuHour MH");
                    query.AppendLine("inner join MenuGroup MG on MG.MenuHour_ID = MH.ID");
                    query.AppendLine("inner join MenuItem MI on MI.MenuGroup_ID = MG.ID");
                    if (name == "All")
                    {
                        query.AppendLine("where RES_ID = @resid and MI.IsEnable = 1");
                    }
                    else
                    {
                        query.AppendLine("where RES_ID = @resid and MG.ID = @MENUGROUP and MI.IsEnable = 1");
                    }
                    var results = await dbConnection.QueryAsync<ViewMenuItem>(query.ToString(), new { MENUGROUP = MenuGroupID, resid = res_id });
                    if (results.ToList().Count > 0)
                    {

                        foreach (var item in results)
                        {
                            query = new StringBuilder();
                            query.AppendLine("SELECT A.*  FROM [dbo].[AddOn] as A");
                            query.AppendLine("INNER JOIN [dbo].[MenuItemAddOn] as B");
                            query.AppendLine("ON B.AddOn_ID = A.ID");
                            query.AppendLine("WHERE B.MenuItem_ID = @ID  AND B.IsEnable = 1");
                            var addOn = await dbConnection.QueryAsync<ViewAddOnItem>(query.ToString(), new { ID = item.ID });
                            if (addOn != null)
                            {
                                item.AddOns = addOn.ToList();
                                foreach (var item1 in addOn)
                                {
                                    var addOption = await dbConnection.QueryAsync<ViewAddOnOption>("SELECT * FROM [dbo].[AddOnOption] WHERE AddOn_ID = @ID", new { ID = item1.ID });
                                    item1.Options = addOption.ToList();
                                }
                            }
                        }
                    }
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<ViewMenuItem> GetMenuItemByID(int MenuItem_ID, int res_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("select MI.*,MH.TaxType,MG.Name as MenuGroup_Name from MenuHour MH");
                    query.AppendLine("inner join MenuGroup MG on MG.MenuHour_ID = MH.ID");
                    query.AppendLine("inner join MenuItem MI on MI.MenuGroup_ID = MG.ID");
                    query.AppendLine("where RES_ID = @resid and MI.ID = @MENUITEM and MI.IsEnable = 1");
                    var results = await dbConnection.QueryFirstOrDefaultAsync<ViewMenuItem>(query.ToString(), new { MENUITEM = MenuItem_ID, resid = res_id });
                    if (results != null)
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT A.*  FROM [dbo].[AddOn] as A");
                        query.AppendLine("INNER JOIN [dbo].[MenuItemAddOn] as B");
                        query.AppendLine("ON B.AddOn_ID = A.ID");
                        query.AppendLine("WHERE B.MenuItem_ID = @ID AND B.IsEnable = 1");
                        var addOn = await dbConnection.QueryAsync<ViewAddOnItem>(query.ToString(), new { ID = results.ID });
                        if (addOn != null)
                        {
                            results.AddOns = addOn.ToList();

                            foreach (var item in addOn)
                            {
                                var addOption = await dbConnection.QueryAsync<ViewAddOnOption>("SELECT * FROM [dbo].[AddOnOption] WHERE AddOn_ID = @ID",new { ID = item.ID });
                                item.Options = addOption.ToList();
                            }
                        }

                    }
                    return results;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<ViewMenuItem> GetCreateMenuItem(int MenuGroup_ID, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var createDate = DateTime.UtcNow.AddHours(9);
                    var result = new ViewMenuItem();
                    var query = new StringBuilder();
                    query.AppendLine("INSERT INTO [MenuItem] ([MenuGroup_ID],[CookingTime],[Status],[CreatedDate]) VALUES (@GROUPID,(SELECT [DefaultCookingTime] FROM [RESTAURANT] WHERE RES_ID = @RESID),0,@DATE)");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { GROUPID = MenuGroup_ID, RESID = resid, DATE = createDate });
                    if (status == 1)
                    {
                        query = new StringBuilder();
                        query.AppendLine("select MI.* from MenuHour MH");
                        query.AppendLine("inner join MenuGroup MG on MG.MenuHour_ID = MH.ID");
                        query.AppendLine("inner join MenuItem MI on MI.MenuGroup_ID = MG.ID");
                        query.AppendLine("where RES_ID = @RESID and MI.[CreatedDate] = @DATE and MI.Status = 0");
                        result = await dbConnection.QueryFirstOrDefaultAsync<ViewMenuItem>(query.ToString(), new { DATE = createDate, RESID = resid });
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> PutMenuItem(EditMenuItem data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("select MI.* from MenuHour MH");
                    query.AppendLine("inner join MenuGroup MG on MG.MenuHour_ID = MH.ID");
                    query.AppendLine("inner join MenuItem MI on MI.MenuGroup_ID = MG.ID");
                    query.AppendLine("where RES_ID = @RESID and MI.[ID] = @MENUITEM");
                    var menuItem = await dbConnection.QueryFirstOrDefaultAsync<ViewMenuItem>(query.ToString(), new { MENUITEM = data.ID, RESID = resid });

                    data.Name_EN ??= menuItem.Name_EN;
                    data.Name_TH ??= menuItem.Name_TH;
                    data.Desc_EN ??= menuItem.Desc_EN;
                    data.Desc_TH ??= menuItem.Desc_TH;
                    data.Price ??= menuItem.Price;
                    data.CookingTime ??= menuItem.CookingTime;
                    data.MenuGroup_ID ??= menuItem.MenuGroup_ID;
                    data.Status ??= menuItem.Status;
                    query = new StringBuilder();
                    query.AppendLine("UPDATE [MenuItem] SET Name_EN = @NAME_EN, Name_TH = @NAME_TH, Desc_EN = @DESC_EN, Desc_TH = @DESC_TH,");
                    query.AppendLine("Price = @PRICE,MenuGroup_ID = @GROUPID,[CookingTime] = @COOKING, Status = @Status");
                    query.AppendLine("WHERE ID = @ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { ID = data.ID, NAME_EN = data.Name_EN, NAME_TH = data.Name_TH, DESC_EN = data.Desc_EN, DESC_TH = data.Desc_TH, PRICE = data.Price, GROUPID = data.MenuGroup_ID, COOKING = data.CookingTime, Status = data.Status });
                    if (status == 0)
                    {
                        return false;
                    }
                    if (data.AddOns != null)
                    {
                        var resetAddOn = await dbConnection.ExecuteAsync("UPDATE [dbo].[MenuItemAddOn] SET  IsEnable = 0 WHERE MenuItem_ID = @ID", new { ID = data.ID });
                        foreach (var item in data.AddOns)
                        {


                            var AddOnID = await dbConnection.QueryFirstOrDefaultAsync<EditAddOn>("SELECT * FROM [dbo].[MenuItemAddOn] WHERE MenuItem_ID = @ID AND AddOn_ID = @AddOn_ID", new { ID = data.ID, AddOn_ID = item.AddOnID });

                            query = new StringBuilder();
                            if (AddOnID != null)
                            {
                                status = await dbConnection.ExecuteAsync("UPDATE [dbo].[MenuItemAddOn] SET  IsEnable = 1 WHERE AddOn_ID = @ID", new { ID = item.AddOnID });
                            }
                            else
                            {
                                var checkAddOn = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT ID FROM [dbo].[AddOn] WHERE ID = @ID", new { ID = item.AddOnID });
                                if (checkAddOn > 0)
                                {
                                    query.AppendLine("INSERT INTO [dbo].[MenuItemAddOn](MenuItem_ID,AddOn_ID) VALUES(@MenuItem_ID,@AddOn_ID)");
                                    status = await dbConnection.ExecuteAsync(query.ToString(), new { MenuItem_ID = data.ID, AddOn_ID = item.AddOnID });
                                }
                            }
                            if (status == 0)
                            {
                                return false;
                            }
                        }
                    }

                  
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> DeleteMenuItem(int MenuItem_ID, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("select MI.* from MenuHour MH");
                    query.AppendLine("inner join MenuGroup MG on MG.MenuHour_ID = MH.ID");
                    query.AppendLine("inner join MenuItem MI on MI.MenuGroup_ID = MG.ID");
                    query.AppendLine("where RES_ID = @RESID and MI.[ID] = @MENUITEM");
                    var menuItem = await dbConnection.QueryFirstOrDefaultAsync<ViewMenuItem>(query.ToString(), new { MENUITEM = MenuItem_ID, RESID = resid });
                    if (menuItem == null)
                        return false;
                    query = new StringBuilder();
                    query.AppendLine("UPDATE dbo.MenuItem SET IsEnable = 0 WHERE ID = @ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { ID = MenuItem_ID });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> PutRestaurantItemImage(ResultUploadItemImage data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("UPDATE [MenuItem] SET [ImageM] = @FILENAME WHERE  [ID] = @ITEMID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { ITEMID = data.MenuItem_ID, FILENAME = data.FileName });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ViewAddOnItem>> GetAllAddOn(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [dbo].[AddOn] WHERE RES_ID = @RES_ID AND Status = 1");
                    var listAddOn = await dbConnection.QueryAsync<ViewAddOnItem>(query.ToString(), new { RES_ID = resid });
                    if (listAddOn.ToList().Count > 0)
                    {
                        var addonStrings = listAddOn.Select(i => i.ID.ToString(CultureInfo.InvariantCulture)).Aggregate((s1, s2) => s1 + ", " + s2);
                        query = new StringBuilder();
                        query.AppendLine("select * from AddOnOption WHERE  AddOn_ID IN (" + addonStrings + ")");
                        var listAddOnOption = await dbConnection.QueryAsync<ViewAddOnOption>(query.ToString(), new { });
                        if (listAddOn != null)
                            foreach (var item in listAddOn)
                            {
                                item.Options = listAddOnOption.Where(x => x.AddOn_ID == item.ID).ToList();
                            }
                    }
                    return listAddOn.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ViewAddOnItem> PostAddOn(PostAddOnItem data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var createDate = DateTime.Now;
                    var status = 0;
                    var result = new ViewAddOnItem();
                    var query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[AddOn](RES_ID,Name_EN,Name_TH,Require,OptionCount,Status,IsEnable,CreatedDate) VALUES(@RES_ID,@Name_EN,@Name_TH,@Require,@OptionCount,@Status,1,@CreatedDate)");

                    status = await dbConnection.ExecuteAsync(query.ToString(),
                        new
                        {
                            RES_ID = resid,
                            Name_EN = data.Name_EN,
                            Name_TH = data.Name_TH,
                            Require = data.Require,
                            OptionCount = data.OptionCount,
                            Status = data.Status,
                            CreatedDate = createDate
                        });
                    if (data.Options != null)
                    {
                        foreach (var item in data.Options)
                        {
                            query = new StringBuilder();
                            query.AppendLine("select * from [AddOn]");
                            query.AppendLine("where RES_ID = @RESID and [CreatedDate] = @DATE and Status = 1");
                            result = await dbConnection.QueryFirstOrDefaultAsync<ViewAddOnItem>(query.ToString(), new { DATE = createDate, RESID = resid });


                            query = new StringBuilder();
                            query.AppendLine("INSERT INTO [dbo].[AddOnOption](AddOn_ID,Name_EN,Name_TH,Price,CreatedDate,Status) VALUES(@AddOn_ID,@Name_EN,@Name_TH,@Price,@CreatedDate,1)");
                            status = await dbConnection.ExecuteAsync(query.ToString(),
                                new
                                {
                                    AddOn_ID = result.ID,
                                    Name_EN = item.Name_EN,
                                    Name_TH = item.Name_TH,
                                    Price = item.Price,
                                    CreatedDate = createDate
                                });
                        }
                    }
                    if (status == 1)
                    {
                        query = new StringBuilder();
                        query.AppendLine("select * from [AddOn]");
                        query.AppendLine("where RES_ID = @RESID and [CreatedDate] = @DATE and Status = 1");
                        result = await dbConnection.QueryFirstOrDefaultAsync<ViewAddOnItem>(query.ToString(), new { DATE = createDate, RESID = resid });

                        query = new StringBuilder();
                        query.AppendLine("SELECT A.* FROM [dbo].[AddOnOption] as A ");
                        query.AppendLine("INNER JOIN [dbo].[AddOn] as B ");
                        query.AppendLine("ON B.ID = A.AddOn_ID");
                        query.AppendLine("WHERE B.RES_ID = @RESID and A.CreatedDate = @DATE and A.Status = 1");
                        var addOnOption = await dbConnection.QueryAsync<ViewAddOnOption>(query.ToString(), new { DATE = createDate, RESID = resid });
                        if (addOnOption != null)
                        {
                            result.Options = addOnOption.ToList();
                        }

                        return result;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ViewAddOnItem> PutAddOn(PutAddOnItem data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var createDate = DateTime.Now;
                    var status = 0;
                    var result = new ViewAddOnItem();
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [AddOn]");
                    query.AppendLine("WHERE RES_ID = @RESID and [ID] = @ADDONID AND IsEnable = 1");
                    var addonItem = await dbConnection.QueryFirstOrDefaultAsync<PostAddOnItem>(query.ToString(), new { ADDONID = data.ID, RESID = resid });
                    if (addonItem != null)
                    {
                        query = new StringBuilder();
                        query.AppendLine("UPDATE [AddOn] SET Name_EN = @Name_EN, Name_TH = @Name_TH,Require = @Require,OptionCount = @OptionCount");
                        query.AppendLine("WHERE RES_ID = @RESID AND ID = @ID");

                        data.Name_EN ??= addonItem.Name_EN;
                        data.Name_TH ??= addonItem.Name_TH;
                        data.Require ??= addonItem.Require;
                        data.OptionCount ??= addonItem.OptionCount;



                        status = await dbConnection.ExecuteAsync(query.ToString(),
                            new
                            {
                                RESID = resid,
                                ID = data.ID,
                                RES_ID = resid,
                                Name_EN = data.Name_EN,
                                Name_TH = data.Name_TH,
                                Require = data.Require,
                                OptionCount = data.OptionCount,
                            });
                    }
                    if (status == 1)
                    {
                        query = new StringBuilder();
                        query.AppendLine("select * from [AddOn]");
                        query.AppendLine("where RES_ID = @RESID and [CreatedDate] = @DATE and Status = 1");
                        result = await dbConnection.QueryFirstOrDefaultAsync<ViewAddOnItem>(query.ToString(), new { DATE = createDate, RESID = resid });
                        return result;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> DeleteAddOn(int AddOn_ID, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("select * from [AddOn]");
                    query.AppendLine("where RES_ID = @RESID and [ID] = @ADDONID");
                    var addonItem = await dbConnection.QueryFirstOrDefaultAsync<ViewAddOnItem>(query.ToString(), new { ADDONID = AddOn_ID, RESID = resid });
                    if (addonItem == null)
                        return false;
                    query = new StringBuilder();
                    query.AppendLine("DELETE dbo.[AddOn]  WHERE ID = @ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { ID = AddOn_ID });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewAddOnOption>> GetAllAddOnOption(int AddOnID, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT A.* FROM [dbo].[AddOnOption] as A ");
                    query.AppendLine("INNER JOIN [dbo].[AddOn] as B ");
                    query.AppendLine("ON B.ID = A.AddOn_ID");
                    query.AppendLine("WHERE RES_ID = @RES_ID AND A.AddOn_ID = @ID");

                    var data = await dbConnection.QueryAsync<ViewAddOnOption>(query.ToString(), new { RES_ID = resid , ID = AddOnID });
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ViewAddOnOption> PostAddOnOption(ViewAddOnOption data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var createDate = DateTime.Now;
                    var result = new ViewAddOnOption();
                    var status = 0;
                    var query = new StringBuilder();
                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[AddOnOption](AddOn_ID,Name_EN,Name_TH,Price,CreatedDate,Status) VALUES(@AddOn_ID,@Name_EN,@Name_TH,@Price,@CreatedDate,@Status)");
                    status = await dbConnection.ExecuteAsync(query.ToString(),
                        new
                        {
                            AddOn_ID = data.AddOn_ID,
                            Name_EN = data.Name_EN,
                            Name_TH = data.Name_TH,
                            Price = data.Price,
                            CreatedDate = createDate,
                            Status = data.Status
                        });
                    if (status == 1)
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT A.* FROM [dbo].[AddOnOption] as A ");
                        query.AppendLine("INNER JOIN [dbo].[AddOn] as B ");
                        query.AppendLine("ON B.ID = A.AddOn_ID");
                        query.AppendLine("WHERE B.RES_ID = @RESID and A.CreatedDate = @DATE and A.Status = 1");
                        result = await dbConnection.QueryFirstOrDefaultAsync<ViewAddOnOption>(query.ToString(), new { DATE = createDate, RESID = resid });
                        return result;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ViewAddOnOption> PutAddOnOption(ViewAddOnOption data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var createDate = DateTime.Now;
                    var result = new ViewAddOnOption();
                    var status = 0;
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [AddOnOption]");
                    query.AppendLine("WHERE AddOn_ID = @ADDONID and [ID] = @ID AND IsEnable = 1");
                    var addonOption = await dbConnection.QueryFirstOrDefaultAsync<ViewAddOnOption>(query.ToString(), new { ADDONID = data.AddOn_ID, ID = data.ID });
                    if (addonOption != null)
                    {
                        query = new StringBuilder();
                        query.AppendLine("UPDATE [AddOnOption] SET Name_EN = @Name_EN, Name_TH = @Name_TH, Price = @Price, Status = @Status");
                        query.AppendLine("WHERE AddOn_ID = @AddOn_ID AND ID = @ID");

                        data.AddOn_ID ??= addonOption.AddOn_ID;
                        data.Name_EN ??= addonOption.Name_EN;
                        data.Name_TH ??= addonOption.Name_TH;
                        data.Price ??= addonOption.Price;
                        data.Status ??= addonOption.Status;
                        status = await dbConnection.ExecuteAsync(query.ToString(), new { ID = data.ID, AddOn_ID = data.AddOn_ID, Name_EN = data.Name_EN, Name_TH = data.Name_TH, Price = data.Price, Status = data.Status });

                        if (status == 1)
                        {
                            query = new StringBuilder();
                            query.AppendLine("SELECT A.* FROM [dbo].[AddOnOption] as A ");
                            query.AppendLine("INNER JOIN [dbo].[AddOn] as B ");
                            query.AppendLine("ON B.ID = A.AddOn_ID");
                            query.AppendLine("WHERE AddOn_ID = @AddOn_ID AND A.ID = @ID AND B.RES_ID = @RES_ID");
                            result = await dbConnection.QueryFirstOrDefaultAsync<ViewAddOnOption>(query.ToString(), new { ID = data.ID, AddOn_ID = data.AddOn_ID, RES_ID = resid });
                            return result;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> DeleteAddOnOption(int AddOnOption_ID, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT A.* FROM [dbo].[AddOnOption] as A ");
                    query.AppendLine("INNER JOIN [dbo].[AddOn] as B ");
                    query.AppendLine("ON B.ID = A.AddOn_ID");
                    query.AppendLine("WHERE RES_ID = @RESID  AND A.ID = @ID");
                    var addonItem = await dbConnection.QueryFirstOrDefaultAsync<ViewAddOnItem>(query.ToString(), new { ID = AddOnOption_ID, RESID = resid });
                    if (addonItem == null)
                        return false;
                    query = new StringBuilder();
                    query.AppendLine("DELETE dbo.[AddOnOption]  WHERE ID = @ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { ID = AddOnOption_ID });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<DayOfRepeat> GetRepeatMenuItemByID(int MenuItem_ID)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var data = await dbConnection.QueryFirstOrDefaultAsync<DayOfRepeat>(@"SELECT (select value from fn_split_string_to_column([DayRepeat],',') a where column_id in (1)) as IsSun,
                                                                             (select value from fn_split_string_to_column([DayRepeat],',') a where column_id in (2)) as IsMon,
                                                                             (select value from fn_split_string_to_column([DayRepeat],',') a where column_id in (3)) as IsTue,
                                                                             (select value from fn_split_string_to_column([DayRepeat],',') a where column_id in (4)) as IsWed,
                                                                             (select value from fn_split_string_to_column([DayRepeat],',') a where column_id in (5)) as IsThu,
                                                                             (select value from fn_split_string_to_column([DayRepeat],',') a where column_id in (6)) as IsFri,
                                                                             (select value from fn_split_string_to_column([DayRepeat],',') a where column_id in (7)) as IsSat,
                                                                              IsEveryDay
                                                                              FROM [OhoApp].[dbo].[MenuItem] WHERE ID = @ID"
                                                                                , new { ID = MenuItem_ID });
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutRepeatMenuItem(DataMenuItem data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    var query = new StringBuilder();
                    var dayOfweek = (data.DayRepeat.IsSun == true ? 1 : 0) + "," + (data.DayRepeat.IsMon == true ? 1 : 0) + "," + (data.DayRepeat.IsTue == true ? 1 : 0) + "," + (data.DayRepeat.IsWed == true ? 1 : 0) + "," + (data.DayRepeat.IsThu == true ? 1 : 0) + "," + (data.DayRepeat.IsFri == true ? 1 : 0) + "," + (data.DayRepeat.IsSat == true ? 1 : 0);
                    

                    query.AppendLine("SELECT Quota,Discount FROM [dbo].[MenuItem] WHERE ID = @ID");
                    var _data = await dbConnection.QueryFirstOrDefaultAsync<DataMenuItem>(query.ToString(), new
                    {
                        ID = data.MenuItem_ID
                    });
                    if (_data != null)
                    {
                        query = new StringBuilder();
                        query.AppendLine("UPDATE [dbo].[MenuItem] SET ");
                        query.AppendLine("Quota = @QUOTA, DefaultQuota = @DEFAULTQUOTA, ");

                        if (data.DayRepeat.IsEveryDay == true)
                        {
                            query.AppendLine("Discount = @DISCOUNT,DayRepeat = @DAYREPEAT,IsEveryDay = 1");
                        }
                        else
                        {
                            query.AppendLine("Discount = @DISCOUNT,DayRepeat = @DAYREPEAT,IsEveryDay = 0");
                        }
                        query.AppendLine("WHERE ID = @ID");

                        var status = await dbConnection.ExecuteAsync(query.ToString(), new
                        {
                            ID = data.MenuItem_ID,
                            QUOTA = data.Quota,
                            DEFAULTQUOTA = data.Quota,
                            DISCOUNT = data.Discount,
                            DAYREPEAT = dayOfweek,
                        });
                        if (status == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<List<DataRepeat>> GetDataRepeatMenuItem(string day)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {


                    var query = new StringBuilder();
                    List<DataRepeat> _data = new List<DataRepeat>();
                    int number = 0;

                    if (day == "Sunday")
                    {
                        number = 1;
                    }
                    else if (day == "Monday")
                    {
                        number = 2;
                    }
                    else if (day == "Tuesday")
                    {
                        number = 3;
                    }
                    else if (day == "Wednesday")
                    {
                        number = 4;
                    }
                    else if (day == "Thursday")
                    {
                        number = 5;
                    }
                    else if (day == "Friday")
                    {
                        number = 6;
                    }
                    else if (day == "Saturday")
                    {
                        number = 7;
                    }


                    query.AppendLine(@"SELECT ID as 'MenuItem_ID',DefaultQuota
                                           FROM [dbo].[MenuItem]
                                           WHERE (select value from fn_split_string_to_column(DayRepeat,',') a where column_id in (@DAY)) = 1 OR IsEveryDay = 1");
                    var data = await dbConnection.QueryAsync<DataRepeat>(query.ToString(), new { DAY = number });

                    return data.ToList();

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutRepeatQuotaItem(int id, int quota)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {


                    var query = new StringBuilder();
                    query.AppendLine(@"UPDATE [dbo].[MenuItem] SET Quota = @QUOTA WHERE ID = @ID");
                    var status = await dbConnection.ExecuteAsync(query.ToString(), new { ID = id, QUOTA = quota });
                    if (status == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutMoveMenuItem(MoveMenuItem data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("select MI.* from MenuHour MH");
                    query.AppendLine("inner join MenuGroup MG on MG.MenuHour_ID = MH.ID");
                    query.AppendLine("inner join MenuItem MI on MI.MenuGroup_ID = MG.ID");
                    query.AppendLine("where RES_ID = @RESID and MI.[ID] = @MENUITEM");
                    var menuItem = await dbConnection.QueryFirstOrDefaultAsync<EditMenuItem>(query.ToString(), new { MENUITEM = data.MenuItem_ID, RESID = resid });
                    if (menuItem == null)
                    {
                        return false;
                    }
                    query = new StringBuilder();
                    query.AppendLine("UPDATE [MenuItem] SET [MenuGroup_ID] = @MENUGROUPNEW");
                    query.AppendLine("WHERE ID = @ITEMID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { ITEMID = data.MenuItem_ID, MENUGROUPNEW = data.MenuGroup_ID });
                    if (status == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutDuplicateMenuItem(DuplicateMenuItem data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var createDate = DateTime.Now;
                    var query = new StringBuilder();
                    query.AppendLine("select MI.* from MenuHour MH");
                    query.AppendLine("inner join MenuGroup MG on MG.MenuHour_ID = MH.ID");
                    query.AppendLine("inner join MenuItem MI on MI.MenuGroup_ID = MG.ID");
                    query.AppendLine("where RES_ID = @resid and MI.ID = @MENUITEM and MI.Status = 1");
                    var menuItem = await dbConnection.QueryFirstOrDefaultAsync<EditMenuItem>(query.ToString(), new { MENUITEM = data.MenuItem_ID, resid = resid });
                    if (menuItem != null)
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT ID as AddOnID, IsEnable  FROM [dbo].[AddOn] WHERE RES_ID = @RES_ID AND IsEnable = 1");
                        var listAddOn = await dbConnection.QueryAsync<EditAddOn>(query.ToString(), new { RES_ID = resid });
                        menuItem.AddOns = listAddOn.ToList();
                    }
                    else
                    {
                        return false;
                    }
                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [MenuItem] ([Name_EN],[Name_TH],[Desc_EN],[Desc_TH],[PercentTax],[Price],[Delivery],[DineIn]");
                    query.AppendLine(",[ImageM],[ImageL],[CookingTime],[OutOfOrder_Item],[MenuGroup_ID],[Status],[CreatedDate],[Pickup],[Sequence])");
                    query.AppendLine("SELECT [Name_EN],[Name_TH],[Desc_EN],[Desc_TH],[PercentTax],[Price],[Delivery],[DineIn]");
                    query.AppendLine(",[ImageM],[ImageL],[CookingTime],0,@MENUGROUP,1,@DATE,[Pickup],[Sequence] FROM [MenuItem]");
                    query.AppendLine("WHERE ID = @ITEMID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { ITEMID = data.MenuItem_ID, MENUGROUP = data.MenuGroup_ID, DATE = createDate });
                    if (status == 0)
                    {
                        return false;
                    }

                    if (menuItem.AddOns != null)
                        foreach (var item in menuItem.AddOns)
                        {
                            status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[MenuItemAddOn](MenuItem_ID,AddOn_ID,IsEnable,CreatedDate) VALUES(@MenuItem_ID,@AddOn_ID,@IsEnable,@CreatedDate)",
                                new 
                                {
                                    MenuItem_ID = menuItem.ID,
                                    IsEnable = item.IsEnable,
                                    AddOn_ID = item.AddOnID,
                                    CreatedDate = createDate
                                });
                            if (status == 0)
                            {
                                return false;
                            }
                        }
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutDuplicateAddOn(DuplicateAddOn data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    var createDate = DateTime.Now;
                    int status = 0;
                    query.AppendLine("SELECT *  FROM [dbo].[AddOn] WHERE ID = @ID AND RES_ID = @RESID");
                    var checkAddOn = await dbConnection.QueryFirstOrDefaultAsync<PutAddOnItem>(query.ToString(), new { ID = data.AddOn_ID, RESID = resid });
                    if (checkAddOn  != null)
                    {
                        query = new StringBuilder();
                        query.AppendLine("INSERT INTO [dbo].[AddOn](RES_ID,Name_EN,Name_TH,Require,OptionCount,Status,IsEnable,CreatedDate)  VALUES(@RES_ID,@Name_EN,@Name_TH,@Require,@OptionCount,@Status,@IsEnable,@CreatedDate)");
                        status = await dbConnection.ExecuteAsync(query.ToString(), 
                            new
                            {
                                RES_ID = resid,
                                Name_EN = checkAddOn.Name_EN,
                                Name_TH = checkAddOn.Name_TH,
                                Require = checkAddOn.Require,
                                OptionCount = checkAddOn.OptionCount,
                                Status = checkAddOn.Status,
                                IsEnable = checkAddOn.IsEnable,
                                CreatedDate = createDate
                            });
                        if (status == 1)
                        {
                            query = new StringBuilder();
                            query.AppendLine("SELECT ID FROM [dbo].[AddOn] WHERE RES_ID = @RES_ID AND CreatedDate = @CreatedDate");
                            var addOnID = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { RES_ID = resid, CreatedDate = createDate });
                            if (addOnID > 0)
                            {
                                query = new StringBuilder();
                                query.AppendLine("SELECT A.* FROM [dbo].[AddOnOption] as A INNER JOIN [dbo].[AddOn] as B ON B.ID = A.AddOn_ID WHERE A.AddOn_ID = @ID");
                                var addOnOption = await dbConnection.QueryAsync<ViewAddOnOption>(query.ToString(), new { ID = data.AddOn_ID });
                                if (addOnOption != null)
                                {
                                    foreach (var item in addOnOption)
                                    {
                                        query = new StringBuilder();
                                        query.AppendLine("INSERT INTO [dbo].[AddOnOption](AddOn_ID,Name_EN,Name_TH,Price,Status,IsEnable,CreatedDate) VALUES(@AddOn_ID,@Name_EN,@Name_TH,@Price,@Status,@IsEnable,@CreatedDate)");
                                        status = await dbConnection.ExecuteAsync(query.ToString(), 
                                            new
                                            {
                                                AddOn_ID = addOnID ,
                                                Name_EN = item.Name_EN,
                                                Name_TH = item.Name_TH,
                                                Price = item.Price,
                                                Status = item.Status,
                                                IsEnable = 1,
                                                CreatedDate = createDate
                                            });
                                    }
                                }
                            }                       
                        }
                        else
                        {
                            return false;
                        }

                    }
                    else
                    {
                        return false;
                    }              
                    if (status == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        #region Services Test

        public async Task<bool> PutStatusOTP(PutStatusOTP data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();

                    var verify = false;
                    if (data.IsVerify == false)
                    {
                        verify = false;
                    }
                    else
                    {
                        verify = true;
                    }

                    query.AppendLine("UPDATE [dbo].[RESTAURANT]  SET OTP_validate = @OTP,AlternativeVerify = @VERIFY WHERE RES_ID = @RESID ");

                    var status = await dbConnection.ExecuteAsync(query.ToString(), new { RESID = resid, OTP = data.IsVerify, VERIFY = verify });
                    if (status == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutStatusApprove(PutStatusOTP data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();

                    var verify = false;
                    if (data.IsVerify == false)
                    {
                        verify = false;
                    }
                    else
                    {
                        verify = true;
                    }

                    query.AppendLine("UPDATE [dbo].[RESTAURANT]  SET IsApproval = @VERIFY WHERE RES_ID = @RESID ");

                    var status = await dbConnection.ExecuteAsync(query.ToString(), new { RESID = resid, VERIFY = verify });
                    if (status == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        public async Task<bool> ResetRestaurantInfo(RestaurantInfo data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [dbo].[RESTAURANT] WHERE RES_ID=@RES_ID");
                    var _res = await dbConnection.QueryFirstOrDefaultAsync<RestaurantInfo>(query.ToString(), new { RES_ID = resid });
                    data.Email ??= _res.Email;
                    data.PhoneNumber ??= _res.PhoneNumber;


                    query = new StringBuilder();
                    query.AppendLine("UPDATE dbo.RESTAURANT SET RestaurantNameEN=@RestaurantNameEN,RestaurantNameTH=@RestaurantNameTH,");
                    query.AppendLine("Logo=@Logo,Location=@Location,Address=@Address,ConfirmAddress=@ConfirmAddress,");
                    query.AppendLine("Email=@Email,PhoneNumber=@PhoneNumber,OfficialPhoneNumber = @OfficialPhoneNumber,");
                    query.AppendLine("PreferredPhoneNumber = @PreferredPhoneNumber,PreferredEmail = @PreferredEmail, ServiceCharge = @ServiceCharge,");
                    query.AppendLine("Rate=@Rate,PriceRate=@PriceRate,Cuisine_ID=@Cuisine_ID,CuisineOtherName=@CuisineOtherName,");
                    query.AppendLine("Status=@Status,PlaceId=@PLACE WHERE RES_ID=@RES_ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(),
                        new
                        {
                            RES_ID = resid,
                            RestaurantNameEN = data.RestaurantNameEN,
                            RestaurantNameTH = data.RestaurantNameTH,
                            Logo = data.Logo,
                            Location = data.Location,
                            Address = data.Address,
                            ConfirmAddress = data.ConfirmAddress,
                            Email = data.Email,
                            PhoneNumber = data.PhoneNumber,
                            OfficialPhoneNumber = data.OfficialPhoneNumber,
                            PreferredPhoneNumber = data.PreferredPhoneNumber,
                            PreferredEmail = data.PreferredEmail,
                            ServiceCharge = data.ServiceCharge,
                            Rate = data.Rate,
                            PriceRate = data.PriceRate,
                            Cuisine_ID = data.Cuisine_ID,
                            CuisineOtherName = data.CuisineOtherName,
                            Status = data.Status,
                            PLACE = data.PlaceId
                        });

                    query = new StringBuilder();
                    query.AppendLine("UPDATE [dbo].[OpeningHour] SET IsEnable = 0 WHERE RES_ID = @RESID ");
                    status = await dbConnection.ExecuteAsync(query.ToString(), new { RESID = resid });

                    query = new StringBuilder();
                    query.AppendLine("UPDATE [dbo].[RESTAURANT]  SET OTP_validate = 0,AlternativeVerify = 0,IsApproval = 0 WHERE RES_ID = @RESID ");
                    status = await dbConnection.ExecuteAsync(query.ToString(), new { RESID = resid });

                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        public async Task<List<ViewRestaurantrOrderDetail>> GetCurrentOrder(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT A.ID,A.LogID,A.OrderID,A.Order_NO,A.Order_Type,A.RES_ID,A.SubTotal,A.SmallOrderFee,A.ServiceCharge,A.Commission,A.Discount,A.Total");
                    query.AppendLine(",A.StartTime,A.EndTime,A.PaymentMethod,A.PaymentStatus,A.Accept_Time,A.Cooked_Time,A.CookingTime,A.Distance,A.Delivery_Start,A.Delivery_End");
                    query.AppendLine(",A.Complete_Time,A.CreatedDate,A.RestaurantStatus as Status,B.Location as RestaurantLocation, F.Location as CustomerLocation,A.CUS_ID,C.Firstname +  '  '   + C.Lastname as CustomerName,C.PhoneNumber as CustomerPhoneNumber ");
                    query.AppendLine("FROM [Order] A INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("INNER JOIN (SELECT ID,Order_NO,RES_ID FROM [Order] WHERE  (Order_Type IN (1,2) AND  RestaurantStatus < 20 AND RestaurantStatus != 0 ) OR (Order_Type = 3 AND PaymentStatus = 1 AND  RestaurantStatus < 20 AND RestaurantStatus != 0 )  ) D ON A.ID = D.ID");
                    query.AppendLine("LEFT JOIN OrderAddress F ON A.ID = F.Order_ID");
                    query.AppendLine("WHERE A.RES_ID = @RES_ID ORDER BY A.ID DESC");
                    var results = await dbConnection.QueryAsync<ViewRestaurantrOrderDetail>(query.ToString(), new { RES_ID = resid });
                    if (results != null)
                        foreach (var item in results)
                        {
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.ID,MI.Name_EN,MI.Name_TH,MI.Desc_EN,MI.Desc_TH,a.Amount,a.Price,MH.TaxType");
                            query.AppendLine(",a.Comment,MI.MenuGroup_ID");
                            query.AppendLine("FROM [OrderItem] a");
                            query.AppendLine("INNER JOIN MenuItem MI ON MI.ID = a.MenuItem_ID");
                            query.AppendLine("INNER JOIN MenuGroup MG ON MG.ID = MI.MenuGroup_ID");
                            query.AppendLine("INNER JOIN MenuHour MH ON MG.MenuHour_ID = MH.ID");
                            query.AppendLine("WHERE a.Order_ID = @ORDER");
                            var orderItems = await dbConnection.QueryAsync<ViewOrderItem>(query.ToString(), new { ORDER = item.ID });
                            item.Items = orderItems.ToList();
                            if (item.Items.Count > 0)
                            {
                                foreach (var itemAddOn in orderItems)
                                {
                                    var AddOnOption = await dbConnection.QueryAsync<ViewAddOnOrderOption>(@"SELECT A.ID,B.AddOn_ID,A.AddOnOption_ID,A.OrderItem_ID,B.Name_EN,B.Name_TH,A.Price FROM [dbo].[OrderItemAddOn] as A  INNER JOIN [dbo].[AddOnOption] as B ON B.ID = A.AddOnOption_ID  WHERE  A.OrderItem_ID = @ORDER ",
                                        new
                                        {
                                            ORDER = itemAddOn.ID

                                        });
                                    if (AddOnOption != null)
                                    {
                                        itemAddOn.Options = AddOnOption.ToList();
                                    }
                                }
                            }
                            item.Distance = (item.CustomerLocation == null || item.RestaurantLocation == null ? 0m : (decimal)CalculateDistance(GetLocation(item.CustomerLocation), GetLocation(item.RestaurantLocation)));
                            item.OrderRef = await dbConnection.QueryFirstOrDefaultAsync<string>("SELECT OrderRef  FROM [OhoApp].[Delivery].[RateDetails] WHERE OrderNO = @OrderNO ", new { OrderNO = item.Order_No });

                            if (item.OrderRef != null)
                            {
                                item.DriverInfo = await dbConnection.QueryFirstOrDefaultAsync<DriverInfo>("SELECT [DriverID]  ,[DriverName]  ,[DriverPhone] ,[PlateNumber] ,[Photo] FROM [OhoApp].[Delivery].[DriverDetails] WHERE OrderRef = @ID", new { ID = item.OrderRef });
                            }
                            item.OrderAddress = await dbConnection.QueryFirstOrDefaultAsync<OrderAddress>("SELECT * FROM OrderAddress where Order_ID = @ORDER_ID", new { ORDER_ID = item.ID });
                            item.Distance = (item.OrderAddress == null || item.RestaurantLocation == null ? 0m : (decimal)CalculateDistance(GetLocation(item.OrderAddress.Location), GetLocation(item.RestaurantLocation)));
                        }
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewRestaurantrOrderDetail>> GetHistoryOrder(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT A.ID,A.LogID,A.OrderID,A.Order_NO,A.Order_Type,A.RES_ID,A.SubTotal,A.SmallOrderFee,A.ServiceCharge,A.Commission,A.Discount,A.Total");
                    query.AppendLine(",A.StartTime,A.EndTime,A.PaymentMethod,A.PaymentStatus,A.Accept_Time,A.Cooked_Time,A.CookingTime,A.Distance,A.Delivery_Start,A.Delivery_End");
                    query.AppendLine(",A.Complete_Time,A.CreatedDate,A.RestaurantStatus as Status,B.Location as RestaurantLocation, F.Location as CustomerLocation,A.CUS_ID,C.Firstname +  '  '   + C.Lastname as CustomerName,C.PhoneNumber as CustomerPhoneNumber ");
                    query.AppendLine("FROM [Order] A INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("INNER JOIN (SELECT ID FROM [Order] WHERE (Order_Type IN (1,2,3) AND RestaurantStatus >= 20)) D ON A.ID = D.ID ");
                    query.AppendLine("LEFT JOIN OrderAddress F ON A.ID = F.Order_ID");
                    query.AppendLine("WHERE A.RES_ID = @RES_ID ORDER BY A.ID DESC");
                    var results = await dbConnection.QueryAsync<ViewRestaurantrOrderDetail>(query.ToString(), new { RES_ID = resid });
                    foreach (var item in results)
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT a.ID,MI.Name_EN,MI.Name_TH,MI.Desc_EN,MI.Desc_TH,a.Amount,a.Price,MH.TaxType");
                        query.AppendLine(",a.Comment,MI.MenuGroup_ID");
                        query.AppendLine("FROM [OrderItem] a");
                        query.AppendLine("INNER JOIN MenuItem MI ON MI.ID = a.MenuItem_ID");
                        query.AppendLine("INNER JOIN MenuGroup MG ON MG.ID = MI.MenuGroup_ID");
                        query.AppendLine("INNER JOIN MenuHour MH ON MG.MenuHour_ID = MH.ID");
                        query.AppendLine("WHERE a.Order_ID = @ORDER");
                        var orderItems = await dbConnection.QueryAsync<ViewOrderItem>(query.ToString(), new { ORDER = item.ID });
                        item.Items = orderItems.ToList();
                        if (item.Items.Count > 0)
                        {
                            foreach (var itemAddOn in orderItems)
                            {
                                var AddOnOption = await dbConnection.QueryAsync<ViewAddOnOrderOption>(@"SELECT A.ID,B.AddOn_ID,A.AddOnOption_ID,A.OrderItem_ID,B.Name_EN,B.Name_TH,A.Price FROM [dbo].[OrderItemAddOn] as A  INNER JOIN [dbo].[AddOnOption] as B ON B.ID = A.AddOnOption_ID  WHERE  A.OrderItem_ID = @ORDER ",
                                    new
                                    {
                                        ORDER = itemAddOn.ID

                                    });
                                if (AddOnOption != null)
                                {
                                    itemAddOn.Options = AddOnOption.ToList();
                                }
                            }
                        }
                        item.OrderAddress = await dbConnection.QueryFirstOrDefaultAsync<OrderAddress>("SELECT * FROM OrderAddress where Order_ID = @ORDER_ID", new { ORDER_ID = item.ID });
                        item.Distance = (item.CustomerLocation == null || item.RestaurantLocation == null ? 0m : (decimal)CalculateDistance(GetLocation(item.CustomerLocation), GetLocation(item.RestaurantLocation)));
                        item.OrderRef = await dbConnection.QueryFirstOrDefaultAsync<string>("SELECT OrderRef  FROM [OhoApp].[Delivery].[RateDetails] WHERE OrderNO = @OrderNO ", new { OrderNO = item.Order_No });

                        if (item.OrderRef != null)
                        {
                            item.DriverInfo = await dbConnection.QueryFirstOrDefaultAsync<DriverInfo>("SELECT [DriverID]  ,[DriverName]  ,[DriverPhone] ,[PlateNumber] ,[Photo] FROM [OhoApp].[Delivery].[DriverDetails] WHERE OrderRef = @ID", new { ID = item.OrderRef });
                        }
                    }
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ViewRestaurantrOrderDetail> GetOrderByID(int orderid ,int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT A.ID,A.LogID,A.OrderID,A.Order_NO,A.Order_Type,A.RES_ID,A.SubTotal,A.SmallOrderFee,A.ServiceCharge,A.Commission,A.Discount,A.Total");
                    query.AppendLine(",A.StartTime,A.EndTime,A.PaymentMethod,A.PaymentStatus,A.Accept_Time,A.Cooked_Time,A.CookingTime,A.Distance,A.Delivery_Start,A.Delivery_End");
                    query.AppendLine(",A.Complete_Time,A.CreatedDate,A.RestaurantStatus as Status,B.Location as RestaurantLocation, F.Location as CustomerLocation,A.CUS_ID,C.Firstname +  '  '   + C.Lastname as CustomerName,C.PhoneNumber as CustomerPhoneNumber ");
                    query.AppendLine("FROM [Order] A INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("LEFT JOIN OrderAddress F ON A.ID = F.Order_ID");
                    query.AppendLine("WHERE A.RES_ID = @RES_ID AND A.ID = @ORDER_ID");
                    var result = await dbConnection.QueryFirstOrDefaultAsync<ViewRestaurantrOrderDetail>(query.ToString(), new { RES_ID = resid, ORDER_ID = orderid });
                    if (result != null)
                    {
                        //Address Order AND Distance
                        result.OrderAddress = await dbConnection.QueryFirstOrDefaultAsync<OrderAddress>("SELECT * FROM OrderAddress where Order_ID = @ORDER_ID", new { ORDER_ID = result.ID });
                        result.Distance = (result.OrderAddress == null || result.RestaurantLocation == null ? 0m : (decimal)CalculateDistance(GetLocation(result.OrderAddress.Location), GetLocation(result.RestaurantLocation)));

                        //Order Rate
                        query = new StringBuilder();
                        query.AppendLine("SELECT O.ID as Order_ID,R.Rate as FoodRate,R.Comment as FoodComment,D.Rate as DriverRate,D.Comment as DriverComment FROM [dbo].[Order] as O LEFT JOIN [dbo].[RestaurantRate] as R ON R.Order_ID = O.ID");
                        query.AppendLine("LEFT JOIN [dbo].[DriverRate] as D ON D.Order_ID = R.Order_ID");
                        query.AppendLine("WHERE O.ID = @ORDER_ID AND O.RES_ID = @RES_ID");
                        result.OrderRate = await dbConnection.QueryFirstOrDefaultAsync<OrderRate>(query.ToString(), new { RES_ID = resid, ORDER_ID = orderid });



                        //Order Items
                        query = new StringBuilder();
                        query.AppendLine("SELECT a.ID,MI.Name_EN,MI.Name_TH,MI.Desc_EN,MI.Desc_TH,a.Amount,a.Price,MH.TaxType");
                        query.AppendLine(",a.Comment,MI.MenuGroup_ID");
                        query.AppendLine("FROM [OrderItem] a");
                        query.AppendLine("INNER JOIN MenuItem MI ON MI.ID = a.MenuItem_ID");
                        query.AppendLine("INNER JOIN MenuGroup MG ON MG.ID = MI.MenuGroup_ID");
                        query.AppendLine("INNER JOIN MenuHour MH ON MG.MenuHour_ID = MH.ID");
                        query.AppendLine("WHERE a.Order_ID = @ORDER");
                        var orderItems = await dbConnection.QueryAsync<ViewOrderItem>(query.ToString(), new { ORDER = result.ID });
                        result.Items = orderItems.ToList();
                        if (result.Items.Count > 0)
                        {
                            foreach (var itemAddOn in orderItems)
                            {
                                var AddOnOption = await dbConnection.QueryAsync<ViewAddOnOrderOption>(@"SELECT A.ID,B.AddOn_ID,A.AddOnOption_ID,A.OrderItem_ID,B.Name_EN,B.Name_TH,A.Price FROM [dbo].[OrderItemAddOn] as A  INNER JOIN [dbo].[AddOnOption] as B ON B.ID = A.AddOnOption_ID  WHERE  A.OrderItem_ID = @ORDER ",
                                    new
                                    {
                                        ORDER = itemAddOn.ID

                                    });
                                if (AddOnOption != null)
                                {
                                    itemAddOn.Options = AddOnOption.ToList();
                                }
                            }
                        }

                        result.OrderRef = await dbConnection.QueryFirstOrDefaultAsync<string>("SELECT OrderRef  FROM [OhoApp].[Delivery].[RateDetails] WHERE OrderNO = @OrderNO ", new { OrderNO = result.Order_No });
                        if (result.OrderRef != null)
                        {
                            result.DriverInfo = await dbConnection.QueryFirstOrDefaultAsync<DriverInfo>("SELECT [DriverID]  ,[DriverName]  ,[DriverPhone] ,[PlateNumber] ,[Photo] FROM [OhoApp].[Delivery].[DriverDetails] WHERE OrderRef = @ID", new { ID = result.OrderRef });
                        }

                    }
                    return result;
                }                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<List<ViewRestaurantrOrderDetail>> GetOrderByOrderNO(string OrderNO, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT A.ID,A.LogID,A.OrderID,A.Order_NO,A.Order_Type,A.RES_ID,A.SubTotal,A.SmallOrderFee,A.ServiceCharge,A.Commission,A.Discount,A.Total");
                    query.AppendLine(",A.StartTime,A.EndTime,A.PaymentMethod,A.PaymentStatus,A.Accept_Time,A.Cooked_Time,A.CookingTime,A.Distance,A.Delivery_Start,A.Delivery_End");
                    query.AppendLine(",A.Complete_Time,A.CreatedDate,A.RestaurantStatus as Status,B.Location as RestaurantLocation, F.Location as CustomerLocation,A.CUS_ID,C.Firstname +  '  '   + C.Lastname as CustomerName,C.PhoneNumber as CustomerPhoneNumber ");
                    query.AppendLine("FROM [Order] A INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("LEFT JOIN OrderAddress F ON A.ID = F.Order_ID");
                    query.AppendLine("WHERE A.RES_ID = @RES_ID and Order_NO LIKE '%" + OrderNO + "%'");
                    var result = await dbConnection.QueryAsync<ViewRestaurantrOrderDetail>(query.ToString(), new { RES_ID = resid });
                    if (result != null)
                        foreach (var item in result)
                        {
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.ID,MI.Name_EN,MI.Name_TH,MI.Desc_EN,MI.Desc_TH,a.Amount,a.Price,MH.TaxType");
                            query.AppendLine(",a.Comment,MI.MenuGroup_ID");
                            query.AppendLine("FROM [OrderItem] a");
                            query.AppendLine("INNER JOIN MenuItem MI ON MI.ID = a.MenuItem_ID");
                            query.AppendLine("INNER JOIN MenuGroup MG ON MG.ID = MI.MenuGroup_ID");
                            query.AppendLine("INNER JOIN MenuHour MH ON MG.MenuHour_ID = MH.ID");
                            query.AppendLine("WHERE a.Order_ID = @ORDER");
                            var orderItems = await dbConnection.QueryAsync<ViewOrderItem>(query.ToString(), new { ORDER = item.ID });
                            item.Items = orderItems.ToList();
                            if (item.Items.Count > 0)
                            {
                                foreach (var itemAddOn in orderItems)
                                {
                                    var AddOnOption = await dbConnection.QueryAsync<ViewAddOnOrderOption>(@"SELECT A.ID,B.AddOn_ID,A.AddOnOption_ID,A.OrderItem_ID,B.Name_EN,B.Name_TH,A.Price FROM [dbo].[OrderItemAddOn] as A  INNER JOIN [dbo].[AddOnOption] as B ON B.ID = A.AddOnOption_ID  WHERE  A.OrderItem_ID = @ORDER ",
                                        new
                                        {
                                            ORDER = itemAddOn.ID

                                        });
                                    if (AddOnOption != null)
                                    {
                                        itemAddOn.Options = AddOnOption.ToList();
                                    }
                                }
                            }
                            item.Distance = (item.CustomerLocation == null || item.RestaurantLocation == null ? 0m : (decimal)CalculateDistance(GetLocation(item.CustomerLocation), GetLocation(item.RestaurantLocation)));
                            item.OrderRef = await dbConnection.QueryFirstOrDefaultAsync<string>("SELECT OrderRef  FROM [OhoApp].[Delivery].[RateDetails] WHERE OrderNO = @OrderNO ", new { OrderNO = item.Order_No });

                            if (item.OrderRef != null)
                            {
                                item.DriverInfo = await dbConnection.QueryFirstOrDefaultAsync<DriverInfo>("SELECT [DriverID]  ,[DriverName]  ,[DriverPhone] ,[PlateNumber] ,[Photo] FROM [OhoApp].[Delivery].[DriverDetails] WHERE OrderRef = @ID", new { ID = item.OrderRef });
                            }
                            item.OrderAddress = await dbConnection.QueryFirstOrDefaultAsync<OrderAddress>("SELECT * FROM OrderAddress where Order_ID = @ORDER_ID", new { ORDER_ID = item.ID });
                            item.Distance = (item.OrderAddress == null || item.RestaurantLocation == null ? 0m : (decimal)CalculateDistance(GetLocation(item.OrderAddress.Location), GetLocation(item.RestaurantLocation)));
                        }
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutAcceptOrder(AcceptOrder data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int statusOrder = (int)OrderStatus.Cooking;
                    int status = 0 ;
                    var query = new StringBuilder();
                    query.AppendLine("SELECT A.ID,A.LogID,A.OrderID,A.Order_NO,A.Order_Type,A.RES_ID,A.SubTotal,A.SmallOrderFee,A.ServiceCharge,A.Commission,A.Discount,A.Total");
                    query.AppendLine(",A.StartTime,A.EndTime,A.PaymentMethod,A.PaymentStatus,A.Accept_Time,A.Cooked_Time,A.CookingTime,A.Distance,A.Delivery_Start,A.Delivery_End");
                    query.AppendLine(",A.Complete_Time,A.RestaurantStatus as Status,B.Location as RestaurantLocation, C.Location as CustomerLocation,C.Firstname +  '  '   + C.Lastname as CustomerName ");
                    query.AppendLine("FROM [Order] A INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("where A.RES_ID = @RES_ID and A.ID = @ORDER_ID");
                    var order = await dbConnection.QueryFirstOrDefaultAsync<ViewRestaurantrOrderDetail>(query.ToString(), new { RES_ID = resid, ORDER_ID = data.Order_ID });
                    switch (order.Order_Type)
                    {
                        case OrderType_Customer.Delivery:
                            statusOrder = (int)OrderStatus.Cooking;
                            break;
                        case OrderType_Customer.PickUp:
                            statusOrder = (int)OrderStatus.Cooking;
                            break;
                        case OrderType_Customer.DineIn:
                            statusOrder = (int)OrderStatus.Cooking;
                            break;
                    }
                    if (order.Order_Type == OrderType_Customer.DineIn)
                    {
                        query = new StringBuilder();
                        query.AppendLine("UPDATE [Order] SET  RestaurantStatus = @STATUS,Accept_Time = GETDATE(), Cooked_Time = GETDATE()  WHERE ID = @ORDER_ID AND RES_ID = @RES_ID");
                        status = await dbConnection.ExecuteAsync(query.ToString(), new { STATUS = statusOrder, ORDER_ID = data.Order_ID, RES_ID = resid });
                        if (status == 0)
                            return false;
                    }
                    else if (order.Order_Type == OrderType_Customer.PickUp && order.PaymentStatus == PaymentStatus.Paid)
                    {
                        query = new StringBuilder();
                        query.AppendLine("UPDATE [Order] SET  RestaurantStatus = @STATUS,Accept_Time = GETDATE(), Cooked_Time = GETDATE()  WHERE ID = @ORDER_ID AND RES_ID = @RES_ID");
                        status = await dbConnection.ExecuteAsync(query.ToString(), new { STATUS = statusOrder, ORDER_ID = data.Order_ID, RES_ID = resid });
                        if (status == 0)
                            return false;
                    }
                    else if (order.Order_Type == OrderType_Customer.Delivery && order.PaymentStatus == PaymentStatus.Paid)
                    {
                        query = new StringBuilder();
                        query.AppendLine("UPDATE [Order] SET  RestaurantStatus = @STATUS,Accept_Time = GETDATE(), Cooked_Time = GETDATE()  WHERE ID = @ORDER_ID AND RES_ID = @RES_ID");
                        status = await dbConnection.ExecuteAsync(query.ToString(), new { STATUS = statusOrder, ORDER_ID = data.Order_ID, RES_ID = resid });
                        if (status == 0)
                            return false;
                    }

                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutCookingDone(CookingOrder data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int statusOrder = (int)OrderStatus.Ready;
                    var query = new StringBuilder();
                    query.AppendLine("SELECT A.ID,A.LogID,A.OrderID,A.Order_NO,A.Order_Type,A.RES_ID,A.SubTotal,A.SmallOrderFee,A.ServiceCharge,A.Commission,A.Discount,A.Total");
                    query.AppendLine(",A.StartTime,A.EndTime,A.PaymentMethod,A.PaymentStatus,A.Accept_Time,A.Cooked_Time,A.CookingTime,A.Distance,A.Delivery_Start,A.Delivery_End");
                    query.AppendLine(",A.Complete_Time,A.RestaurantStatus as Status,B.Location as RestaurantLocation, C.Location as CustomerLocation,C.Firstname +  '  '   + C.Lastname as CustomerName ");
                    query.AppendLine("FROM [Order] A INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("where A.RES_ID = @RES_ID and A.ID = @ORDER_ID");
                    var order = await dbConnection.QueryFirstOrDefaultAsync<ViewRestaurantrOrderDetail>(query.ToString(), new { RES_ID = resid, ORDER_ID = data.Order_ID });
                    if (order == null)
                        return false;

                    query = new StringBuilder();

                    if (order.Order_Type == OrderType_Customer.PickUp)
                    {
                        query.AppendLine("UPDATE [Order] SET RestaurantStatus = @STATUS,[Cooked_Time] = GETDATE()");
                        statusOrder = (int)OrderStatus.Ready;
                    }
                    else if (order.Order_Type == OrderType_Customer.DineIn)
                    {
                        query.AppendLine("UPDATE [Order] SET RestaurantStatus = @STATUS,Status = @STATUS ,[Cooked_Time] = GETDATE()");
                        statusOrder = (int)OrderStatus.Complete;
                    }
                    query = new StringBuilder();
                    query.AppendLine("UPDATE [Order] SET RestaurantStatus = @STATUS,[Cooked_Time] = GETDATE()");
                    query.AppendLine("WHERE ID = @ORDERID AND RES_ID = @RESID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { STATUS = statusOrder, ORDERID = data.Order_ID, RESID = resid });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<bool> PutTakeOutComplete(PickUpCompleteOrder data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int statusOrder = (int)OrderStatus.Complete;
                    var query = new StringBuilder();
                    query.AppendLine("SELECT A.ID,A.LogID,A.OrderID,A.Order_NO,A.Order_Type,A.RES_ID,A.SubTotal,A.SmallOrderFee,A.ServiceCharge,A.Commission,A.Discount,A.Total");
                    query.AppendLine(",A.StartTime,A.EndTime,A.PaymentMethod,A.PaymentStatus,A.Accept_Time,A.Cooked_Time,A.CookingTime,A.Distance,A.Delivery_Start,A.Delivery_End");
                    query.AppendLine(",A.Complete_Time,A.RestaurantStatus as Status,B.Location as RestaurantLocation, C.Location as CustomerLocation,C.Firstname +  '  '   + C.Lastname as CustomerName ");
                    query.AppendLine("FROM [Order] A INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("where A.RES_ID = @RES_ID and A.ID = @ORDER_ID");
                    var order = await dbConnection.QueryFirstOrDefaultAsync<OrderDetail>(query.ToString(), new { RES_ID = resid, ORDER_ID = data.Order_ID });
                    if (order == null)
                        return false;
                    query = new StringBuilder();
                    query.AppendLine("UPDATE [Order] SET RestaurantStatus = @STATUS, Status = @STATUS,[Complete_Time] = GETDATE()");
                    query.AppendLine("WHERE ID = @ORDERID AND RES_ID = @RESID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { STATUS = statusOrder, ORDERID = data.Order_ID, RESID = resid });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutCancelOrder(CancelOrder data, int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int statusOrder = (int)OrderStatus.RestaurantCancel;
                    int status = 0;
                    var query = new StringBuilder();
                    query.AppendLine("SELECT A.ID,A.LogID,A.OrderID,A.Order_NO,A.Order_Type,A.RES_ID,A.SubTotal,A.SmallOrderFee,A.ServiceCharge,A.Commission,A.Discount,A.Total");
                    query.AppendLine(",A.StartTime,A.EndTime,A.PaymentMethod,A.PaymentStatus,A.Accept_Time,A.Cooked_Time,A.CookingTime,A.Distance,A.Delivery_Start,A.Delivery_End");
                    query.AppendLine(",A.Complete_Time,A.RestaurantStatus as Status,B.Location as RestaurantLocation, C.Location as CustomerLocation,C.Firstname +  '  '   + C.Lastname as CustomerName ");
                    query.AppendLine("FROM [Order] A INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("where A.RES_ID = @RES_ID and A.ID = @ORDER_ID");
                    var order = await dbConnection.QueryFirstOrDefaultAsync<OrderDetail>(query.ToString(), new { RES_ID = resid, ORDER_ID = data.Order_ID });
                    if (order == null)
                    {
                        return false;
                    }
                    else
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT B.ID FROM [dbo].[Order] as A INNER JOIN [dbo].[Coupon_Save] as B ON B.Order_ID =  A.ID WHERE A.ID = @ID");
                        var CouponID = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new
                        {
                            ID = data.Order_ID
                        });
                        query = new StringBuilder();
                        query.AppendLine("UPDATE [dbo].[Coupon_Save] SET IsUse = 0 WHERE ID = @ID");
                        var updateStatusCoupon = await dbConnection.ExecuteAsync(query.ToString(), new { ID = CouponID });

                        query = new StringBuilder();
                        query.AppendLine("SELECT B.MenuItem_ID,B.Amount FROM [dbo].[Order] as A INNER JOIN [dbo].[OrderItem] as B ON B.Order_ID = A.ID WHERE B.Order_ID = @ID");
                        var menuItem = await dbConnection.QueryAsync<menuItemCancel>(query.ToString(), new { ID = data.Order_ID });
                        foreach (var item in menuItem)
                        {
                            query = new StringBuilder();
                            query.AppendLine("UPDATE [dbo].[MenuItem] SET Quota += @Quota  WHERE ID = @ID");
                            status = await dbConnection.ExecuteAsync(query.ToString(), new { Quota = item.Amount, ID = item.MenuItem_ID });
                        }
                    }
                    query = new StringBuilder();
                    query.AppendLine("UPDATE [Order] SET [Status] = @STATUS,RestaurantStatus = @STATUS,[Complete_Time] = GETDATE()");
                    query.AppendLine("WHERE ID = @ORDERID AND RES_ID = @RESID");
                    status = await dbConnection.ExecuteAsync(query.ToString(), new { STATUS = statusOrder, ORDERID = data.Order_ID, RESID = resid });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<List<ListReject>> GetListReject()
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    //Add Coupon
                    var query = new StringBuilder();
                    query.AppendLine("SELECT ID,Description FROM [dbo].[ListReject]");
                    var data = await dbConnection.QueryAsync<ListReject>(query.ToString(), new { });

                    if (data != null)
                    {
                        return data.ToList();
                    }
                    else
                    {
                        return data.ToList();
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> RejectOrder(RejectOrder data, int RES_ID)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    int status = 0;
                    query = new StringBuilder();
                    query.AppendLine("SELECT B.ID FROM [dbo].[Order] as A INNER JOIN [dbo].[Coupon_Save] as B ON B.Order_ID =  A.ID WHERE A.ID = @ID");
                    var CouponID = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new
                    {
                        ID = data.Order_ID
                    });
                    query = new StringBuilder();
                    query.AppendLine("UPDATE [dbo].[Coupon_Save] SET IsUse = 0 WHERE ID = @ID");
                    var updateStatusCoupon = await dbConnection.ExecuteAsync(query.ToString(), new { ID = CouponID });

                    query = new StringBuilder();
                    query.AppendLine("SELECT B.MenuItem_ID,B.Amount FROM [dbo].[Order] as A INNER JOIN [dbo].[OrderItem] as B ON B.Order_ID = A.ID WHERE B.Order_ID = @ID");
                    var menuItem = await dbConnection.QueryAsync<menuItemCancel>(query.ToString(), new { ID = data.Order_ID });
                    foreach (var item in menuItem)
                    {
                        status = await dbConnection.ExecuteAsync("UPDATE [dbo].[MenuItem] SET Quota += @Amount  WHERE ID = @ID", new { Amount = item.Amount, ID = item.MenuItem_ID });
                    }

                    query = new StringBuilder();
                    query.AppendLine("UPDATE [dbo].[Order] SET Status = 99, RestaurantStatus = 96 WHERE ID = @ORDER_ID AND RES_ID = @RES_ID");
                    status = await dbConnection.ExecuteAsync(query.ToString(), new { ORDER_ID = data.Order_ID, RES_ID = RES_ID });

                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[ListOrderReject] (Order_ID,RES_ID,Reject_ID) VALUES(@Order_ID,@RES_ID,@Reject_ID)");
                    status = await dbConnection.ExecuteAsync(query.ToString(), 
                        new 
                        {
                            Order_ID = data.Order_ID,
                            RES_ID = RES_ID,
                            Reject_ID = data.Reason_ID
                        });



                    if (status == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }



    }
}
