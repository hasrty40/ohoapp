﻿using Dapper;
using Microsoft.Extensions.Configuration;
using OhoApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhoApp.Services
{
    public interface ICustomerData
    {
        Task<AppLanguage> GetAppLanguage(int cusid);
        Task<bool> PutAppLanguage(AppLanguage data, int cusid);
        Task<CustomerInfo> GetLoginCustomer(LoginCustomer data);
        Task<bool> PostLogCustomerLogin(CustomerInfo data, string guid);
        Task<int> PostRegister(SignUpCustomer data);
        Task<CustomerGuestInfo> PostCheckGuild(CustomerGuest data);
        Task<CustomerNoti> GetCustomerNoti(string Device_ID, int cusid);
        Task<bool> PostCustomerNoti(CustomerNoti data, int cusid);
        Task<bool> PutCustomerNoti(CustomerNoti data, int cusid);
        Task<CustomerInfo> GetCustomerByPhoneOrEmail(string EmailOrPhone);
        Task<bool> OtpResetPw(OtpSMSCustomer data);
        Task<int> GetCheckOtpResetPw(CheckOtpResetPw data);
        Task<CustomerInfo> GetCustomerInfo(int cusid);
        Task<bool> PutCustomerInfo(CustomerInfo data, int cusid);
        Task<bool> PutCustomerPw(CustomerPassword data, int cusid);
        Task<bool> PutCustomerImage(string filename, int cusid);
        Task<List<AddressCustomer>> GetRecentAddress(int cusid);
        Task<AddressCustomer> GetRecentAddressByID(int ID, int cusid);
        Task<List<AddressCustomer>> GetAddressCustomer(int cusid);
        Task<AddressCustomer> GetAddressCustomerByID(int cusid, int id);
        Task<bool> PostCustomerAddress(AddressCustomer data, int cusid);
        Task<bool> PutCustomerAddress(AddressCustomer data, int cusid);
        Task<bool> DeleteCustomerAddress(int Address_ID, int cusid);
        Task<bool> PutCustomerLocation(CustomerGps data, int cusid);
        Task<List<Cuisine>> GetAllCuisine();
        Task<List<RestaurantData>> GetAllRestaurant(OrderType Type, string Name, string StartTime, string EndTime,string Day, int cusid,int CuisineType);
        Task<List<RestaurantData>> GetAllRestaurant2(OrderType Type, string Name, string StartTime, string EndTime, string Day, int cusid, int CuisineType);
        Task<bool> PutFavoriteRestaurant(FavoriteRestauarnt data, int cusid);
        Task<RestaurantData> GetRestaurantByID(int res_id, string day,int cusid);
        Task<List<ViewMenuGroup>> GetMenuCategoryByResID(int res_id);
        Task<ViewMenuItem> GetMenuItemByID(int MenuItem_ID);
        Task<List<ViewMenuItem>> GetMenuItemByResID(int res_id);
        Task<int> PostOrder(CreateOrder data, int cusid);
        Task<ViewCustomerOrderDetail> GetOrderByID(int orderid, int resid);
        Task<List<ViewCustomerOrderDetail>> GetOrderByOrderNO(string OrderNO, int cusid);
        Task<List<ViewCustomerOrderDetail>> GetCurrentOrder(int cusid);
        Task<List<ViewCustomerOrderDetail>> GetHistoryOrder(int cusid);
        Task<List<CouponDetail>> GetAllCoupon(int cusid);
        Task<CouponDetail> GetCouponDetail(Coupon data, int cusid);
        Task<int> SaveCoupon(SaveCoupon data, int cusid);
        Task<bool> CheckCouponForRestaurant(CheckCoupon data);
        Task<List<ReasonReport>> GetReasonReport();
        Task<int> PostReport(ReportOrder data, int cusid);
        Task<bool> PostImageReport(string filename, int id,int orderid);
        Task<bool> PostOrderRate(OrderRate data, int cusid);
        Task<bool> ChangeStatusDeliveryComplete(ChangeStatusDeliveryToComplete data, int cusid);
        Task<bool> PutCancelOrder(int Order_ID,int cusid);
        Task<bool> ChangeStatusOrder(ChangeOrderStatus data, int cusid);
        Task<List<OrderTypeDeliery>> GetOrderTypeDelivery();
        Task<bool> UpdateStatusOrderTypeDelivery(string OrderNO, int OrderStatus);

    }


    public class CustomerRepository : ICustomerData
    {
        #region Services


        private readonly IConfiguration configuration;

        public CustomerRepository(IConfiguration config)
        {
            this.configuration = config;
        }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(ConfigurationExtensions.GetConnectionString(this.configuration, "DefaultConnection"));
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public Location GetLocation(string Location)
        {
            var coordArrey = Location.Split(',');
            var location = new Location();
            location.Latitude = double.Parse(coordArrey[0]);
            location.Longitude = double.Parse(coordArrey[1]);
            return location;
        }

        public double CalculateDistance(Location point1, Location point2)
        {
            var d1 = point1.Latitude * (Math.PI / 180.0);
            var num1 = point1.Longitude * (Math.PI / 180.0);
            var d2 = point2.Latitude * (Math.PI / 180.0);
            var num2 = point2.Longitude * (Math.PI / 180.0) - num1;
            var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) +
                     Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);
            return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
        }

        public static int GetIso8601WeekOfYear(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        #endregion

        public async Task<AppLanguage> GetAppLanguage(int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = await dbConnection.QueryFirstOrDefaultAsync<AppLanguage>("SELECT [AppLanguage] as 'Language' FROM [CUSTOMER]WHERE CUS_ID=@CUSID", new { CUSID = cusid });
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutAppLanguage(AppLanguage data, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("UPDATE [CUSTOMER] SET [AppLanguage]=@LANGUAGE WHERE CUS_ID=@CUSID",
                                                                new { LANGUAGE = (int)data.Language, CUSID = cusid, });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<CustomerInfo> GetLoginCustomer(LoginCustomer data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    string email = "";
                    string phone = "";
                    if (IsValidEmail(data.Username) == true)
                    {
                        email = data.Username;
                        phone = null;
                    }
                    else
                    {
                        email = null;
                        phone = data.Username;
                    }
                    var res = await dbConnection.QueryFirstOrDefaultAsync<CustomerInfo>("SELECT * FROM [dbo].[CUSTOMER] WHERE (Email=@EMAIL OR PhoneNumber=@PHONENUMBER) AND Password= @PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS AND IsEnable = 1", new { EMAIL = email, PHONENUMBER = phone, PASSWORD = data.Password });
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PostLogCustomerLogin(CustomerInfo data, string guid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("INSERT INTO dbo.Log_CUSTOMER_Login (CUS_ID,Guid) VALUES ");
                    query.AppendLine("(@CUSID,@GUID)");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { CUSID = data.CUS_ID, GUID = guid });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostRegister(SignUpCustomer data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkName = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT CUS_ID FROM [dbo].[CUSTOMER] WHERE Email=@EMAIL OR PhoneNumber=@PHONENUMBER and Email != '' and PhoneNumber != ''", new { EMAIL = data.Email, PHONENUMBER = data.PhoneNumber });
                    if (checkName > 0)
                        return 0;
                    else if (data.Password.Length < 8)
                    {
                        return 2;
                    }
                    else
                    {
                        int status = await dbConnection.ExecuteAsync("INSERT INTO dbo.CUSTOMER (Firstname,Lastname,Email,PhoneNumber,Password,Status) VALUES " +
                        "(@FirstName,@LastName,@EMAIL,@PHONENUMBER,@PASSWORD,@STATUS)",
                        new { FirstName = data.Firstname, LastName = data.Lastname, EMAIL = data.Email, PHONENUMBER = data.PhoneNumber, PASSWORD = data.Password, STATUS = 1 });
                        if (status == 1)
                            return 1;
                        else
                            return 0;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<CustomerGuestInfo> PostCheckGuild(CustomerGuest data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    CustomerGuestInfo checkId = new CustomerGuestInfo();
                    checkId = await dbConnection.QueryFirstOrDefaultAsync<CustomerGuestInfo>("SELECT * FROM [dbo].[CUSTOMER] WHERE Token=@GUID", new { GUID = data.Guid });
                    if (checkId != null)
                    {
                        return checkId;
                    }
                    else
                    {
                        var query = new StringBuilder();
                        query.AppendLine("INSERT INTO dbo.CUSTOMER (Firstname,Lastname,Status,Token) VALUES ");
                        query.AppendLine("('Guest',' ',1,@TOKEN)");
                        int status = await dbConnection.ExecuteAsync(query.ToString(), new { TOKEN = data.Guid });
                        if (status == 1)
                        {
                            checkId = await dbConnection.QueryFirstOrDefaultAsync<CustomerGuestInfo>("SELECT * FROM [dbo].[CUSTOMER] WHERE Token=@GUID", new { GUID = data.Guid });
                            return checkId;
                        }
                        else
                        {
                            return checkId;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<CustomerNoti> GetCustomerNoti(string Device_ID, int cusid)
        {
            CustomerNoti resdata = new CustomerNoti();
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT a.[Device_ID],b.[CUS_ID],b.[NotiIsEnable] as [IsEnable] FROM [CustomerNotiDevice] a ");
                    query.AppendLine("left join [CUSTOMER] b on a.CUS_ID = b.CUS_ID");
                    query.AppendLine("WHERE a.[Device_ID] = @DEVICE");
                    resdata = await dbConnection.QueryFirstOrDefaultAsync<CustomerNoti>(query.ToString(), new { DEVICE = Device_ID, CUS_ID = cusid });
                    return resdata;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PostCustomerNoti(CustomerNoti data, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[CustomerNotiDevice] ([Device_ID],[CUS_ID],[IsEnable]) VALUES (@DEVICE,@CUS_ID,@ISENABLE)");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { DEVICE = data.Device_ID, CUS_ID = cusid, ISENABLE = data.IsEnable });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutCustomerNoti(CustomerNoti data, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    if (data.IsEnable == false)
                        query.AppendLine("DELETE [dbo].[CustomerNotiDevice] WHERE [Device_ID] = @DEVICE AND [CUS_ID] = @CUS_ID;");
                    query.AppendLine("UPDATE [dbo].[CUSTOMER] SET [NotiIsEnable] = @STATUS WHERE [CUS_ID] = @CUS_ID;");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { DEVICE = data.Device_ID, CUS_ID = cusid, STATUS = data.IsEnable });
                    if (status >= 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<CustomerInfo> GetCustomerByPhoneOrEmail(string EmailOrPhone)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    string email = "";
                    string phone = "";
                    if (IsValidEmail(EmailOrPhone) == true)
                    {
                        email = EmailOrPhone;
                        phone = null;
                    }
                    else
                    {
                        email = null;
                        phone = EmailOrPhone;
                    }
                    var results = await dbConnection.QueryFirstOrDefaultAsync<CustomerInfo>("SELECT * FROM [dbo].[CUSTOMER] WHERE Email=@EMAIL OR PhoneNumber=@PHONE", new { EMAIL = email, PHONE = phone });
                    return results;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> OtpResetPw(OtpSMSCustomer data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("INSERT INTO dbo.OTP_CustomerResetPw (Reference_Key,OTP,CUS_ID,ExpireDate) VALUES (@Reference_Key,@OTP,@CUS_ID,@ExpireDate)",
                    new { Reference_Key = data.Reference_Key, OTP = data.OTP, CUS_ID = data.CUS_ID, ExpireDate = data.ExpireDate });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> GetCheckOtpResetPw(CheckOtpResetPw data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    string email = "";
                    string phone = "";
                    if (IsValidEmail(data.EmailOrPhone) == true)
                    {
                        email = data.EmailOrPhone;
                        phone = null;
                    }
                    else
                    {
                        email = null;
                        phone = data.EmailOrPhone;
                    }
                    var customer = await dbConnection.QueryFirstOrDefaultAsync<CustomerInfo>("SELECT * FROM [dbo].[CUSTOMER] WHERE Email=@EMAIL OR PhoneNumber=@PHONE", new { EMAIL = email, PHONE = phone });
                    if (customer == null)
                        return 0;
                    OtpSMSRestaurant obj = await dbConnection.QueryFirstOrDefaultAsync<OtpSMSRestaurant>("SELECT * FROM OTP_CustomerResetPw WHERE [Reference_Key] = @Reference_Key AND [OTP] = @OTP AND [CUS_ID] = @CUS_ID ",
                    new { Reference_Key = data.Reference_Key, OTP = data.OTP, CUS_ID = customer.CUS_ID });
                    if (obj != null)
                    {
                        if (obj.ExpireDate >= DateTime.UtcNow.AddHours(9))
                        {
                            int status = await dbConnection.ExecuteAsync("UPDATE dbo.CUSTOMER SET Password=@PASSWORD WHERE CUS_ID=@CUSID", new { PASSWORD = data.NewPassword, CUSID = customer.CUS_ID, });
                            if (status == 1)
                                return 2;
                            else
                                return 0;
                        }
                        else
                            return 1;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<CustomerInfo> GetCustomerInfo(int cusid)
        {
            CustomerInfo resdata = new CustomerInfo();
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [dbo].[CUSTOMER] WHERE CUS_ID = @CUS_ID");
                    resdata = await dbConnection.QueryFirstOrDefaultAsync<CustomerInfo>(query.ToString(), new { CUS_ID = cusid });
                    return resdata;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutCustomerInfo(CustomerInfo data, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [dbo].[CUSTOMER] WHERE CUS_ID = @CUS_ID");
                    var Customer = await dbConnection.QueryFirstOrDefaultAsync<CustomerInfo>(query.ToString(), new { CUS_ID = cusid });
                    data.Firstname ??= Customer.Firstname;
                    data.Lastname ??= Customer.Lastname;
                    data.Email ??= Customer.Email;
                    data.Location ??= Customer.Location;
                    data.PhoneNumber ??= Customer.PhoneNumber;
                    data.Password ??= Customer.Password;
                    query.AppendLine("UPDATE CUSTOMER SET [Firstname] = @Firstname, [Lastname] = @Lastname, [Email] = @Email, [PhoneNumber] = @PhoneNumber,Location = @Location, Password = @Password WHERE  [CUS_ID] = @CUS_ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { CUS_ID = cusid, Firstname = data.Firstname, Lastname = data.Lastname, Email = data.Email, PhoneNumber = data.PhoneNumber,Location = data.Location, Password = data.Password });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutCustomerPw(CustomerPassword data, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkPassword = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT CUS_ID FROM [dbo].[CUSTOMER] WHERE CUS_ID=@CUS_ID and Password = @Password", new { CUS_ID = cusid, Password = data.OldPassword });
                    if (checkPassword == 0)
                        return false;
                    else
                    {
                        var query = new StringBuilder();
                        query.AppendLine("UPDATE CUSTOMER SET [Password] = @Password WHERE  [CUS_ID] = @CUS_ID");
                        int status = await dbConnection.ExecuteAsync(query.ToString(), new { CUS_ID = cusid, Password = data.NewPassword });
                        if (status == 1)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<AddressCustomer>> GetRecentAddress (int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 3 A.* FROM [dbo].[OrderAddress] as A");
                    query.AppendLine("INNER JOIN [dbo].[Order] as B ON  B.ID = A.Order_ID");
                    query.AppendLine("WHERE  B.CUS_ID = @CUS_ID AND B.Order_Type = 3 ORDER BY A.ID DESC");
                    var result = await dbConnection.QueryAsync<AddressCustomer>(query.ToString(), new { CUS_ID = cusid });
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<AddressCustomer> GetRecentAddressByID(int ID,int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT A.* FROM [dbo].[OrderAddress] as A");
                    query.AppendLine("INNER JOIN [dbo].[Order] as B ON  B.ID = A.Order_ID");
                    query.AppendLine("WHERE  B.CUS_ID = @CUS_ID AND A.ID = @ID ");
                    var result = await dbConnection.QueryFirstOrDefaultAsync<AddressCustomer>(query.ToString(), new { CUS_ID = cusid,ID = ID });
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }





        public async Task<List<AddressCustomer>> GetAddressCustomer(int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT *  FROM [dbo].[CustomerAddress] WHERE CUS_ID = @CUS_ID");
                    var result = await dbConnection.QueryAsync<AddressCustomer>(query.ToString(), new { CUS_ID = cusid });
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<AddressCustomer> GetAddressCustomerByID(int cusid,int id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT *  FROM [dbo].[CustomerAddress] WHERE CUS_ID = @CUS_ID AND ID = @ID");
                    var result = await dbConnection.QueryFirstOrDefaultAsync<AddressCustomer>(query.ToString(), new { CUS_ID = cusid, ID = id });
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutCustomerImage(string filename, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("UPDATE CUSTOMER SET [Image] = @FILENAME WHERE  [CUS_ID] = @CUS_ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { CUS_ID = cusid, FILENAME = filename });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PostCustomerAddress(AddressCustomer data, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    if (data.DefaultSelection == true)
                        await dbConnection.ExecuteAsync("UPDATE CustomerAddress SET DefaultSelection = 0 WHERE CUS_ID = @CUS_ID", new { CUS_ID = cusid });
                    var query = new StringBuilder();
                    query.AppendLine("  INSERT INTO [dbo].[CustomerAddress](Name,Location,Address,Address_Detail,DefaultSelection,Comment,CUS_ID) VALUES(@Name,@Location,@Address,@Address_Detail,@DefaultSelection,@Comment,@CUS_ID)");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), 
                        new 
                        {
                            Name = data.Name,
                            Location = data.Location,
                            Address = data.Address,
                            Address_Detail = data.Address_Detail,
                            DefaultSelection =  data.DefaultSelection,
                            Comment = data.Comment,
                            CUS_ID = cusid
                        });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutCustomerAddress(AddressCustomer data, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    if (data.DefaultSelection == true)
                        await dbConnection.ExecuteAsync("UPDATE CustomerAddress SET DefaultSelection = 0 WHERE CUS_ID = @CUS_ID", new { CUS_ID = cusid });
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [dbo].[CustomerAddress] WHERE ID = @ID AND CUS_ID = @CUS_ID");
                    var CustomerAddress = await dbConnection.QueryFirstOrDefaultAsync<AddressCustomer>(query.ToString(), new { CUS_ID = cusid, ID = data.ID });
                    data.Name ??= CustomerAddress.Name;
                    data.Location ??= CustomerAddress.Location;
                    data.Address ??= CustomerAddress.Address;
                    data.Address_Detail ??= CustomerAddress.Address_Detail;
                    data.Comment ??= CustomerAddress.Comment;
                    query = new StringBuilder();
                    query.AppendLine("UPDATE [dbo].[CustomerAddress] SET Name = @Name,Location = @Location,Address = @Address, Address_Detail = @Address_Detail, DefaultSelection = @DefaultSelection,Comment = @Comment Where ID = @ID and CUS_ID = @CUS_ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { ID = data.ID, CUS_ID = cusid, Name = data.Name, Location = data.Location, Address = data.Address, Address_Detail = data.Address_Detail, DefaultSelection = data.DefaultSelection, Comment = data.Comment });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> DeleteCustomerAddress(int Address_ID, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT [ID] FROM [CustomerAddress]");
                    query.AppendLine("WHERE [CUS_ID] = @CUSID and [ID] = @ID");
                    var address = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { ID = Address_ID, CUSID = cusid });
                    if (address > 0)
                    {
                        query = new StringBuilder();
                        query.AppendLine("DELETE [CustomerAddress] WHERE [ID] = @ID");
                        int status = await dbConnection.ExecuteAsync(query.ToString(), new { ID = address });
                        if (status == 1)
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PutCustomerLocation(CustomerGps data, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("UPDATE CUSTOMER SET [Location] = @Location, LocationName = @LocationName,LocationAddress = @LocationAddress,LocationDetail = @LocationDetail,LocationNote = @LocationNote WHERE  [CUS_ID] = @CUS_ID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { CUS_ID = cusid, Location = data.GPS, LocationName = data.LocationName, LocationAddress = data.LocationAddress, LocationDetail = data.LocationDetail, LocationNote = data.LocationNote });
                    if (status == 1)
                        return true;
                    else
                        return false;

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<Cuisine>> GetAllCuisine()
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var results = await dbConnection.QueryAsync<Cuisine>("SELECT * FROM [dbo].[Cuisine] WHERE IsEnable = 1");
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RestaurantData>> GetAllRestaurant(OrderType Type,string Name,string StartTime,string EndTime,string Day,int cusid,int CuisineType)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    int number = 0;

                    #region Check Day


                    if (Day == "Monday")
                    {
                        number = 1;
                    }
                    else if (Day == "Tuesday")
                    {
                        number = 2;
                    }
                    else if (Day == "Wednesday")
                    {
                        number = 3;
                    }
                    else if (Day == "Thursday")
                    {
                        number = 4;
                    }
                    else if (Day == "Friday")
                    {
                        number = 5;
                    }
                    else if (Day == "Saturday")
                    {
                        number = 6;
                    }
                    else if (Day == "Sunday")
                    {
                        number = 7;
                    }

                    #endregion

                    query.AppendLine("SELECT Distinct A.RES_ID,A.* ,B.IsEveryDay  FROM [dbo].[RESTAURANT] as A ");
                    query.AppendLine("INNER JOIN [dbo].[OpeningHour] as B ON B.RES_ID = A.RES_ID");

                    var checkNameMenu = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT ID FROM [dbo].[MenuItem] WHERE Name_EN LIKE '%" + Name + "%' OR Name_TH LIKE '%" + Name + "%'", new { });

                    if (checkNameMenu > 0)
                    {
                        query.AppendLine("INNER JOIN ( SELECT MH.RES_ID FROM  [dbo].[MenuHour] as MH  INNER JOIN [dbo].[MenuGroup] as MG ON MG.MenuHour_ID = MH.ID INNER JOIN [dbo].[MenuItem] as MI ON MI.MenuGroup_ID = MG.ID WHERE MI.Name_EN LIKE '%" + Name + "%' OR MI.Name_TH LIKE '%" + Name + "%') as C ON C.RES_ID = B.RES_ID ");
                    }


                    query.AppendLine("WHERE A.AlternativeVerify = 1 AND A.OTP_validate = 1 AND A.IsApproval = 1 AND A.IsEnable = 1");


                    if (Name != null)
                    {
                        var checkName = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT RES_ID FROM [dbo].[RESTAURANT] WHERE RestaurantNameTH LIKE '%" + Name + "%' or RestaurantNameEN LIKE '%" + Name + "%'", new { });
                        if (checkName > 0)
                        {
                            query.AppendLine("AND (A.RestaurantNameTH LIKE '%" + Name + "%' or A.RestaurantNameEN LIKE '%" + Name + "%')");
                        }
                    }


                    if (StartTime != null && EndTime != null)
                    {
                        switch (number)
                        {
                            case 1:
                                query.AppendLine("AND CONVERT(TIME,@StartTime) between [StartTime]  AND [EndTime]");
                                query.AppendLine("OR CONVERT(TIME,@EndTime) between [StartTime]  AND [EndTime]");
                                break;
                            case 2:
                                query.AppendLine("AND CONVERT(TIME,@StartTime) between [StartTime2]  AND [EndTime2]");
                                query.AppendLine("OR CONVERT(TIME,@EndTime) between [StartTime2]  AND [EndTime2]");

                                break;
                            case 3:
                                query.AppendLine("AND CONVERT(TIME,@StartTime) between [StartTime3]  AND [EndTime3]");
                                query.AppendLine("OR CONVERT(TIME,@EndTime) between [StartTime3]  AND [EndTime3]");

                                break;
                            case 4:
                                query.AppendLine("AND CONVERT(TIME,@StartTime) between [StartTime4]  AND [EndTime4]");
                                query.AppendLine("OR CONVERT(TIME,@EndTime) between [StartTime4]  AND [EndTime4]");

                                break;
                            case 5:
                                query.AppendLine("AND CONVERT(TIME,@StartTime) between [StartTime5]  AND [EndTime5]");
                                query.AppendLine("OR CONVERT(TIME,@EndTime) between [StartTime5]  AND [EndTime5]");

                                break;
                            case 6:
                                query.AppendLine("AND CONVERT(TIME,@StartTime) between [StartTime6]  AND [EndTime6]");
                                query.AppendLine("OR CONVERT(TIME,@EndTime) between [StartTime6]  AND [EndTime6]");

                                break;
                            case 7:
                                query.AppendLine("AND CONVERT(TIME,@StartTime) between [StartTime7]  AND [EndTime7]");
                                query.AppendLine("OR CONVERT(TIME,@EndTime) between [StartTime7]  AND [EndTime7]");

                                break;
                            default:
                                break;
                        }
                    }

                    query.AppendLine("AND (select value from fn_split_string_to_column(B.DayOfWeek,',') a where column_id in (" + number + ")) = 1 OR B.IsEveryday = 1");



                    switch (Type)
                    {
                        case OrderType.PickUp_DineIn:
                            switch (number)
                            {
                                case 1:
                                    query.AppendLine("AND( (select value from fn_split_string_to_column(B.Service,',') a where column_id in (1)) = 1");
                                    query.AppendLine("OR (select value from fn_split_string_to_column(B.Service,',') a where column_id in (2)) = 1 )");
                                    break;
                                case 2:
                                    query.AppendLine("AND( (select value from fn_split_string_to_column(B.Service2,',') a where column_id in (1)) = 1");
                                    query.AppendLine("OR (select value from fn_split_string_to_column(B.Service2,',') a where column_id in (2)) = 1)");
                                    break;
                                case 3:
                                    query.AppendLine("AND( (select value from fn_split_string_to_column(B.Service3,',') a where column_id in (1)) = 1");
                                    query.AppendLine("OR (select value from fn_split_string_to_column(B.Service3,',') a where column_id in (2)) = 1)");
                                    break;
                                case 4:
                                    query.AppendLine("AND( (select value from fn_split_string_to_column(B.Service4,',') a where column_id in (1)) = 1");
                                    query.AppendLine("OR (select value from fn_split_string_to_column(B.Service4,',') a where column_id in (2)) = 1)");
                                    break;
                                case 5:
                                    query.AppendLine("AND( (select value from fn_split_string_to_column(B.Service5,',') a where column_id in (1)) = 1");
                                    query.AppendLine("OR (select value from fn_split_string_to_column(B.Service5,',') a where column_id in (2)) = 1)");
                                    break;
                                case 6:
                                    query.AppendLine("AND( (select value from fn_split_string_to_column(B.Service6,',') a where column_id in (1)) = 1");
                                    query.AppendLine("OR (select value from fn_split_string_to_column(B.Service6,',') a where column_id in (2)) = 1)");
                                    break;
                                case 7:
                                    query.AppendLine("AND( (select value from fn_split_string_to_column(B.Service7,',') a where column_id in (1)) = 1");
                                    query.AppendLine("OR (select value from fn_split_string_to_column(B.Service7,',') a where column_id in (2)) = 1)");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case OrderType.Delivery:
                            switch (number)
                            {
                                case 1:
                                    query.AppendLine("AND (select value from fn_split_string_to_column(B.Service,',') a where column_id in (3)) = 1");
                                    break;
                                case 2:
                                    query.AppendLine("AND (select value from fn_split_string_to_column(B.Service2,',') a where column_id in (3)) = 1");
                                    break;
                                case 3:
                                    query.AppendLine("AND (select value from fn_split_string_to_column(B.Service3,',') a where column_id in (3)) = 1");
                                    break;
                                case 4:
                                    query.AppendLine("AND (select value from fn_split_string_to_column(B.Service4,',') a where column_id in (3)) = 1");
                                    break;
                                case 5:
                                    query.AppendLine("AND (select value from fn_split_string_to_column(B.Service5,',') a where column_id in (3)) = 1");
                                    break;
                                case 6:
                                    query.AppendLine("AND (select value from fn_split_string_to_column(B.Service6,',') a where column_id in (3)) = 1");
                                    break;
                                case 7:
                                    query.AppendLine("AND (select value from fn_split_string_to_column(B.Service7,',') a where column_id in (3)) = 1");
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    if (CuisineType > 0)
                    {
                        query.AppendLine("AND Cuisine_ID IN ("+ CuisineType +") ");
                    }


                    var result = await dbConnection.QueryAsync<RestaurantData>(query.ToString(), new { StartTime = StartTime , EndTime = EndTime, Cuisine_ID = CuisineType });
                    var allCuisine = await dbConnection.QueryAsync<Cuisine>("SELECT * FROM [dbo].[Cuisine]", new { });
                    query = new StringBuilder();
                    query.AppendLine("SELECT [Firstname],[Lastname],[Email],[PhoneNumber],[Password],[Location] FROM [dbo].[CUSTOMER] WHERE CUS_ID = @CUS_ID");
                    var customer = await dbConnection.QueryFirstOrDefaultAsync<CustomerInfo>(query.ToString(), new { CUS_ID = cusid });

                    foreach (var item in result)
                    {
                        var checkMenu = await dbConnection.QueryFirstOrDefaultAsync<int>(@"SELECT MI.ID FROM [dbo].[MenuItem] as MI 
                                                                                           INNER JOIN [dbo].[MenuGroup] as MG ON MG.ID = MI.MenuGroup_ID
                                                                                           INNER JOIN [dbo].[MenuHour] as MH ON MH.ID = MG.MenuHour_ID
                                                                                           WHERE MI.Quota > 0 AND MH.RES_ID = @RES_ID", 
                                                                                           new 
                                                                                           { 
                                                                                               RES_ID = item.RES_ID
                                                                                           });
                        if (checkMenu  == 0)
                        {
                            item.IsClosed = true;
                            item.HasMenu = false;

                        }
                        else
                        {
                            item.IsClosed = false;
                            item.HasMenu = true;
                        }

                        query = new StringBuilder();


                        if (item.IsEveryDay == 1)
                        {
                            query.AppendLine("SELECT convert(varchar,[StartTime8],8) as StartTime,convert(varchar,[EndTime8],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                        }
                        else
                        {
                            switch (number)
                            {
                                case 1:
                                    query.AppendLine("SELECT convert(varchar,[StartTime],8) as StartTime,convert(varchar,[EndTime],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                    break;
                                case 2:
                                    query.AppendLine("SELECT convert(varchar,[StartTime2],8) as StartTime,convert(varchar,[EndTime2],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                    break;
                                case 3:
                                    query.AppendLine("SELECT convert(varchar,[StartTime3],8) as StartTime,convert(varchar,[EndTime3],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                    break;
                                case 4:
                                    query.AppendLine("SELECT convert(varchar,[StartTime4],8) as StartTime,convert(varchar,[EndTime4],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                    break;
                                case 5:
                                    query.AppendLine("SELECT convert(varchar,[StartTime5],8) as StartTime,convert(varchar,[EndTime5],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                    break;
                                case 6:
                                    query.AppendLine("SELECT convert(varchar,[StartTime6],8) as StartTime,convert(varchar,[EndTime6],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                    break;
                                case 7:
                                    query.AppendLine("SELECT convert(varchar,[StartTime7],8) as StartTime,convert(varchar,[EndTime7],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                    break;
                                default:
                                    break;
                            }
                        }


                  






                        query = new StringBuilder();
                        query.AppendLine("SELECT MAX(A.Discount) as 'Discount',MIN(A.Price) as 'Price' FROM [dbo].[MenuItem] as A");
                        query.AppendLine("INNER JOIN [dbo].[MenuGroup] as B ON B.ID = A.MenuGroup_ID");
                        query.AppendLine("INNER JOIN [dbo].[MenuHour] as C ON C.ID = B.MenuHour_ID");
                        query.AppendLine("WHERE C.RES_ID = @RES_ID");


                        var _data = await dbConnection.QueryFirstOrDefaultAsync<PriceRestaurant>(query.ToString(), new { RES_ID = item.RES_ID });

                        if (_data != null)
                        {
                            item.Price = _data.Price;
                            item.Discount = _data.Discount;
                            if (item.Discount > 0)
                            {
                                item.Total = _data.Price - (_data.Price * _data.Discount / 100);
                            }
                            else
                            {
                                item.Total = item.Price;
                            }
                        }


                        query = new StringBuilder();
                        switch (number)
                        {
                            case 1:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");

                                break;
                            case 2:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service2,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service2,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service2,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID"); break;
                            case 3:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service3,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service3,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service3,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID"); break;
                            case 4:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service4,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service4,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service4,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID"); break;
                            case 5:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service5,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service5,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service5,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID"); break;
                            case 6:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service6,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service6,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service6,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID"); break;
                            case 7:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service7,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service7,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service7,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID"); break;
                            default:
                                break;
                        }
                        item.RestaurantService = await dbConnection.QueryFirstOrDefaultAsync<RestaurantService>(query.ToString(), new { RES_ID = item.RES_ID });                     
                        item.Distance = Math.Round((item.Location == null || customer.Location == null ? 0m : (decimal)CalculateDistance(GetLocation(customer.Location), GetLocation(item.Location))), 2); 
                        if (item.Cuisine_ID > 0)
                            item.Cuisine = allCuisine.Where(x => x.ID == item.Cuisine_ID).FirstOrDefault();
                        query = new StringBuilder();
                        query.AppendLine("SELECT IsFavorite  FROM [OhoApp].[dbo].[FavoriteRES] WHERE RES_ID = @RES_ID AND CUS_ID = @CUS_ID");
                        var checkFavorite = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { RES_ID = item.RES_ID, CUS_ID = cusid });
                        if (checkFavorite == 1)
                        {
                            item.IsFavorite = true;
                        }
                        else
                        {
                            item.IsFavorite = false;
                        }                    
                    }
                    if (Type == OrderType.Delivery)
                    {
                        result = result.Where(x => x.Distance <= (15 * 1000));
                    }
                    else if  (Type == OrderType.PickUp_DineIn)
                    {
                        result = result.Where(x => x.Distance <= (50 * 1000));
                    }
                    

                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RestaurantData>> GetAllRestaurant2(OrderType Type, string Name, string StartTime, string EndTime, string Day, int cusid, int CuisineType)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    int number = 0;

                    #region Check Day


                    if (Day == "Monday")
                    {
                        number = 1;
                    }
                    else if (Day == "Tuesday")
                    {
                        number = 2;
                    }
                    else if (Day == "Wednesday")
                    {
                        number = 3;
                    }
                    else if (Day == "Thursday")
                    {
                        number = 4;
                    }
                    else if (Day == "Friday")
                    {
                        number = 5;
                    }
                    else if (Day == "Saturday")
                    {
                        number = 6;
                    }
                    else if (Day == "Sunday")
                    {
                        number = 7;
                    }

                    #endregion

                    if (Name == null)
                    {
                        Name = "";
                    }

                    query.AppendLine("EXEC GetAllRestaurant @DAY = @Day, @NAME = @Name");

                    var result = await dbConnection.QueryAsync<RestaurantData>(query.ToString(), new { Name = Name, Day = number });

                    var allCuisine = await dbConnection.QueryAsync<Cuisine>("SELECT * FROM [dbo].[Cuisine]", new { });
                    query = new StringBuilder();
                    query.AppendLine("SELECT [Firstname],[Lastname],[Email],[PhoneNumber],[Password],[Location] FROM [dbo].[CUSTOMER] WHERE CUS_ID = @CUS_ID");
                    var customer = await dbConnection.QueryFirstOrDefaultAsync<CustomerInfo>(query.ToString(), new { CUS_ID = cusid });

                    foreach (var item in result)
                    {
                        if (item.IsEveryDay == 1)
                        {
                            number = 8;
                        }


                        #region CheckStatus 

                        query = new StringBuilder();
                        query.AppendLine("EXEC CheckStatus @RES_ID = @RES_ID,@DAY = @DAY");

                        var checkStatus = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { RES_ID = item.RES_ID, DAY = number });

                        var checkMenu = await dbConnection.QueryFirstOrDefaultAsync<int>(@"SELECT MI.ID FROM [dbo].[MenuItem] as MI 
                                                                                           INNER JOIN [dbo].[MenuGroup] as MG ON MG.ID = MI.MenuGroup_ID
                                                                                           INNER JOIN [dbo].[MenuHour] as MH ON MH.ID = MG.MenuHour_ID
                                                                                           WHERE MI.Quota > 0 AND MH.RES_ID = @RES_ID",
                                                                                           new
                                                                                           {
                                                                                               RES_ID = item.RES_ID
                                                                                           });
                        if (item.Status == 0)
                        {
                            item.Status = 0;
                        }
                        else if (checkStatus > 0 && checkMenu > 0 && item.Status == RestaurantStatusActive.Open)
                        {
                            item.HasMenu = true;
                            item.IsClosed = false;
                        }
                        else if(checkStatus == 0 || checkMenu == 0)
                        {
                            item.HasMenu = false ;
                            item.IsClosed = true;
                        }


                        #endregion

                        #region Discount

                        query = new StringBuilder();
                        query.AppendLine("SELECT MAX(A.Discount) as 'Discount',MIN(A.Price) as 'Price' FROM [dbo].[MenuItem] as A");
                        query.AppendLine("INNER JOIN [dbo].[MenuGroup] as B ON B.ID = A.MenuGroup_ID");
                        query.AppendLine("INNER JOIN [dbo].[MenuHour] as C ON C.ID = B.MenuHour_ID");
                        query.AppendLine("WHERE C.RES_ID = @RES_ID");


                        var _data = await dbConnection.QueryFirstOrDefaultAsync<PriceRestaurant>(query.ToString(), new { RES_ID = item.RES_ID });

                        if (_data != null)
                        {
                            item.Price = _data.Price;
                            item.Discount = _data.Discount;
                            if (item.Discount > 0)
                            {
                                item.Total = _data.Price - (_data.Price * _data.Discount / 100);
                            }
                            else
                            {
                                item.Total = item.Price;
                            }
                        }


                        #endregion


                        #region Service


                        query = new StringBuilder();
                        query.AppendLine("EXEC [dbo].[QueryServiceByDay] @DAY = @DAY,@RES_ID = @RES_ID");
                        item.RestaurantService = await dbConnection.QueryFirstOrDefaultAsync<RestaurantService>(query.ToString(), new { RES_ID = item.RES_ID, DAY = number });


                        #endregion



                        item.Distance = Math.Round((item.Location == null || customer.Location == null ? 0m : (decimal)CalculateDistance(GetLocation(customer.Location), GetLocation(item.Location))), 2);
                        if (item.Cuisine_ID > 0)
                            item.Cuisine = allCuisine.Where(x => x.ID == item.Cuisine_ID).FirstOrDefault();
                        query = new StringBuilder();
                        query.AppendLine("SELECT IsFavorite  FROM [OhoApp].[dbo].[FavoriteRES] WHERE RES_ID = @RES_ID AND CUS_ID = @CUS_ID");
                        var checkFavorite = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { RES_ID = item.RES_ID, CUS_ID = cusid });
                        if (checkFavorite == 1)
                        {
                            item.IsFavorite = true;
                        }
                        else
                        {
                            item.IsFavorite = false;
                        }

                        query = new StringBuilder();
                        if (StartTime != null && EndTime != null)
                        {
                            query.AppendLine("EXEC [dbo].[QueryRestaurantSearchTime] @RES_ID = @RESID ,@DAY = @Day,@STARTTIME =  @TimeStart,@ENDTIME = @TimeEnd");
                            var SearchTime = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { RESID = item.RES_ID, Day = number, TimeStart = StartTime, TimeEnd = EndTime });
                            if (SearchTime > 0)
                            {
                                item.HaveRestaurant = true;
                            }
                            else
                            {
                                item.HaveRestaurant = false;
                            }
                        }
                    }

                    if (Type == OrderType.Delivery)
                    {
                        result = result.Where(x => x.RestaurantService.IsDelivery == true);
                    }
                    else
                    {
                        result = result.Where(x => x.RestaurantService.IsDineIn == true || x.RestaurantService.IsPickup == true);
                    }


                    if (StartTime != null && EndTime != null)
                    {
                        result = result.Where(x => x.HaveRestaurant == true);
                    }

                    if (CuisineType > 0 )
                    {
                        result = result.Where(x => x.Cuisine_ID == CuisineType);
                    }



                    if (Type == OrderType.Delivery)
                    {
                        result = result.Where(x => x.Distance <= (15 * 1000));
                    }
                    else if (Type == OrderType.PickUp_DineIn)
                    {
                        result = result.Where(x => x.Distance <= (50 * 1000));
                    }
                    return result.ToList();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> PutFavoriteRestaurant(FavoriteRestauarnt data, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = 0;
                    bool IsFavorite = true;

                    int checkFavoriteRES = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT ID FROM [dbo].[FavoriteRES] WHERE RES_ID = @RES_ID AND CUS_ID = @CUS_ID", new { CUS_ID = cusid, RES_ID = data.RES_ID});
                    if (checkFavoriteRES == 0)
                    {
                        status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[FavoriteRES](RES_ID,CUS_ID,IsFavorite) VALUES(@RES_ID,@CUS_ID,1)", new { RES_ID = data.RES_ID, CUS_ID = cusid });
                        if (status == 1)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        var checkStatus = await dbConnection.QueryFirstOrDefaultAsync<bool>("SELECT IsFavorite FROM [dbo].[FavoriteRES] WHERE RES_ID = @RES_ID AND CUS_ID = @CUS_ID", new { CUS_ID = cusid, RES_ID = data.RES_ID });
                        if (checkStatus == true)
                        {
                            IsFavorite = false;
                        }
                        else
                        {
                            IsFavorite = true;
                        }
                        var query = new StringBuilder();
                        query.AppendLine("UPDATE [dbo].[FavoriteRES] SET IsFavorite = @IsFavorite WHERE CUS_ID = @CUS_ID AND RES_ID = @RES_ID");
                        status = await dbConnection.ExecuteAsync(query.ToString(), new { RES_ID = data.RES_ID, CUS_ID = cusid, IsFavorite = IsFavorite });
                        if (status == 1)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<RestaurantData> GetRestaurantByID(int res_id,string Day,int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    int number = 0;
                    query.AppendLine("SELECT * FROM [dbo].[RESTAURANT] WHERE RES_ID=@resid");
                    var results = await dbConnection.QueryFirstOrDefaultAsync<RestaurantData>(query.ToString(), new { resid = res_id });
                    if (results != null)
                    {
                        if (results.Cuisine_ID > 0)
                            results.Cuisine = await dbConnection.QueryFirstOrDefaultAsync<Cuisine>("SELECT * FROM [dbo].[Cuisine] WHERE ID=@id", new { id = results.Cuisine_ID });
                        query = new StringBuilder();

                        #region Check Day


                        if (Day == "Monday")
                        {
                            number = 1;
                        }
                        else if (Day == "Tuesday")
                        {
                            number = 2;
                        }
                        else if (Day == "Wednesday")
                        {
                            number = 3;
                        }
                        else if (Day == "Thursday")
                        {
                            number = 4;
                        }
                        else if (Day == "Friday")
                        {
                            number = 5;
                        }
                        else if (Day == "Saturday")
                        {
                            number = 6;
                        }
                        else if (Day == "Sunday")
                        {
                            number = 7;
                        }

                        #endregion

                        var allCuisine = await dbConnection.QueryAsync<Cuisine>("SELECT * FROM [dbo].[Cuisine]", new { });
                        query = new StringBuilder();
                        query.AppendLine("SELECT [Firstname],[Lastname],[Email],[PhoneNumber],[Password],[Location] FROM [dbo].[CUSTOMER] WHERE CUS_ID = @CUS_ID");
                        var customer = await dbConnection.QueryFirstOrDefaultAsync<CustomerInfo>(query.ToString(), new { CUS_ID = cusid });
                        query = new StringBuilder();
                        switch (number)
                        {
                            case 1:
                                query.AppendLine("SELECT convert(varchar,[StartTime],8) as StartTime,convert(varchar,[EndTime],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                break;
                            case 2:
                                query.AppendLine("SELECT convert(varchar,[StartTime2],8) as StartTime,convert(varchar,[EndTime2],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                break;
                            case 3:
                                query.AppendLine("SELECT convert(varchar,[StartTime3],8) as StartTime,convert(varchar,[EndTime3],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                break;
                            case 4:
                                query.AppendLine("SELECT convert(varchar,[StartTime4],8) as StartTime,convert(varchar,[EndTime4],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                break;
                            case 5:
                                query.AppendLine("SELECT convert(varchar,[StartTime5],8) as StartTime,convert(varchar,[EndTime5],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                break;
                            case 6:
                                query.AppendLine("SELECT convert(varchar,[StartTime6],8) as StartTime,convert(varchar,[EndTime6],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                break;
                            case 7:
                                query.AppendLine("SELECT convert(varchar,[StartTime7],8) as StartTime,convert(varchar,[EndTime7],8) as EndTime FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");
                                break;
                            default:
                                break;
                        }
                        var time = await dbConnection.QueryFirstOrDefaultAsync<OpeningHourRestaurant>(query.ToString(), new { RES_ID = res_id });
                        query = new StringBuilder();
                        query.AppendLine("SELECT MAX(A.Discount) as 'Discount',MIN(A.Price) as 'Price' FROM [dbo].[MenuItem] as A");
                        query.AppendLine("INNER JOIN [dbo].[MenuGroup] as B ON B.ID = A.MenuGroup_ID");
                        query.AppendLine("INNER JOIN [dbo].[MenuHour] as C ON C.ID = B.MenuHour_ID");
                        query.AppendLine("WHERE C.RES_ID = @RES_ID");


                        var _data = await dbConnection.QueryFirstOrDefaultAsync<PriceRestaurant>(query.ToString(), new { RES_ID = res_id });

                        if (_data != null)
                        {
                            results.Price = _data.Price;
                            results.Discount = _data.Discount;
                            if (results.Discount > 0)
                            {
                                results.Total = _data.Price - (_data.Price * _data.Discount / 100);
                            }
                        }


                        query = new StringBuilder();
                        switch (number)
                        {
                            case 1:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID");

                                break;
                            case 2:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service2,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service2,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service2,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID"); break;
                            case 3:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service3,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service3,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service3,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID"); break;
                            case 4:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service4,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service4,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service4,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID"); break;
                            case 5:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service5,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service5,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service5,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID"); break;
                            case 6:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service6,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service6,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service6,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID"); break;
                            case 7:
                                query.AppendLine("SELECT (select value from fn_split_string_to_column(Service7,',') a where column_id in (1)) as IsPickUp,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service7,',') a where column_id in (2)) as IsDineIn,");
                                query.AppendLine("(select value from fn_split_string_to_column(Service7,',') a where column_id in (3)) as IsDelivery");
                                query.AppendLine("FROM [dbo].[OpeningHour] WHERE RES_ID = @RES_ID"); break;
                            default:
                                break;
                        }
                        results.RestaurantService = await dbConnection.QueryFirstOrDefaultAsync<RestaurantService>(query.ToString(), new { RES_ID = res_id });
                        results.Distance = Math.Round((results.Location == null || customer.Location == null ? 0m : (decimal)CalculateDistance(GetLocation(customer.Location), GetLocation(results.Location))), 2);
                        if (results.Cuisine_ID > 0)
                            results.Cuisine = allCuisine.Where(x => x.ID == results.Cuisine_ID).FirstOrDefault();

                        query = new StringBuilder();
                        query.AppendLine("SELECT Value FROM [dbo].[ServiceCharge] WHERE ID = @ID");

                        results.ServiceCharge = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { ID = results.ServiceCharge });

                        query = new StringBuilder();
                        query.AppendLine("SELECT IsFavorite  FROM [OhoApp].[dbo].[FavoriteRES] WHERE RES_ID = @RES_ID AND CUS_ID = @CUS_ID");
                        var checkFavorite = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { RES_ID = res_id, CUS_ID = cusid });
                        if (checkFavorite == 1)
                        {
                            results.IsFavorite = true;
                        }
                        else
                        {
                            results.IsFavorite = false;
                        }
                    }
                    return results;                  
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewMenuGroup>> GetMenuCategoryByResID(int res_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int sequence = 1;
                    List<ViewMenuGroup> data = new List<ViewMenuGroup>();
                    var query = new StringBuilder();
                    //All
                    query.AppendLine("SELECT MG.* FROM [dbo].[MenuGroup] as MG INNER JOIN [dbo].[MenuHour] as MH ON MH.ID = MG.MenuHour_ID");
                    query.AppendLine("WHERE MH.RES_ID = @RES_ID AND MG.Sequence = 1 ORDER BY MG.ID ");
                    var all = await dbConnection.QueryFirstOrDefaultAsync<ViewMenuGroup>(query.ToString(), new { RES_ID = res_id });
                    if (all == null)
                    {
                        return data.ToList();
                    }
                    else
                    {
                        all.IsAll = true;
                        data.Add(all);
                        query = new StringBuilder();
                        query.AppendLine("SELECT DISTINCT MG.[ID],MG.[Name],MG.[MenuHour_ID],MG.[CreatedDate] FROM MenuItem as MI ");
                        query.AppendLine("INNER JOIN [dbo].[MenuGroup] MG ON MG.ID = MI.MenuGroup_ID");
                        query.AppendLine("INNER JOIN MenuHour MH on MG.MenuHour_ID = MH.ID ");
                        query.AppendLine("WHERE MH.RES_ID = @RES_ID  and MG.IsEnable = 1 AND MG.Sequence != 1  ORDER BY MG.ID");

                        var _menuGroup = await dbConnection.QueryAsync<ViewMenuGroup>(query.ToString(), new { RES_ID = res_id });

                        if (_menuGroup != null)
                        {
                            foreach (var item in _menuGroup)
                            {
                                sequence += 1;
                                item.Sequence = sequence;
                                data.Add(item);
                            }
                        }
                    }         


                    return data.OrderBy(x => x.Sequence).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewMenuItem>> GetMenuItemByResID(int res_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query = new StringBuilder();
                    query.AppendLine("select MI.*,MH.TaxType,MG.Name as MenuGroup_Name from MenuHour MH");
                    query.AppendLine("inner join MenuGroup MG on MG.MenuHour_ID = MH.ID");
                    query.AppendLine("inner join MenuItem MI on MI.MenuGroup_ID = MG.ID");
                    query.AppendLine("where RES_ID = @resid and MI.Status = 1 AND MI.IsEnable = 1");              
                    var results = await dbConnection.QueryAsync<ViewMenuItem>(query.ToString(), new { resid = res_id });
                    if (results.ToList().Count > 0)
                    {

                        foreach (var item in results)
                        {
                            query = new StringBuilder();
                            query.AppendLine("SELECT A.*  FROM [dbo].[AddOn] as A");
                            query.AppendLine("INNER JOIN [dbo].[MenuItemAddOn] as B");
                            query.AppendLine("ON B.AddOn_ID = A.ID");
                            query.AppendLine("WHERE B.MenuItem_ID = @ID  AND B.IsEnable = 1");
                            var addOn = await dbConnection.QueryAsync<ViewAddOnItem>(query.ToString(), new { ID = item.ID });
                            if (addOn != null)
                            {
                                item.AddOns = addOn.ToList();
                                foreach (var item1 in addOn)
                                {
                                    var addOption = await dbConnection.QueryAsync<ViewAddOnOption>("SELECT * FROM [dbo].[AddOnOption] WHERE AddOn_ID = @ID", new { ID = item1.ID });
                                    item1.Options = addOption.ToList();
                                }
                            }
                        }
                    }
                    return results.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ViewMenuItem> GetMenuItemByID(int MenuItem_ID)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("select MI.*,MH.TaxType,MG.Name as MenuGroup_Name from MenuHour MH");
                    query.AppendLine("inner join MenuGroup MG on MG.MenuHour_ID = MH.ID");
                    query.AppendLine("inner join MenuItem MI on MI.MenuGroup_ID = MG.ID");
                    query.AppendLine("where  MI.ID = @MENUITEM and MI.IsEnable = 1");
                    var results = await dbConnection.QueryFirstOrDefaultAsync<ViewMenuItem>(query.ToString(), new { MENUITEM = MenuItem_ID  });
                    if (results != null)
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT A.*  FROM [dbo].[AddOn] as A");
                        query.AppendLine("INNER JOIN [dbo].[MenuItemAddOn] as B");
                        query.AppendLine("ON B.AddOn_ID = A.ID");
                        query.AppendLine("WHERE B.MenuItem_ID = @ID AND B.IsEnable = 1");
                        var addOn = await dbConnection.QueryAsync<ViewAddOnItem>(query.ToString(), new { ID = results.ID });
                        if (addOn != null)
                        {
                            results.AddOns = addOn.ToList();

                            foreach (var item in addOn)
                            {
                                var addOption = await dbConnection.QueryAsync<ViewAddOnOption>("SELECT * FROM [dbo].[AddOnOption] WHERE AddOn_ID = @ID", new { ID = item.ID });
                                item.Options = addOption.ToList();
                            }
                        }

                    }
                    return results;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostOrder(CreateOrder data, int cusid )
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();


                    #region  Get Order_NO

                    query.AppendLine("SELECT TOP 1 Code from [Order]");
                    query.AppendLine("WHERE [Year] = (datepart(year,getdate())%(100)) ");
                    query.AppendLine("and Order_Type = @ORDERTYPE and RES_ID = @RESID ORDER BY Code DESC");
                    int lastCode = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { ORDERTYPE = (int)data.Order_Type, RESID = data.RES_ID });
                    lastCode = lastCode == 0 ? 1 : lastCode + 1;

                    query = new StringBuilder();
                    query.AppendLine("SELECT [FORMAT] FROM RestaurantFormatOrder WHERE RES_ID = @RESID");
                    var Format = await dbConnection.QueryFirstOrDefaultAsync<string>(query.ToString(), new { ORDERTYPE = (int)data.Order_Type, RESID = data.RES_ID });

                    Format += lastCode.ToString();
                    if ((int)data.Order_Type == 1)
                    {
                        Format += "PI";

                    }
                    else if ((int)data.Order_Type == 2)
                    {
                        Format += "DI";
                    }
                    else
                    {
                        Format += "DV";

                    }



                    #endregion


                    #region Add Order

                    query = new StringBuilder();
                    query.AppendLine(" EXEC [dbo].[AddOrder] @LastCode = @Code,@RES_ID = @RESID,@ORDERTYPE = @Type,@CUS_ID = @CUSID,@PaymentMethod = @Payment,@StartTime = @TimeStart, @EndTime = @TimeEnd,@FORMAT = @Format ;");

                    var statusOrder = await dbConnection.ExecuteAsync(query.ToString(),
                        new
                        {
                            ID = data.orderID,
                            Type = (int)data.Order_Type,
                            RESID = data.RES_ID,
                            CUSID = cusid,
                            TimeStart = data.StartTime,
                            TimeEnd = data.EndTime,
                            Payment = 0,
                            Code = lastCode,
                            Format = Format
                        });

                    #endregion

                    statusOrder = 1;
                    if (statusOrder == 1)
                    {

                        #region Order Detail

                        query = new StringBuilder();
                        query.AppendLine("SELECT * from [Order]");
                        query.AppendLine("WHERE Order_Type = @ORDERTYPE and RES_ID = @RESID and Code = @CODE");
                        var order = await dbConnection.QueryFirstOrDefaultAsync<OrderDetail>(query.ToString(), new { ORDERTYPE = data.Order_Type, RESID = data.RES_ID, CODE = lastCode });
                        var orderid = order.ID;

                        #endregion

                        if (data.Items != null)

    

                            foreach (var item in data.Items)
                            {

                                #region  Update Quota 

                                var quota = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT Quota FROM [dbo].[MenuItem] WHERE ID = @ID", new { ID = item.MenuItem_ID });
                                var updateQuota = 0;
                                if (quota == item.Amount)
                                {
                                    updateQuota = await dbConnection.ExecuteAsync("UPDATE [dbo].[MenuItem] SET Quota = 0 WHERE ID = @ID", new { ID = item.MenuItem_ID });
                                }
                                else if (quota == 0 || item.Amount > quota || item.Amount == 0)
                                {
                                    return 0;
                                }
                                else
                                {
                                    updateQuota = await dbConnection.ExecuteAsync("UPDATE [dbo].[MenuItem] SET Quota = @Quota WHERE ID = @ID", new { ID = item.MenuItem_ID, Quota = quota - item.Amount });
                                }

                                #endregion

                                #region Add MenuItem
                                query = new StringBuilder();
                                query.AppendLine("INSERT INTO OrderItem ([MenuItem_ID],[Price],[Amount],[Comment],[Order_ID])");
                                query.AppendLine("SELECT ID,(Price * @AMOUNT),@AMOUNT,@COMMENT,@ORDERID");
                                query.AppendLine("FROM MenuItem WHERE ID = @ITEMID");
                                int statusItem = await dbConnection.ExecuteAsync(query.ToString(), new { AMOUNT = item.Amount, ITEMID = item.MenuItem_ID, COMMENT = item.Comment, ORDERID = orderid });


                                #endregion

                                if (statusItem == 1)
                                {
                                    if (item.AddOns != null)
                                    {
                                        #region Add AddOn Options

                                        foreach (var item1 in item.AddOns)
                                        {
                                            query = new StringBuilder();
                                            query.AppendLine("SELECT ID from [OrderItem]");
                                            query.AppendLine("WHERE MenuItem_ID = @ITEMID and Order_ID = @ORDERID Order By ID DESC");
                                            int orderitemid = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { ITEMID = item.MenuItem_ID, ORDERID = orderid });
                                            query = new StringBuilder();
                                            query.AppendLine("INSERT INTO OrderItemAddOn ([AddOnOption_ID],[Price],[OrderItem_ID])");
                                            query.AppendLine("SELECT ID,(Price * @AMOUNT),@ORDERITEMID");
                                            query.AppendLine("FROM AddOnOption");
                                            query.AppendLine("WHERE ID = @ID");
                                            await dbConnection.ExecuteAsync(query.ToString(), new { ORDERITEMID = orderitemid, AMOUNT = item.Amount, ID = item1.AddOnOption_ID });
                                        }


                                        #endregion
                                    }
                                }
                            }
                        if (data.Address != null && data.Customer != null  && data.Order_Type == OrderType_Customer.Delivery)
                        {

                            query = new StringBuilder();
                            query.AppendLine("INSERT INTO [OrderAddress] (Order_ID,CustomerName,[Name],[Location],[Address],[Address_Detail],[Comment],[PhoneNumber]) VALUES(@ORDERID,@CustomerName,@Name,@Location,@Address,@Address_Detail,@Comment,@PhoneNumber)");
                            var addOrderAddress = await dbConnection.ExecuteAsync(query.ToString(),
                                new
                                {

                                    ORDERID = orderid,
                                    CustomerName = data.Customer.Name,
                                    Name = data.Address.Name,
                                    Location = data.Address.Location,
                                    Address = data.Address.Address,
                                    Address_Detail = data.Address.AddressDetail,
                                    Comment = data.Address.Comment,
                                    PhoneNumber = data.Customer.PhoneNumber
                                });

                        }
                        else
                        {
                            query = new StringBuilder();
                            query.AppendLine("INSERT INTO [OrderAddress] (Order_ID,CustomerName,[Name],[Location],[Address],[Address_Detail],[Comment],[PhoneNumber]) SELECT '" + orderid + "' as Order_ID,Firstname + '  ' + Lastname as CustomerName,LocationName,Location,LocationAddress,LocationDetail,LocationNote,PhoneNumber FROM [dbo].[CUSTOMER] WHERE CUS_ID = @CUS_ID");
                            var addOrderAddress = await dbConnection.ExecuteAsync(query.ToString(),
                                  new
                                  {
                                      CUS_ID = cusid
                                  });
                        }


                        if (data.Order_Type == OrderType_Customer.Delivery)
                        {
                            query = new StringBuilder();
                            query.AppendLine("UPDATE [dbo].[Order] SET DeliveryFee = @FEE,OrderID = @ID, LogID = @LogID,Distance = @Distance WHERE ID = @ORDERID");
                            await dbConnection.ExecuteAsync(query.ToString(),
                                new
                                {
                                    ID = data.orderID,
                                    Distance = data.distance,
                                    ORDERID = orderid,
                                    LogID = data.logID,
                                    FEE = data.DeliveryFee

                                });
                        }

                        query = new StringBuilder();
                        query.AppendLine(" EXEC dbo.updateOrderPrice @ORDERID = @ORDER, @RESID = @RES,@COUPON = @COUPONID,@CUSID = @CUS;");
                        var update = await dbConnection.ExecuteAsync(query.ToString(), new { ORDER = orderid, RES = data.RES_ID, COUPONID = data.Coupon_ID, CUS = cusid });
                        if (update == 1)
                            return orderid;
                        else
                            return 0;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<ViewCustomerOrderDetail> GetOrderByID(int orderid, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT a.*,B.Location as RestaurantLocation, C.Location as CustomerLocation FROM [Order] A");
                    query.AppendLine("INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("where A.CUS_ID = @CUS_ID and A.ID = @ORDER_ID");
                    var result = await dbConnection.QueryFirstOrDefaultAsync<ViewCustomerOrderDetail>(query.ToString(), new { CUS_ID = cusid, ORDER_ID = orderid });
                    if (result != null)
                    {
                        result.OrderRef = await dbConnection.QueryFirstOrDefaultAsync<string>("SELECT OrderRef  FROM [OhoApp].[Delivery].[RateDetails] WHERE OrderNO = @OrderNO ", new { OrderNO = result.Order_No });
                        if (result.OrderRef != null)
                        {
                            result.DriverID = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT DriverID FROM [OhoApp].[Delivery].[DriverDetails] WHERE OrderRef = @ID", new { ID = result.OrderRef });
                        }
                        result.OrderAddress = await dbConnection.QueryFirstOrDefaultAsync<OrderAddress>("SELECT * FROM OrderAddress where Order_ID = @ORDER_ID", new { ORDER_ID = orderid });
                        result.Distance = (result.OrderAddress == null || result.RestaurantLocation == null ? 0m : (decimal)CalculateDistance(GetLocation(result.OrderAddress.Location), GetLocation(result.RestaurantLocation)));
                        query = new StringBuilder();
                        query.AppendLine("SELECT a.ID,MI.Name_EN,MI.Name_TH,MI.Desc_EN,MI.Desc_TH,a.Amount,a.Price,MH.TaxType");
                        query.AppendLine(",a.Comment,MI.MenuGroup_ID");
                        query.AppendLine("FROM [OrderItem] a");
                        query.AppendLine("INNER JOIN MenuItem MI ON MI.ID = a.MenuItem_ID");
                        query.AppendLine("INNER JOIN MenuGroup MG ON MG.ID = MI.MenuGroup_ID");
                        query.AppendLine("INNER JOIN MenuHour MH ON MG.MenuHour_ID = MH.ID");
                        query.AppendLine("WHERE a.Order_ID = @ORDER");
                        var orderItems = await dbConnection.QueryAsync<ViewOrderItem>(query.ToString(), new { ORDER = orderid });
                        result.Items = orderItems.ToList();
                        if (result.Items.Count > 0)
                        {
                            foreach (var item in orderItems)
                            {
                                var AddOnOption = await dbConnection.QueryAsync<ViewAddOnOrderOption>(@"SELECT A.ID,B.AddOn_ID,A.AddOnOption_ID,A.OrderItem_ID,B.Name_EN,B.Name_TH,A.Price FROM [dbo].[OrderItemAddOn] as A  INNER JOIN [dbo].[AddOnOption] as B ON B.ID = A.AddOnOption_ID  WHERE  A.OrderItem_ID = @ORDER ",
                                    new
                                    {
                                        ORDER = item.ID

                                    });
                                if (AddOnOption != null)
                                {
                                    item.Options = AddOnOption.ToList();
                                }
                            }
                        }
                    }                 
                
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewCustomerOrderDetail>> GetOrderByOrderNO(string OrderNO, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT a.*,B.Location as RestaurantLocation, C.Location as CustomerLocation FROM [Order] A");
                    query.AppendLine("INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("where A.CUS_ID = @CUS_ID and Order_NO LIKE '%" + OrderNO + "%'");
                    var result = await dbConnection.QueryAsync<ViewCustomerOrderDetail>(query.ToString(), new { CUS_ID = cusid });
                    if (result != null)
                    {
                        foreach (var item in result)
                        {
                            item.OrderRef = await dbConnection.QueryFirstOrDefaultAsync<string>("SELECT OrderRef  FROM [OhoApp].[Delivery].[RateDetails] WHERE OrderNO = @OrderNO ", new { OrderNO = item.Order_No });
                            if (item.OrderRef != null)
                            {
                                item.DriverID = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT DriverID FROM [OhoApp].[Delivery].[DriverDetails] WHERE OrderRef = @ID", new { ID = item.OrderRef });
                            }
                            item.OrderAddress = await dbConnection.QueryFirstOrDefaultAsync<OrderAddress>("SELECT * FROM OrderAddress where Order_ID = @ORDER_ID", new { ORDER_ID = item.ID });
                            item.Distance = (item.OrderAddress == null || item.RestaurantLocation == null ? 0m : (decimal)CalculateDistance(GetLocation(item.OrderAddress.Location), GetLocation(item.RestaurantLocation)));
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.ID,MI.Name_EN,MI.Name_TH,MI.Desc_EN,MI.Desc_TH,a.Amount,a.Price,MH.TaxType");
                            query.AppendLine(",a.Comment,MI.MenuGroup_ID");
                            query.AppendLine("FROM [OrderItem] a");
                            query.AppendLine("INNER JOIN MenuItem MI ON MI.ID = a.MenuItem_ID");
                            query.AppendLine("INNER JOIN MenuGroup MG ON MG.ID = MI.MenuGroup_ID");
                            query.AppendLine("INNER JOIN MenuHour MH ON MG.MenuHour_ID = MH.ID");
                            query.AppendLine("WHERE a.Order_ID = @ORDER");
                            var orderItems = await dbConnection.QueryAsync<ViewOrderItem>(query.ToString(), new { ORDER = item.ID });
                            item.Items = orderItems.ToList();
                            if (item.Items.Count > 0)
                            {
                                foreach (var itemAddOn in orderItems)
                                {
                                    var AddOnOption = await dbConnection.QueryAsync<ViewAddOnOrderOption>(@"SELECT A.ID,B.AddOn_ID,A.AddOnOption_ID,A.OrderItem_ID,B.Name_EN,B.Name_TH,A.Price FROM [dbo].[OrderItemAddOn] as A  INNER JOIN [dbo].[AddOnOption] as B ON B.ID = A.AddOnOption_ID  WHERE  A.OrderItem_ID = @ORDER ",
                                        new
                                        {
                                            ORDER = itemAddOn.ID

                                        });
                                    if (AddOnOption != null)
                                    {
                                        itemAddOn.Options = AddOnOption.ToList();
                                    }
                                }
                            }
                        }
                    }
                    return result.ToList();
                }
            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewCustomerOrderDetail>> GetCurrentOrder(int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT a.*,B.Location as RestaurantLocation, C.Location as CustomerLocation FROM [Order] A");
                    query.AppendLine("INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("where A.CUS_ID = @CUS_ID AND A.Status = 1 ");
                    var result = await dbConnection.QueryAsync<ViewCustomerOrderDetail>(query.ToString(), new { CUS_ID = cusid });
                    if (result != null)
                    {
                        foreach (var item in result)
                        {
                            item.OrderRef = await dbConnection.QueryFirstOrDefaultAsync<string>("SELECT OrderRef  FROM [OhoApp].[Delivery].[RateDetails] WHERE OrderNO = @OrderNO ", new { OrderNO = item.Order_No });
                            if (item.OrderRef != null)
                            {
                                item.DriverID = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT DriverID FROM [OhoApp].[Delivery].[DriverDetails] WHERE OrderRef = @ID", new { ID = item.OrderRef });
                            }
                            item.OrderAddress = await dbConnection.QueryFirstOrDefaultAsync<OrderAddress>("SELECT * FROM OrderAddress where Order_ID = @ORDER_ID", new { ORDER_ID = item.ID });
                            item.Distance = (item.OrderAddress == null || item.RestaurantLocation == null ? 0m : (decimal)CalculateDistance(GetLocation(item.OrderAddress.Location), GetLocation(item.RestaurantLocation)));
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.ID,MI.Name_EN,MI.Name_TH,MI.Desc_EN,MI.Desc_TH,a.Amount,a.Price,MH.TaxType");
                            query.AppendLine(",a.Comment,MI.MenuGroup_ID");
                            query.AppendLine("FROM [OrderItem] a");
                            query.AppendLine("INNER JOIN MenuItem MI ON MI.ID = a.MenuItem_ID");
                            query.AppendLine("INNER JOIN MenuGroup MG ON MG.ID = MI.MenuGroup_ID");
                            query.AppendLine("INNER JOIN MenuHour MH ON MG.MenuHour_ID = MH.ID");
                            query.AppendLine("WHERE a.Order_ID = @ORDER");
                            var orderItems = await dbConnection.QueryAsync<ViewOrderItem>(query.ToString(), new { ORDER = item.ID });
                            item.Items = orderItems.ToList();
                            if (item.Items.Count > 0)
                            {
                                foreach (var itemAddOn in orderItems)
                                {
                                    var AddOnOption = await dbConnection.QueryAsync<ViewAddOnOrderOption>(@"SELECT A.ID,B.AddOn_ID,A.AddOnOption_ID,A.OrderItem_ID,B.Name_EN,B.Name_TH,A.Price FROM [dbo].[OrderItemAddOn] as A  INNER JOIN [dbo].[AddOnOption] as B ON B.ID = A.AddOnOption_ID  WHERE  A.OrderItem_ID = @ORDER ",
                                        new
                                        {
                                            ORDER = itemAddOn.ID

                                        });
                                    if (AddOnOption != null)
                                    {
                                        itemAddOn.Options = AddOnOption.ToList();
                                    }
                                }
                            }
                        }

                    }

                    return result.OrderByDescending(x => x.ID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ViewCustomerOrderDetail>> GetHistoryOrder(int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT a.*,B.Location as RestaurantLocation, C.Location as CustomerLocation FROM [Order] A");
                    query.AppendLine("INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("where A.CUS_ID = @CUS_ID AND A.Status  > 1 ORDER BY A.ID ASC");
                    var result = await dbConnection.QueryAsync<ViewCustomerOrderDetail>(query.ToString(), new { CUS_ID = cusid });
                    if (result != null)
                    {
                        foreach (var item in result)
                        {
                            item.OrderRef = await dbConnection.QueryFirstOrDefaultAsync<string>("SELECT OrderRef  FROM [OhoApp].[Delivery].[RateDetails] WHERE OrderNO = @OrderNO ", new { OrderNO = item.Order_No });
                            if (item.OrderRef != null)
                            {
                                item.DriverID = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT DriverID FROM [OhoApp].[Delivery].[DriverDetails] WHERE OrderRef = @ID", new { ID = item.OrderRef });
                            }
                            item.OrderAddress = await dbConnection.QueryFirstOrDefaultAsync<OrderAddress>("SELECT * FROM OrderAddress where Order_ID = @ORDER_ID", new { ORDER_ID = item.ID });
                            item.Distance = (item.OrderAddress == null || item.RestaurantLocation == null ? 0m : (decimal)CalculateDistance(GetLocation(item.OrderAddress.Location), GetLocation(item.RestaurantLocation)));
                            query = new StringBuilder();
                            query.AppendLine("SELECT a.ID,MI.Name_EN,MI.Name_TH,MI.Desc_EN,MI.Desc_TH,a.Amount,a.Price,MH.TaxType");
                            query.AppendLine(",a.Comment,MI.MenuGroup_ID");
                            query.AppendLine("FROM [OrderItem] a");
                            query.AppendLine("INNER JOIN MenuItem MI ON MI.ID = a.MenuItem_ID");
                            query.AppendLine("INNER JOIN MenuGroup MG ON MG.ID = MI.MenuGroup_ID");
                            query.AppendLine("INNER JOIN MenuHour MH ON MG.MenuHour_ID = MH.ID");
                            query.AppendLine("WHERE a.Order_ID = @ORDER");
                            var orderItems = await dbConnection.QueryAsync<ViewOrderItem>(query.ToString(), new { ORDER = item.ID });
                            item.Items = orderItems.ToList();
                            if (item.Items.Count > 0)
                            {
                                foreach (var itemAddOn in orderItems)
                                {
                                    var AddOnOption = await dbConnection.QueryAsync<ViewAddOnOrderOption>(@"SELECT A.ID,B.AddOn_ID,A.AddOnOption_ID,A.OrderItem_ID,B.Name_EN,B.Name_TH,A.Price FROM [dbo].[OrderItemAddOn] as A  INNER JOIN [dbo].[AddOnOption] as B ON B.ID = A.AddOnOption_ID  WHERE  A.OrderItem_ID = @ORDER ",
                                        new
                                        {
                                            ORDER = itemAddOn.ID

                                        });
                                    if (AddOnOption != null)
                                    {
                                        itemAddOn.Options = AddOnOption.ToList();
                                    }
                                }
                            }
                        }

                    }

                    return result.OrderByDescending( x => x.ID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<CouponDetail>> GetAllCoupon(int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT B.ID,B.Code,B.RES_ID,B.Discount,B.Min,B.ExpirationDate,A.IsUse,B.IsPercent FROM [dbo].[Coupon_Save] as A INNER JOIN [dbo].[Coupon] as B  ON B.ID = A.Coupon_ID");
                    query.AppendLine("WHERE A.CUS_ID = @CUS_ID ORDER BY A.IsUse ASC");
                    var data = await dbConnection.QueryAsync<CouponDetail>(query.ToString(), new { CUS_ID = cusid });
                    if (data != null)
                    {
                        return data.ToList();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<CouponDetail> GetCouponDetail (Coupon data,int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT * FROM [dbo].[Coupon] WHERE Code = @Code");
                    var _data = await dbConnection.QueryFirstOrDefaultAsync<CouponDetail>(query.ToString(), new { Code = data.Code });
                    if (_data != null)
                    {
                        //Check Expiration
                        if (DateTime.Now > _data.ExpirationDate )
                        {
                            _data.IsExp = true;
                        }
                        else
                        {
                            _data.IsExp = false;
                        }

                        query = new StringBuilder();
                        query.AppendLine("SELECT ID FROM [dbo].[Coupon_Save] WHERE CUS_ID = @CUS_ID AND Coupon_ID = @ID");
                        var HasCoupon = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { ID = _data.ID, CUS_ID = cusid });
                        if (HasCoupon > 0)
                        {
                            data.HasCoupon = true;
                        }
                        else
                        {
                            data.HasCoupon = false;
                        }

                        
                        return _data;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<int> SaveCoupon(SaveCoupon data, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    //Add Coupon
                    var query = new StringBuilder();
                    int status = 0;
                    query.AppendLine("SELECT ID FROM [dbo].[Coupon_Save] WHERE CUS_ID = @CUS_ID AND Coupon_ID = @ID");
                    var HasCoupon = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { CUS_ID = cusid , ID = data.Coupon_ID });

                    if (HasCoupon > 0)
                    {
                        return 2;
                    }
                    else
                    {
                        query = new StringBuilder();
                        query.AppendLine("INSERT INTO [dbo].[Coupon_Save](Coupon_ID,CUS_ID) VALUES(@Coupon_ID,@CUS_ID)");
                        status = await dbConnection.ExecuteAsync(query.ToString(),
                            new
                            {
                                Coupon_ID = data.Coupon_ID,
                                CUS_ID = cusid

                            });
                        if (status == 1)
                        {
                            return 1;
                        }
                        else
                        {
                            return 0;
                        }
                    }             
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> CheckCouponForRestaurant(CheckCoupon data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    //Add Coupon
                    var query = new StringBuilder();
                    query.AppendLine("SELECT ID FROM [dbo].[Coupon] WHERE RES_ID = @RES_ID AND ID = @ID");
                    var checkCoupon = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { RES_ID = data.RES_ID, ID = data.Coupon_ID });


                    var checkUnit = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT COUNT(ID) as Unit FROM [dbo].[Coupon_Save] WHERE IsUse = 1 AND Coupon_ID  = @Coupon_ID");


                    if (checkCoupon > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                  
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        public async Task<int> PostReport(ReportOrder data,int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    var query = new StringBuilder();
                    var CreateDate = DateTime.Now;
                    query.AppendLine("INSERT INTO [dbo].[Customer_Report](Order_ID,CUS_ID,Reason_ID,Description,CreateDate) VALUES(@Order_ID,@CUS_ID,@Reason_ID,@Description,@CreateDate)");
                    var addReport = await dbConnection.ExecuteAsync(query.ToString(), 
                        new 
                        {
                            Order_ID = data.Order_ID,
                            CUS_ID = cusid,
                            Reason_ID = data.Reason_ID,
                            Description = data.Description,
                            CreateDate = CreateDate
                        });
                    addReport = await dbConnection.ExecuteAsync("UPDATE [dbo].[Order] SET Status = 96,RestaurantStatus = 95 WHERE ID = @ID", new { ID = data.Order_ID });



                    if (addReport == 1)
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT ID FROM [dbo].[Customer_Report] WHERE CreateDate = @CreateDate AND CUS_ID = @CUS_ID");
                        var id = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(),new { CreateDate = CreateDate , CUS_ID = cusid });


                        return id;
                    }
                    else
                    {
                        return 0;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PostImageReport(string filename,int id,int orderid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    //Add Coupon
                    var query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[ListImageReport](ReportID,Name) VALUES(@ID,@Name)");
                    var addImage = await dbConnection.ExecuteAsync(query.ToString(), 
                        new 
                        {
                            ID = id,
                            Name = filename
                        });


                    if (addImage == 1)
                    {
                        
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ReasonReport>> GetReasonReport()
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    //Add Coupon
                    var query = new StringBuilder();
                    query.AppendLine("SELECT* FROM [dbo].[ListReport]");
                    var data = await dbConnection.QueryAsync<ReasonReport>(query.ToString(), new { });



                    if (data != null)
                    {
                        return data.ToList() ;
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> ChangeStatusDeliveryComplete(ChangeStatusDeliveryToComplete data ,int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("UPDATE [dbo].[Order] SET Status = 20 WHERE  ID = @ID AND Order_NO = @Order_NO AND Order_Type = 3");
                    var status = await dbConnection.ExecuteAsync(query.ToString(), new { ID = data.ID, Order_NO = data.Order_NO });



                    if (status > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PostOrderRate(OrderRate data, int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int statusOrder = (int)OrderStatus.Complete;
                    int status;
                    var query = new StringBuilder();
                    query.AppendLine("SELECT a.*,B.Location as RestaurantLocation, C.Location as CustomerLocation FROM [Order] A");
                    query.AppendLine("INNER JOIN RESTAURANT B ON A.RES_ID = B.RES_ID");
                    query.AppendLine("INNER JOIN CUSTOMER C ON A.CUS_ID = C.CUS_ID");
                    query.AppendLine("where A.CUS_ID = @CUS_ID and A.ID = @ORDER_ID and A.Status = @STATUS");
                    var order = await dbConnection.QueryFirstOrDefaultAsync<OrderDetail>(query.ToString(), new { CUS_ID = cusid, ORDER_ID = data.Order_ID, STATUS = statusOrder });
                    if (order == null)
                        return false;
                    else if ((int)order.Status != statusOrder || order.Order_Type != OrderType_Customer.Delivery)
                        return false;
                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [RestaurantRate] ([RES_ID],[CUS_ID],[Order_ID],[Rate],[Comment]) VALUES");
                    query.AppendLine("(@RESID,@CUSID,@ORDERID,@RATE,@COMMENT)");
                    status = await dbConnection.ExecuteAsync(query.ToString(), new { RESID = order.RES_ID, CUSID = cusid, ORDERID = data.Order_ID, RATE = data.FoodRate, COMMENT = data.FoodComment });
                    if (status == 0)
                        return false;
                    query = new StringBuilder();
                    query.AppendLine("INSERT INTO [dbo].[DriverRate](DriverID,CUS_ID,Order_ID,Rate,Comment) VALUES(@DriverID,@CUS_ID,@Order_ID,@Rate,@Comment)");
                    status = await dbConnection.ExecuteAsync(query.ToString(), 
                        new 
                        {
                            DriverID = order.DriverID,
                            CUS_ID = cusid,
                            Order_ID = data.Order_ID,
                            Rate = data.DriverRate,
                            Comment = data.DriverComment
                        });

                    if (status == 0)
                    {
                        return false;

                    }
                    else
                    {
                        query = new StringBuilder();
                        query.AppendLine("UPDATE [Order] SET [Status] = @STATUS,RestaurantStatus = @STATUS,[Complete_Time] = GETDATE()");
                        query.AppendLine("WHERE ID = @ORDERID AND RES_ID = @RESID");
                        status = await dbConnection.ExecuteAsync(query.ToString(), new { STATUS = statusOrder, ORDERID = data.Order_ID, RESID = order.RES_ID });
                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<bool> PutCancelOrder(int Order_ID,int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    //Add Coupon
                    var query = new StringBuilder();
                    query.AppendLine("SELECT ID FROM [dbo].[Order] WHERE ID = @ID");
                    var Order = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { ID = Order_ID });

                    if (Order > 0)
                    {
                        query = new StringBuilder();
                        query.AppendLine("SELECT B.ID FROM [dbo].[Order] as A INNER JOIN [dbo].[Coupon_Save] as B ON B.Order_ID =  A.ID WHERE A.ID = @ID");
                        var CouponID = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(),new 
                        {
                            ID = Order_ID
                        });
                        query = new StringBuilder();
                        query.AppendLine("UPDATE [dbo].[Coupon_Save] SET IsUse = 0 WHERE ID = @ID");
                        var updateStatusCoupon = await dbConnection.ExecuteAsync(query.ToString(), new { ID = CouponID });

                        query = new StringBuilder();
                        query.AppendLine("SELECT B.MenuItem_ID,B.Amount FROM [dbo].[Order] as A INNER JOIN [dbo].[OrderItem] as B ON B.Order_ID = A.ID WHERE B.Order_ID = @ID");
                        var menuItem = await dbConnection.QueryAsync<menuItemCancel>(query.ToString(), new { ID = Order_ID });
                        foreach (var item in menuItem)
                        {
                            query = new StringBuilder();
                            query.AppendLine("UPDATE [dbo].[MenuItem] SET Quota += @Quota  WHERE ID = @ID");
                            var status = await dbConnection.ExecuteAsync(query.ToString(), new { Quota = item.Amount,ID =item.MenuItem_ID });  
                        }
                        query = new StringBuilder();
                        query.AppendLine("UPDATE [dbo].[Order] SET Status = 98,RestaurantStatus = 98 WHERE ID = @ID");
                        await dbConnection.ExecuteAsync(query.ToString(), new { ID = Order_ID });
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                    
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        public async Task<bool> ChangeStatusOrder(ChangeOrderStatus data,int cusid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    var query = new StringBuilder();
                    if (data.Status == 2 || data.Status == 3 || data.Status == 4 )
                    {
                        query.AppendLine("UPDATE [dbo].[Order] SET RestaurantStatus = @Status WHERE CUS_ID = @CUS_ID AND ID = @ID");
                    }
                    else 
                    {
                        query.AppendLine("UPDATE [dbo].[Order] SET Status = @Status,RestaurantStatus = @Status  WHERE CUS_ID = @CUS_ID AND ID = @ID");
                    }
                    var changeStatus = await dbConnection.ExecuteAsync(query.ToString(), new { ID = data.Order_ID, CUS_ID = cusid, Status = data.Status });



                    if (changeStatus > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        public async Task<List<OrderTypeDeliery>> GetOrderTypeDelivery()
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    //Add Coupon
                    var query = new StringBuilder();
                    query.AppendLine("SELECT A.OrderNO,A.OrderRef  FROM  [Delivery].[OrderDetails] as A INNER JOIN [dbo].[Order] as B ON B.Order_NO = A.OrderNO WHERE B.Status <= 20 AND B.RestaurantStatus <= 20 AND B.Order_Type = 3 ");
                    var data = await dbConnection.QueryAsync<OrderTypeDeliery>(query.ToString(), new { });



                    if (data != null)
                    {
                        return data.ToList();
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> UpdateStatusOrderTypeDelivery(string OrderNO,int OrderStatus)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();

                    if (OrderStatus == 20)
                    {
                        query.AppendLine("UPDATE [dbo].[Order] SET RestaurantStatus = @Status,Status = @Status  WHERE Order_NO = @Order_NO");
                    }
                    else if (OrderStatus == 100)
                    {
                        query.AppendLine("UPDATE [dbo].[Order] SET RestaurantStatus = @Status,Status = @Status  WHERE Order_NO = @Order_NO");
                    }
                    else
                    {
                        query.AppendLine("UPDATE [dbo].[Order] SET RestaurantStatus = @Status  WHERE Order_NO = @Order_NO");
                    }
                    var status = await dbConnection.ExecuteAsync(query.ToString(), new { Order_NO = OrderNO, Status = OrderStatus });



                    if (status == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        public async Task<bool> Names()
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    //Add Coupon
                    var query = new StringBuilder();
                    query.AppendLine("");
                    query.AppendLine("");
                    query.AppendLine("");
                    var checkCoupon = await dbConnection.QueryFirstOrDefaultAsync<int>(query.ToString(), new { });



                    if (checkCoupon > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }      

       

    }


}
