﻿using Dapper;
using Microsoft.Extensions.Configuration;
using OhoApp.DTOs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhoApp.Services
{
    public interface IAnalyticData
    {
        Task<List<RevenueAndOrder>> GetTotalRevenueDaily(int resid);
        Task<List<RevenueAndOrder>> GetTotalRevenueWeekly(int resid);
        Task<List<RevenueAndOrder>> GetTotalRevenueMonthly(int resid);
        Task<List<RevenueAndOrder>> GetPickupRevenueDaily(int resid);
        Task<List<RevenueAndOrder>> GetPickupRevenueWeekly(int resid);
        Task<List<RevenueAndOrder>> GetPickupRevenueMonthly(int resid);
        Task<List<RevenueAndOrder>> GetDeliveryRevenueDaily(int resid);
        Task<List<RevenueAndOrder>> GetDeliveryRevenueWeekly(int resid);
        Task<List<RevenueAndOrder>> GetDeliveryRevenueMonthly(int resid);
        Task<List<RevenueAndOrder>> GetDineInRevenueDaily(int resid);
        Task<List<RevenueAndOrder>> GetDineInRevenueWeekly(int resid);
        Task<List<RevenueAndOrder>> GetDineInRevenueMonthly(int resid);
        Task<List<PopularItem>> GetPopularItemDaily(int resid);
        Task<List<PopularItem>> GetPopularItemWeekly(int resid);
        Task<List<PopularItem>> GetPopularItemMonthly(int resid);
    }

    public class AnalyticRepository : IAnalyticData 
    {

        private readonly IConfiguration configuration;

        public AnalyticRepository(IConfiguration config)
        {
            this.configuration = config;
        }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(ConfigurationExtensions.GetConnectionString(this.configuration, "DefaultConnection"));
            }
        }

        public async Task<List<RevenueAndOrder>> GetTotalRevenueDaily(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 FORMAT (CAST(CreatedDate AS DATE), 'ddd dd MMM') AS Date,COUNT(ID) AS Orders,SUM(Total) AS Price");
                    query.AppendLine("FROM [Order]");
                    query.AppendLine("WHERE RestaurantStatus in (20) AND RES_ID = @RESID");
                    query.AppendLine("GROUP BY CONVERT(DATE,CreatedDate)");
                    query.AppendLine("ORDER BY CONVERT(DATE,CreatedDate) DESC");
                    var resdata = await dbConnection.QueryAsync<RevenueAndOrder>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RevenueAndOrder>> GetTotalRevenueWeekly(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 FORMAT(DATEADD(WEEK,DATEPART(WEEK,CreatedDate)-1, DATEADD(wk, DATEDIFF(wk,-1,DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)), 0)), 'dd MMM') + ");
                    query.AppendLine("' - ' + FORMAT(DATEADD(WEEK,DATEPART(WEEK,CreatedDate)-1, DATEADD(wk, DATEDIFF(wk,-1,DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)), 0)) + 6, 'dd MMM') AS Date,");
                    query.AppendLine("COUNT(ID) AS Orders,SUM(Total) AS Price");
                    query.AppendLine("FROM [Order]");
                    query.AppendLine("WHERE RestaurantStatus in (20) AND RES_ID = @RESID");
                    query.AppendLine("AND format(CAST(CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                    query.AppendLine("GROUP BY DATEPART(WEEK,CreatedDate)");
                    query.AppendLine("ORDER BY DATEPART(WEEK,CreatedDate) DESC");
                    var resdata = await dbConnection.QueryAsync<RevenueAndOrder>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RevenueAndOrder>> GetTotalRevenueMonthly(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 FORMAT (CAST(CreatedDate AS DATE), 'MMM') AS Date,COUNT(ID) AS Orders,SUM(Total) AS Price");
                    query.AppendLine("FROM [Order]");
                    query.AppendLine("WHERE RestaurantStatus in (20) AND RES_ID = @RESID");
                    query.AppendLine("AND format(CAST(CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                    query.AppendLine("GROUP BY format(CAST(CreatedDate AS DATE),'MMM')");
                    query.AppendLine("ORDER BY format(CAST(CreatedDate AS DATE),'MMM') DESC");
                    var resdata = await dbConnection.QueryAsync<RevenueAndOrder>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RevenueAndOrder>> GetPickupRevenueDaily(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 FORMAT (CAST(CreatedDate AS DATE), 'ddd dd MMM') AS Date,COUNT(ID) AS Orders,SUM(Total) AS Price");
                    query.AppendLine("FROM [Order]");
                    query.AppendLine("WHERE RestaurantStatus = 20 AND Order_Type = 1 AND RES_ID = @RESID");
                    query.AppendLine("GROUP BY CONVERT(DATE,CreatedDate)");
                    query.AppendLine("ORDER BY CONVERT(DATE,CreatedDate) DESC");
                    var resdata = await dbConnection.QueryAsync<RevenueAndOrder>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RevenueAndOrder>> GetPickupRevenueWeekly(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 FORMAT(DATEADD(WEEK,DATEPART(WEEK,CreatedDate)-1, DATEADD(wk, DATEDIFF(wk,-1,DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)), 0)), 'dd MMM') + ");
                    query.AppendLine("' - ' + FORMAT(DATEADD(WEEK,DATEPART(WEEK,CreatedDate)-1, DATEADD(wk, DATEDIFF(wk,-1,DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)), 0)) + 6, 'dd MMM') AS Date,");
                    query.AppendLine("COUNT(ID) AS Orders,SUM(Total) AS Price");
                    query.AppendLine("FROM [Order]");
                    query.AppendLine("WHERE RestaurantStatus = 20 AND Order_Type = 1 AND RES_ID = @RESID");
                    query.AppendLine("AND format(CAST(CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                    query.AppendLine("GROUP BY DATEPART(WEEK,CreatedDate)");
                    query.AppendLine("ORDER BY DATEPART(WEEK,CreatedDate) DESC");
                    var resdata = await dbConnection.QueryAsync<RevenueAndOrder>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RevenueAndOrder>> GetPickupRevenueMonthly(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 FORMAT (CAST(CreatedDate AS DATE), 'MMM') AS Date,COUNT(ID) AS Orders,SUM(Total) AS Price");
                    query.AppendLine("FROM [Order]");
                    query.AppendLine("WHERE RestaurantStatus = 20 AND Order_Type = 1 AND RES_ID = @RESID");
                    query.AppendLine("AND format(CAST(CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                    query.AppendLine("GROUP BY format(CAST(CreatedDate AS DATE),'MMM')");
                    query.AppendLine("ORDER BY format(CAST(CreatedDate AS DATE),'MMM') DESC");
                    var resdata = await dbConnection.QueryAsync<RevenueAndOrder>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RevenueAndOrder>> GetDeliveryRevenueDaily(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 FORMAT (CAST(CreatedDate AS DATE), 'ddd dd MMM') AS Date,COUNT(ID) AS Orders,SUM(Total) AS Price");
                    query.AppendLine("FROM [Order]");
                    query.AppendLine("WHERE RestaurantStatus = 20 AND Order_Type = 3 AND RES_ID = @RESID");
                    query.AppendLine("GROUP BY CONVERT(DATE,CreatedDate)");
                    query.AppendLine("ORDER BY CONVERT(DATE,CreatedDate) DESC");
                    var resdata = await dbConnection.QueryAsync<RevenueAndOrder>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RevenueAndOrder>> GetDeliveryRevenueWeekly(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 FORMAT(DATEADD(WEEK,DATEPART(WEEK,CreatedDate)-1, DATEADD(wk, DATEDIFF(wk,-1,DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)), 0)), 'dd MMM') + ");
                    query.AppendLine("' - ' + FORMAT(DATEADD(WEEK,DATEPART(WEEK,CreatedDate)-1, DATEADD(wk, DATEDIFF(wk,-1,DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)), 0)) + 6, 'dd MMM') AS Date,");
                    query.AppendLine("COUNT(ID) AS Orders,SUM(Total) AS Price");
                    query.AppendLine("FROM [Order]");
                    query.AppendLine("WHERE RestaurantStatus = 20 AND Order_Type = 3 AND RES_ID = @RESID");
                    query.AppendLine("AND format(CAST(CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                    query.AppendLine("GROUP BY DATEPART(WEEK,CreatedDate)");
                    query.AppendLine("ORDER BY DATEPART(WEEK,CreatedDate) DESC");
                    var resdata = await dbConnection.QueryAsync<RevenueAndOrder>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RevenueAndOrder>> GetDeliveryRevenueMonthly(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 FORMAT (CAST(CreatedDate AS DATE), 'MMM') AS Date,COUNT(ID) AS Orders,SUM(Total) AS Price");
                    query.AppendLine("FROM [Order]");
                    query.AppendLine("WHERE RestaurantStatus = 20 AND Order_Type = 3 AND RES_ID = @RESID");
                    query.AppendLine("AND format(CAST(CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                    query.AppendLine("GROUP BY format(CAST(CreatedDate AS DATE),'MMM')");
                    query.AppendLine("ORDER BY format(CAST(CreatedDate AS DATE),'MMM') DESC");
                    var resdata = await dbConnection.QueryAsync<RevenueAndOrder>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RevenueAndOrder>> GetDineInRevenueDaily(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 FORMAT (CAST(CreatedDate AS DATE), 'ddd dd MMM') AS Date,COUNT(ID) AS Orders,SUM(Total) AS Price");
                    query.AppendLine("FROM [Order]");
                    query.AppendLine("WHERE RestaurantStatus = 20 AND Order_Type = 2 AND RES_ID = @RESID");
                    query.AppendLine("GROUP BY CONVERT(DATE,CreatedDate)");
                    query.AppendLine("ORDER BY CONVERT(DATE,CreatedDate) DESC");
                    var resdata = await dbConnection.QueryAsync<RevenueAndOrder>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RevenueAndOrder>> GetDineInRevenueWeekly(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 FORMAT(DATEADD(WEEK,DATEPART(WEEK,CreatedDate)-1, DATEADD(wk, DATEDIFF(wk,-1,DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)), 0)), 'dd MMM') + ");
                    query.AppendLine("' - ' + FORMAT(DATEADD(WEEK,DATEPART(WEEK,CreatedDate)-1, DATEADD(wk, DATEDIFF(wk,-1,DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)), 0)) + 6, 'dd MMM') AS Date,");
                    query.AppendLine("COUNT(ID) AS Orders,SUM(Total) AS Price");
                    query.AppendLine("FROM [Order]");
                    query.AppendLine("WHERE RestaurantStatus = 20 AND Order_Type = 2 AND RES_ID = @RESID");
                    query.AppendLine("AND format(CAST(CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                    query.AppendLine("GROUP BY DATEPART(WEEK,CreatedDate)");
                    query.AppendLine("ORDER BY DATEPART(WEEK,CreatedDate) DESC");
                    var resdata = await dbConnection.QueryAsync<RevenueAndOrder>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<RevenueAndOrder>> GetDineInRevenueMonthly(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 FORMAT (CAST(CreatedDate AS DATE), 'MMM') AS Date,COUNT(ID) AS Orders,SUM(Total) AS Price");
                    query.AppendLine("FROM [Order]");
                    query.AppendLine("WHERE RestaurantStatus = 20 AND Order_Type = 2 AND RES_ID = @RESID");
                    query.AppendLine("AND format(CAST(CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                    query.AppendLine("GROUP BY format(CAST(CreatedDate AS DATE),'MMM')");
                    query.AppendLine("ORDER BY format(CAST(CreatedDate AS DATE),'MMM') DESC");
                    var resdata = await dbConnection.QueryAsync<RevenueAndOrder>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<PopularItem>> GetPopularItemDaily(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 C.Name_EN,C.Name_TH,COUNT(C.ID) AS Items");
                    query.AppendLine("FROM [Order] A");
                    query.AppendLine("INNER JOIN [OrderItem] B ON A.ID = B.Order_ID");
                    query.AppendLine("INNER JOIN [MenuItem] C ON B.MenuItem_ID = C.ID");
                    query.AppendLine("WHERE A.RestaurantStatus in (20) AND A.RES_ID = @RESID");
                    query.AppendLine("AND CONVERT(DATE,A.CreatedDate) = CONVERT(DATE,GETDATE())");
                    query.AppendLine("GROUP BY C.Name_EN,C.Name_TH");
                    query.AppendLine("ORDER BY COUNT(C.ID) DESC");
                    var resdata = await dbConnection.QueryAsync<PopularItem>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<PopularItem>> GetPopularItemWeekly(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 C.Name_EN,C.Name_TH,COUNT(C.ID) AS Items");
                    query.AppendLine("FROM [Order] A");
                    query.AppendLine("INNER JOIN [OrderItem] B ON A.ID = B.Order_ID");
                    query.AppendLine("INNER JOIN [MenuItem] C ON B.MenuItem_ID = C.ID");
                    query.AppendLine("WHERE A.RestaurantStatus in (20) AND A.RES_ID = @RESID");
                    query.AppendLine("AND DATEPART(WEEK,A.CreatedDate) = DATEPART(WEEK,GETDATE())");
                    query.AppendLine("AND format(CAST(A.CreatedDate AS DATE),'yyy') = format(CAST(GETDATE() AS DATE),'yyy')");
                    query.AppendLine("GROUP BY C.Name_EN,C.Name_TH");
                    query.AppendLine("ORDER BY COUNT(C.ID) DESC");
                    var resdata = await dbConnection.QueryAsync<PopularItem>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<PopularItem>> GetPopularItemMonthly(int resid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("SELECT TOP 4 C.Name_EN,C.Name_TH,COUNT(C.ID) AS Items");
                    query.AppendLine("FROM [Order] A");
                    query.AppendLine("INNER JOIN [OrderItem] B ON A.ID = B.Order_ID");
                    query.AppendLine("INNER JOIN [MenuItem] C ON B.MenuItem_ID = C.ID");
                    query.AppendLine("WHERE A.RestaurantStatus in (20,9) AND A.RES_ID = @RESID");
                    query.AppendLine("AND format(CAST(A.CreatedDate AS DATE),'MMM yyy') = format(CAST(GETDATE() AS DATE),'MMM yyy')");
                    query.AppendLine("GROUP BY C.Name_EN,C.Name_TH");
                    query.AppendLine("ORDER BY COUNT(C.ID) DESC");
                    var resdata = await dbConnection.QueryAsync<PopularItem>(query.ToString(), new { RESID = resid });
                    return resdata.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
